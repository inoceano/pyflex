"""Copyright (C) 2016 Joakim A. Taby

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""
import sys
import os
# noinspection PyUnresolvedReferences
from PySide2 import QtCore, QtGui, QtWidgets
import numpy as np
from ui_mainwindow import Ui_MainWindow, GenericTreeModel
import pyflex
import slenders
import re
import timehist
import errorhandling
# noinspection PyUnresolvedReferences
import h5py
import fnmatch


class Window(QtWidgets.QMainWindow, Ui_MainWindow):
    """
    Parent class for Pyflex. Data flow and Gui.
    """
    setupFullCalcResultsSignal = QtCore.Signal()

    def __init__(self, parent=None):
        # noinspection PyArgumentList
        super(Window, self).__init__(parent)
        self.setupUi(self)
        # Menu on left side.
        # Showing the thing on right side
        self.main_frame = QtWidgets.QWidget()
        # a box to contain it all
        mainbox = QtWidgets.QHBoxLayout()
        # a big splitter to make it flow
        self.mainsplitter = QtWidgets.QSplitter()
        # making a widget for the box on the right
        rightwidget = QtWidgets.QWidget()
        # making a widget for the box on the left
        leftwidget = QtWidgets.QWidget()
        # making a box on the right to have the splitter in
        rightlayout = QtWidgets.QVBoxLayout()
        # making a box on the left to have the splitter in
        leftlayout = QtWidgets.QVBoxLayout()
        # making the right splitter
        self.rightsplitter = QtWidgets.QSplitter()
        # making the left splitter
        self.leftsplitter = QtWidgets.QSplitter()
        # making the windows tile vertically
        self.rightsplitter.setOrientation(QtCore.Qt.Vertical)
        # Making an upper tabbed widget for plotting and graphics
        self.graphictab = QtWidgets.QTabWidget()
        tmp = bool('true')
        self.graphictab.setTabsClosable(tmp)
        self.graphictab.tabCloseRequested.connect(self.close_graphictab)
        self.graphictab.hide()
        self.graphictab.hid = 'yes'
        # making a lower tabbed widget for editing
        self.edittab = QtWidgets.QTabWidget()
        self.edittab.setTabsClosable(tmp)
        self.edittab.tabCloseRequested.connect(self.close_edittab)
        self.edittab.hide()
        self.edittab.hid = 'yes'
        # making the windows tile vertically
        self.leftsplitter.setOrientation(QtCore.Qt.Vertical)
        # Putting the pieces together
        mainbox.addWidget(self.mainsplitter)
        self.treemodel_setup()
        # adding a textbrowser as status and error area
        self.message_pane = QtWidgets.QTextBrowser()
        self.leftsplitter.addWidget(self.message_pane)

        self.rightsplitter.addWidget(self.graphictab)
        self.rightsplitter.addWidget(self.edittab)
        self.mainsplitter.addWidget(leftwidget)
        self.mainsplitter.addWidget(rightwidget)
        rightwidget.setLayout(rightlayout)
        leftwidget.setLayout(leftlayout)
        rightlayout.addWidget(self.rightsplitter)
        leftlayout.addWidget(self.leftsplitter)
        self.main_frame.setLayout(mainbox)
        self.setCentralWidget(self.main_frame)
        self.menuactions()
        # noinspection PyUnresolvedReferences
        self.view.customContextMenuRequested.connect(self.open_context_menu)
        # user messages
        sys.stdout = errorhandling.EmittingStream()
        sys.stdout.textWritten.connect(self.messagehandling)
        sys.stderr = errorhandling.EmittingStream()
        sys.stderr.textWritten.connect(self.errorhandling)
        # Initilizing variables that is set later depending on user
        # Object holding the full stress calculation
        self.fullstress = None
        # Whether full calculations are shown
        # self.fullstress.fullcalcShow = 'no'
        # Layout for the left side of full result tab
        self.fullResultLeftLayout = None
        # Tree view for full results
        self.fullResultView = None
        # Model for full results viewer
        self.fullResultTree = None
        # Widget containing the full results
        self.fullResultWidget = None
        # Container widget for left side of full results tab
        self.fullResultLeftWidget = None
        # Button for export av full results
        self.fullResultsExportButton = None
        # Friction calculation object
        self.friccalc = None
        #connecting signal
        self.view.itemDelegate().updateModelandDataSignal.connect(self.update_slenderdata)

        # creating a directory for working outputfiles
        if not os.path.exists('tmp'):
            os.makedirs('tmp')
        else:
            # list of things to clean up
            for f in os.listdir('tmp'):
                if fnmatch.fnmatch(f, 'fullresult*.h5'):
                    os.remove(os.path.join('tmp', f))

    def update_actions(self):
        has_selection = not self.view.selectionModel().selection().isEmpty()
        self.removeRowAction.setEnabled(has_selection)
        self.removeColumnAction.setEnabled(has_selection)

        has_current = self.view.selectionModel().currentIndex().isValid()
        self.insertRowAction.setEnabled(has_current)
        self.insertColumnAction.setEnabled(has_current)

        if has_current:
            self.view.closePersistentEditor(self.view.selectionModel().currentIndex())

            row = self.view.selectionModel().currentIndex().row()
            column = self.view.selectionModel().currentIndex().column()
            if self.view.selectionModel().currentIndex().parent().isValid():
                self.statusBar().showMessage("Position: (%d,%d)" % (row, column))
            else:
                self.statusBar().showMessage("Position: (%d,%d) in top level" % (row, column))

    def open_context_menu(self, position):
        """
        Method for context menu. Should allow for inserting context based info.
        Need to be rewritten when first item is working.
        :param position: self. mainWindow class containing a treeview and a model
        """
        self.tree_model.layoutChanged.emit()
        self.update_slenderdata()
        indexes = self.view.selectedIndexes()
        menu = QtWidgets.QMenu()
        index = indexes[0]
        item = self.tree_model.getItem(index)
        if index.column() == 0:
            if item.itemData[0] == 'Model data':
                add_layer_action = menu.addAction(self.tr("Add layer"))
                action = menu.exec_(self.view.viewport().mapToGlobal(position))
                if action == add_layer_action:
                    self.update_slenderdata()
                    self.tree_model.addLayer()
                    self.tree_model.updateModel()
            elif item.itemData[0] == 'Time history package':
                plot_action = menu.addAction(self.tr("Plot the time histories"))
                action = menu.exec_(self.view.viewport().mapToGlobal(position))
                if action == plot_action:
                    item = self.tree_model.getItem(index)
                    for timehistInstance in self.tree_model.timehists:
                        if item.itemData[1] == timehistInstance.name:
                            if self.graphictab.hid == 'yes':
                                self.graphictab.show()
                                self.graphictab.hid = 'no'
                            timehistInstance.plot(self.graphictab)
            elif item.itemData[0] == 'Time history file':
                plot_action = menu.addAction(self.tr("Plot the time histories"))
                action = menu.exec_(self.view.viewport().mapToGlobal(position))
                if action == plot_action:
                    item = self.tree_model.getItem(index)
                    for timehistInstance in self.tree_model.timehists:
                        if item.itemData[1] == timehistInstance.name:
                            if self.graphictab.hid == 'yes':
                                self.graphictab.show()
                            timehistInstance.plot(self.graphictab)
            elif item.itemData[0] == 'Calculations':
                friction_action = menu.addAction(self.tr('Calculate friction importance'))
                full_calc_action = menu.addAction(self.tr('Calculate stresses, including the friction effect'))
                action = menu.exec_(self.view.viewport().mapToGlobal(position))
                if action == friction_action:
                    self.add_frictioncalc()
                if action == full_calc_action:
                    self.add_full_calc()

            elif item.itemData[0] == 'Friction calculation':
                # initialising variables
                view_action = None
                plot_action = None
                # finding whether the friction calculations are done
                hasfric = 0
                if hasattr(self.friccalc, 'bendMax'):
                    hasfric = 1
                # adding actions
                edit_action = menu.addAction(self.tr('Edit'))
                if hasfric == 1:
                    view_action = menu.addAction(self.tr('View resulttable'))
                    plot_action = menu.addAction(self.tr('View plot'))
                action = menu.exec_(self.view.viewport().mapToGlobal(position))
                if action == edit_action:
                    self.frictionedit()
                elif action == view_action:
                    self.show_friction_results()
                elif action == plot_action:
                    self.fricplot()

            elif item.itemData[0] == 'Full stress calculation':
                # finding whether the friction calculations are done
                hasfull = 0
                if self.fullResultWidget:
                    hasfull = 1
                # adding actions
                edit_action = menu.addAction(self.tr('Edit'))
                if hasfull:
                    view_action = menu.addAction(self.tr('View results'))
                # add actions for results and stuff here
                action = menu.exec_(self.view.viewport().mapToGlobal(position))
                # noinspection PyUnboundLocalVariable
                if action == edit_action:
                    self.fulledit()
                elif action == view_action:
                    self.show_full_results()

    def add_full_calc(self):
        self.fulledit()
        self.tree_model.addCalc(key='full')
        # self.tree_model.addFullStressCalc()

    def fulledit(self):
        self.update_slenderdata()
        # Checking if we have an open tab for calculations
        # If so, we open that tab
        if not self.fullstress:
            self.fullstress = timehist.FullStressCalc()
            self.fullstress.setup_tab(self)
        self.edittab.addTab(self.fullstress.mainwidget, 'Full stress calc')
        open_tab(self.edittab, 'Full stress calc')

    def fullcalc(self):
        self.update_slenderdata()
        self.setup_fullcalc_results()
        self.setupFullCalcResultsSignal.emit()
        self.fullstress.removeFullCalcResultSignal.connect(self.remove_fullcalc_result)
        self.fullstress.calculate(self.tree_model.slenderdata)
        for i in self.fullstress.analyses:
            i.addFullCalcSignal.connect(self.add_fullcalc_results)
        self.fullstress.fullcalcShow = 'yes'

    def setup_fullcalc_results(self):
        self.show_graphictab()
        tabs = self.graphictab.count()
        found = 0
        for index in range(tabs):
            text = self.graphictab.tabText(index)
            if text == 'Full calculation results':
                self.close_graphictab(index)
                self.show_graphictab()
                found = 1
                break
        self.fullResultWidget = QtWidgets.QSplitter()
        self.fullResultLeftLayout = QtWidgets.QVBoxLayout()
        self.fullResultLeftWidget = QtWidgets.QWidget()
        names = []
        for i in self.fullstress.timehists:
            names.append(i.name)
        if self.fullstress.canvas:
            self.fullstress.deleteplot()
        # adding tree model for results exploration
        self.fullResultTree = FullResultTreeModel(names)
        self.fullResultView = QtWidgets.QTreeView()
        self.fullResultProxy = timehist.FullTreeProxyModel()
        self.fullResultProxy.setSourceModel(self.fullResultTree)
        #self.fullResultView.setModel(self.fullResultProxy)
        self.fullResultView.setModel(self.fullResultTree)
        selection = self.fullResultView.selectionModel()
        selection.selectionChanged.connect(self.update_fullplot)
        self.fullResultLeftLayout.addWidget(self.fullResultView)
        self.fullResultsExportButton = QtWidgets.QPushButton('Export all')
        self.fullResultsExportButton.clicked.connect(self.fullstress.export_all_results)
        self.fullResultLeftLayout.addWidget(self.fullResultsExportButton)
        self.fullResultLeftWidget.setLayout(self.fullResultLeftLayout)
        self.fullResultWidget.addWidget(self.fullResultLeftWidget)
        if found == 0:
            self.graphictab.addTab(self.fullResultWidget, 'Full calculation results')
            self.graphictab.setCurrentIndex(tabs)
        elif found == 1:
            if tabs == 1:
                self.graphictab.addTab(self.fullResultWidget, 'Full calculation results')
                self.graphictab.setCurrentIndex(0)
            else:
                # noinspection PyUnboundLocalVariable
                self.graphictab.insertTab(index, self.fullResultWidget, 'Full calculation results')
                self.graphictab.setCurrentIndex(index)

    def remove_fullcalc_result(self):
        tabs = self.graphictab.count()
        for index in range(tabs):
            text = self.graphictab.tabText(index)
            if text == 'Full calculation results':
                self.close_graphictab(index)
                break

    def update_fullplot(self, selected):
        # assume only 1 index
        index = selected.indexes()
        self.fullResultTree.toggle(index[0])
        self.fullstress.plot(self.fullResultTree.plotData, self.fullResultWidget)

    def add_fullcalc_results(self, number):
        self.fullResultTree.add_result(number)

    def add_frictioncalc(self):
        self.frictionedit()
        self.tree_model.addCalc(key='friction')

    def frictionedit(self):
        self.update_slenderdata()
        # checking if we have an open friction tab
        # if so, we open that tab
        if not self.friccalc:
            # Building up the window
            self.friccalc = timehist.FrictionCalc()
            self.friccalc.setup_tab(self)

        self.edittab.addTab(self.friccalc.mainwidget, 'Friction')
        open_tab(self.edittab, 'Friction')

    def calculate_friction(self):
        self.update_slenderdata()
        self.datacheck()
        self.friccalc.calculate(self.tree_model.slenderdata)
        self.show_friction_results()

    def show_friction_results(self):
        self.show_graphictab()
        # finding if we already have one of these. if so, remove it and insert this there
        tabs = self.graphictab.count()
        found = 0
        for index in range(tabs):
            text = self.graphictab.tabText(index)
            if text == 'Friction calcs':
                self.close_graphictab(index)
                self.show_graphictab()
                found = 1
                break

        main_widget = QtWidgets.QWidget()
        main_layout = QtWidgets.QVBoxLayout()
        main_widget.setLayout(main_layout)
        title = QtWidgets.QLabel('<H1> Friction calculations </H1>')
        main_layout.addWidget(title)
        table = self.friccalc.table()
        main_layout.addWidget(table)
        plot_button = QtWidgets.QPushButton('Plot the stress components')
        plot_button.clicked.connect(self.fricplot)
        main_layout.addWidget(plot_button)
        if found == 0:
            self.graphictab.addTab(main_widget, 'Friction calcs')
            self.graphictab.setCurrentIndex(tabs)
        elif found == 1:
            if tabs == 1:
                self.graphictab.addTab(main_widget, 'Friction calcs')
                self.graphictab.setCurrentIndex(0)
            else:
                # noinspection PyUnboundLocalVariable
                self.graphictab.insertTab(index, main_widget, 'Friction calcs')
                self.graphictab.setCurrentIndex(index)

    def show_full_results(self):
        # searching if tab is already there
        tabs = self.graphictab.count()
        found = False
        for index in range(tabs):
            text = self.graphictab.tabText(index)
            if text == 'Full calculation results':
                found = True
                break
        # if it is, switch to it, if not, add it
        # if the result is not existing, do nothing
        if self.fullResultWidget:
            if found:
                # noinspection PyUnboundLocalVariable
                self.graphictab.setCurrentIndex(index)
            else:
                self.graphictab.addTab(self.fullResultWidget, 'Full calculation results')
                self.show_graphictab()

    def fricplot(self):
        self.show_graphictab()
        self.friccalc.plot(self.graphictab)

    # TODO: move it to slender
    # noinspection PyUnresolvedReferences
    def datacheck(self):
        if hasattr(self.tree_model.slenderdata, 'lay_angle'):
            count = 0
            for i in self.tree_model.slenderdata.lay_angle:
                count += 1
                if i > np.pi / 2 or i < -np.pi / 2:
                    print('The lay angle of layer %d needs to be in the range '
                          '-pi/2<=layang <=pi/2' % count)
        if hasattr(self.tree_model.slenderdata, 'layer_radii'):
            for i in range(len(self.tree_model.slenderdata.layer_radii) - 1):
                if self.tree_model.slenderdata.layer_radii[i + 1] >= self.tree_model.slenderdata.layer_radii[i]:
                    print('In Pyflex we model from the outside inwards')
            for i in self.tree_model.slenderdata.layer_radii:
                if i <= 0:
                    print('The radius of layer %d needs to be positive' % i)
        if hasattr(self.tree_model.slenderdata, 'comp_number'):
            count = 0
            for i in self.tree_model.slenderdata.comp_number:
                count += 1
                if i <= 0:
                    print('The number of tendons in layer %d must be more then 0.'
                          % count)

    def update_slenderdata(self):
        parentindex = self.tree_model.index(0, 0, QtCore.QModelIndex())
        rows = self.tree_model.rowCount(parentindex)
        for row in range(rows):
            index = self.tree_model.index(row, 0, parentindex)
            # find what it is, and if it has kids, go to right branch for putting further
            itemtext = self.tree_model.data(index, QtCore.Qt.DisplayRole)
            dataindex = self.tree_model.index(row, 1, parentindex)
            itemdata = self.tree_model.data(dataindex, QtCore.Qt.DisplayRole)
            if itemtext == 'Data for layer':
                subindex = int(itemdata) - 1
                self.update_layerdata(subindex, index)
            if itemtext == 'Internal pressure layer':
                self.tree_model.slenderdata.intpresslayer = int(float(itemdata))
            if itemtext == 'External pressure layer':
                self.tree_model.slenderdata.extpresslayer = int(float(itemdata))
        self.update_scenariodata()
        self.update_globaldata()
        self.tree_model.updateModel()
        self.tree_model.layoutChanged.emit()

    # noinspection PyUnresolvedReferences,PyUnresolvedReferences
    def update_layerdata(self, subindex, parentindex):
        rows = self.tree_model.rowCount(parentindex)
        for row in range(rows):
            index = self.tree_model.index(row, 0, parentindex)
            itemtext = self.tree_model.data(index, QtCore.Qt.DisplayRole)
            dataindex = self.tree_model.index(row, 1, parentindex)
            itemdata = self.tree_model.data(dataindex, QtCore.Qt.DisplayRole)
            if itemtext == 'Type':
                if len(self.tree_model.slenderdata.layer_type) >= subindex + 1:
                    self.tree_model.slenderdata.layer_type[subindex] = itemdata
                else:
                    self.tree_model.slenderdata.layer_type.append(itemdata)
            if itemtext == 'Radius':
                item = itemcheck(itemdata, 'Radius')
                if item is None:
                    print('Only one radius allowed for a layer')
                    return

                elif hasattr(item, "__iter__"):
                    # error message
                    print("Only one radius allowed for a layer")
                    return
                else:
                    if len(self.tree_model.slenderdata.layer_radii) >= subindex + 1:
                        self.tree_model.slenderdata.layer_radii[subindex] = float(item)
                    else:
                        self.tree_model.slenderdata.layer_radii.append(float(item))

            if itemtext == 'Thickness':
                item = itemcheck(itemdata, 'Thickness')
                if hasattr(item, "__iter__"):
                    # error message
                    print('Only one thickness allowed for a layer')
                    return
                else:
                    if len(self.tree_model.slenderdata.thickness) >= subindex + 1:
                        self.tree_model.slenderdata.thickness[subindex] = float(item)
                    else:
                        self.tree_model.slenderdata.thickness.append(float(item))

            if itemtext == 'Initial gap':
                item = itemcheck(itemdata, 'Initial gap')
                if hasattr(item, "__iter__"):
                    # error message
                    print('Only one initial gap allowed for a layer')
                    return
                else:
                    if len(self.tree_model.slenderdata.gap_ini) >= subindex + 1:
                        self.tree_model.slenderdata.gap_ini[subindex] = float(item)
                    else:
                        self.tree_model.slenderdata.gap_ini.append(float(item))

            if itemtext == 'Youngs modulus':
                item = itemcheck(itemdata, 'Youngs Modulus')
                if hasattr(item, "__iter__"):
                    # error message
                    print('Only one Youngs modulus allowed for a layer')
                    return
                else:
                    if len(self.tree_model.slenderdata.youngs_modulus) >= subindex + 1:
                        self.tree_model.slenderdata.youngs_modulus[subindex] = float(item)
                    else:
                        self.tree_model.slenderdata.youngs_modulus.append(float(item))

            if itemtext == "Poisson's ratio":
                item = itemcheck(itemdata, "Poisson's ratio")
                if hasattr(item, "__iter__"):
                    # error message
                    print("Only one Poisson's ratio allowed for a layer")
                    return
                else:
                    if len(self.tree_model.slenderdata.poisson) >= subindex + 1:
                        self.tree_model.slenderdata.poisson[subindex] = float(item)
                    else:
                        self.tree_model.slenderdata.poisson.append(float(item))

            if itemtext == 'Friction factor':
                item = itemcheck(itemdata, 'Friction factor')
                if hasattr(item, "__iter__"):
                    # error message
                    print('Only one friction factor allowed for a layer')
                    return
                else:
                    if len(self.tree_model.slenderdata.fricfac) >= subindex + 1:
                        self.tree_model.slenderdata.fricfac[subindex] = float(item)
                    else:
                        self.tree_model.slenderdata.fricfac.append(float(item))

            if itemtext == 'Width':
                item = itemcheck(itemdata, 'Width')
                if hasattr(item, "__iter__"):
                    # error message
                    print('Only one width allowed for a layer')
                    return
                else:
                    if len(self.tree_model.slenderdata.width) >= subindex + 1:
                        self.tree_model.slenderdata.width[subindex] = float(item)
                    else:
                        self.tree_model.slenderdata.width.append(float(item))

            if itemtext == 'Lay angle':
                item = itemcheck(itemdata, 'Lay angle')
                if hasattr(item, "__iter__"):
                    # error message
                    print('Only one lay angle allowed for a layer')
                    return
                else:
                    if len(self.tree_model.slenderdata.lay_angle) >= subindex + 1:
                        self.tree_model.slenderdata.lay_angle[subindex] = float(item)
                    else:
                        self.tree_model.slenderdata.lay_angle.append(float(item))

            if itemtext == 'Number of tendons':
                item = itemcheck(itemdata, 'Number of tendons')
                if hasattr(item, "__iter__"):
                    # error message
                    print('Only one number of tendons allowed for a layer')
                    return
                else:
                    if len(self.tree_model.slenderdata.comp_number) >= subindex + 1:
                        self.tree_model.slenderdata.comp_number[subindex] = float(item)
                    else:
                        self.tree_model.slenderdata.comp_number.append(float(item))

    def update_scenariodata(self):
        parentindex = self.tree_model.index(1, 0, QtCore.QModelIndex())
        rows = self.tree_model.rowCount(parentindex)
        for row in range(rows):
            index = self.tree_model.index(row, 0, parentindex)
            itemtext = self.tree_model.data(index, QtCore.Qt.DisplayRole)
            dataindex = self.tree_model.index(row, 1, parentindex)
            itemdata = self.tree_model.data(dataindex, QtCore.Qt.DisplayRole)
            if itemtext == 'Typical curvature':
                item = itemcheck(itemdata, 'Typical curvature')
                if hasattr(item, "__iter__"):
                    # error message
                    self.errorhandling('Only one typical curvature allowed')
                    return
                else:
                    self.tree_model.slenderdata.typical_curvature = item
            if itemtext == 'Typical tension':
                item = itemcheck(itemdata, 'Typical tension')
                if hasattr(item, "__iter__"):
                    # error message
                    self.errorhandling('Only one typical tension allowed')
                else:
                    self.tree_model.slenderdata.typical_tension = item
            if itemtext == 'Internal pressure':
                item = itemcheck(itemdata, 'Internal pressure')
                if hasattr(item, "__iter__"):
                    # error message
                    self.errorhandling('Only one internal pressure allowed')
                else:
                    self.tree_model.slenderdata.internal_pressure = item

    def update_globaldata(self):
        foundglobaldata = None
        parentindex = QtCore.QModelIndex()
        rows = self.tree_model.rowCount(parentindex)
        for row in range(rows):
            index = self.tree_model.index(row, 0, parentindex)
            itemtext = self.tree_model.data(index, QtCore.Qt.DisplayRole)
            if itemtext == 'Global results':
                foundglobaldata = 1
                break
        if foundglobaldata:
            # noinspection PyUnboundLocalVariable
            rows = self.tree_model.rowCount(index)
            count = 0
            for row in range(rows):
                textindex = self.tree_model.index(row, 0, index)
                dataindex = self.tree_model.index(row, 1, index)
                itemtext = self.tree_model.data(textindex, QtCore.Qt.DisplayRole)
                itemdata = self.tree_model.data(dataindex, QtCore.Qt.DisplayRole)
                if itemtext == 'Timehistories from directory':
                    subrows = self.tree_model.rowCount(textindex)
                    for subrow in range(subrows):
                        sub_data_index = self.tree_model.index(subrow, 1, textindex)
                        itemdata = self.tree_model.data(sub_data_index, QtCore.Qt.DisplayRole)
                        self.tree_model.timehists[count].name = itemdata
                        count += 1
                else:
                    self.tree_model.timehists[count].name = itemdata
                    count += 1

    # noinspection PyUnresolvedReferences
    def menuactions(self):
        """
        managing menu selection.
        """
        self.exitAction.triggered.connect(QtWidgets.qApp.quit)
        self.openAction.triggered.connect(self.open)
        self.newAction.triggered.connect(self.treemodel_setup)
        self.importAction.triggered.connect(self.import_ts)
        self.importFolderAction.triggered.connect(self.import_ts_folder)
        self.aboutAction.triggered.connect(self.license)

    # noinspection PyAttributeOutsideInit
    def treemodel_setup(self):
        """
        Setting up the left menu with its tree model.
        Further items in left menu should be added here.
        """
        if hasattr(self, 'tree_model'):
            del self.tree_model
        self.tree_model = TreeModel()
        self.view.setModel(self.tree_model)
        self.leftsplitter.insertWidget(0, self.view)

    # noinspection PyAttributeOutsideInit
    def open(self):
        """Controls what is happening when opening a file. Reads it, parses it, and puts the data in a tree model"""
        # noinspection PyArgumentList,PyCallByClass
        tmp = QtWidgets.QFileDialog.getOpenFileName(self, caption='Open File',
                                                filter="PyFlex input files (*.dat);; Any files (*)",
                                                selectedFilter="PyFlex input files (*.dat)")
        self.fileName = tmp[0]
        if self.fileName:
            self.delimiter = '\t'
            self.commentchar = '#'
            self.tree_model.slenderdata = pyflex.analytical_input(self.fileName,
                                                                  self.delimiter, self.commentchar)
            self.update_globaldata()
            self.put_import()

    def license(self):
        '''Popup a box with about message.'''
        QtWidgets.QMessageBox.about(self, "About Pyflex v1.0",
        """<p>Copyright &copy; 2016 Joakim A Taby.
        All rights reserved in accordance with
        GPL v3 - NO WARRANTIES!
        For full license click <a href='../docs/License/license.md'>here</a>""")

    def put_import(self):
        """Putting the input from input file into where it belongs"""
        self.tree_model.updateModel()

    def import_ts(self):
        """Controls what is happening when opening a file. Reads it, parses it, and puts the data in a tree model"""
        # noinspection PyArgumentList,PyCallByClass
        tmp = QtWidgets.QFileDialog.getOpenFileName(self, caption='Open File',
                                                filter="PyFlex time series files (*.tim);; Any files (*)",
                                                selectedFilter="PyFlex time series files (*.tim)")
        time_file_name = tmp[0]
        file_name = re.split('[/\\\\]', time_file_name)[-1]
        if time_file_name:
            if not hasattr(self.tree_model, 'timehists'):
                self.tree_model.timehists = []
            self.tree_model.timehists.append(timehist.TimeHist(time_file_name, file_name))
            self.tree_model.addTimeHist(file_name)
            self.tree_model.layoutChanged.emit()

    def import_ts_folder(self):
        # getting directory and list of files
        dialog = QtWidgets.QFileDialog()
        dialog.setFileMode(QtWidgets.QFileDialog.Directory)
        dialog.setOption(QtWidgets.QFileDialog.ShowDirsOnly)
        directory = dialog.getExistingDirectory(self, 'Choose Directory', os.path.curdir)
        if directory:
            filelist = os.listdir(directory)
            filelist.sort()
            # making the list of time histories
            if not hasattr(self.tree_model, 'timehists'):
                self.tree_model.timehists = []
            # adding Global result item in tree model if it doesn't exist
            global_result_item = self.tree_model.addGlobalResults()
            # adding the directory items
            directory_parent = self.tree_model.addGlobalDirectory(global_result_item, directory)
            for file in filelist:
                if file.endswith(".tim"):
                    # making tree model for this
                    self.tree_model.timehists.append(timehist.TimeHist(file, file, directory=directory))
                    self.tree_model.addTimeHist(file, parent=directory_parent)
            self.tree_model.layoutChanged.emit()

    def errorhandling(self, text):
        """
        Append red text to the QTextEdit.
        :param text: text to write
        """
        text = text.replace("\r", "")
        text = text.replace("\n", "")
        text = text.strip()
        if len(text) > 0:
            self.message_pane.append(str('<font color="red">' + text + '</font>'))

    def messagehandling(self, text):
        """
        Append text to the QTextEdit.
        :param text: text to add to QTextEdit
        """
        text = text.replace("\r", "")
        text = text.replace("\n", "")
        text = text.strip()
        if len(text) > 0:
            self.message_pane.append(self.message_pane.append(str('<font color="black">' + text + '</font>')))

    def show_graphictab(self):
        # making sure the graphictab is shown
        if self.graphictab.hid == 'yes':
            self.graphictab.show()
            self.graphictab.hid = 'no'

    def close_graphictab(self, index):
        widget = self.graphictab.widget(index)
        self.graphictab.removeTab(index)
        text = self.graphictab.tabText(index)
        if text in ('Friction calcs', 'Plot of stresses from friction calculations'):
            widget.deleteLater()
        # check if tabwidget is empty
        widget = self.graphictab.widget(0)
        if not widget:
            self.graphictab.hide()
            self.graphictab.hid = 'yes'

    def close_edittab(self, index):
        widget = self.edittab.widget(index)
        text = self.edittab.tabText(index)
        if text == 'Friction':
            self.edittab.removeTab(index)

        elif text == 'Full stress calc':
            self.edittab.removeTab(index)
        else:
            self.edittab.removeTab(index)
            widget.deleteLater()

        # check if tabwidget is empty
        widget = self.edittab.widget(0)
        if not widget:
            self.edittab.hide()
            self.edittab.hid = 'yes'

    # noinspection PyPep8Naming
    def eventFilter(self, obj, event):
        if self.fullstress:
            if obj is self.fullstress.mainwidget:
                if event.type() == QtCore.QEvent.DragEnter:
                    if event.mimeData().hasFormat('text/plain'):
                        text = event.mimeData().data('text/plain')
                        text = str(text)
                        text = text.split(',')
                        if text[0][2:] in ('singlefile', 'directory', 'singlefilefromdirectory'):
                            event.accept()  # must accept the dragEnterEvent or else the dropEvent can't occur !!!
                if event.type() == QtCore.QEvent.Drop:
                    if event.mimeData().hasFormat('text/plain'):
                        text = event.mimeData().text()
                        text = text.split(',')
                        dragdrop = self.fullstress.mainwidget.findChild(QtWidgets.QLabel, 'dragdrop')
                        if text[0] in ('singlefile', 'directory'):
                            self.add_ts(text[0], text[1], None, ts_type='full')
                            dragdrop.hide()
                        elif text[0][2:] == 'singlefilefromdirectory':
                            self.add_ts(text[0], text[1], text[2], ts_type='full')
                            dragdrop.hide()
                return True
        if self.friccalc:
            if obj is self.friccalc.mainwidget:
                if event.type() == QtCore.QEvent.DragEnter:
                    if event.mimeData().hasFormat('text/plain'):
                        text = event.mimeData().data('text/plain')
                        text = str(text)
                        text = text.split(',')
                        if text[0] in ('singlefile', 'singlefilefromdirectory'):
                            event.accept()  # must accept the dragEnterEvent or else the dropEvent can't occur !!!
                if event.type() == QtCore.QEvent.Drop:
                    if event.mimeData().hasFormat('text/plain'):
                        text = event.mimeData().data('text/plain')
                        text = text.split(',')
                        dragdrop = self.friccalc.mainwidget.findChild(QtWidgets.QLabel, 'dragdrop')
                        if text[0] == 'singlefilefromdirectory':
                            self.add_ts(text[0], text[1], text[2], ts_type='friction')
                            dragdrop.hide()
                        elif text[0] == 'singlefile':
                            self.add_ts(text[0], text[1], None, ts_type='friction')
                            dragdrop.hide()
                return True  # lets the event continue to the edit
        return False

    def add_ts(self, import_type, info, directory, ts_type='full'):
        self.update_slenderdata()
        info = str(info)
        import_type = str(import_type)
        if import_type == 'singlefile':
            self.append_ts(info, None, ts_type)
        if import_type == 'directory':
            for i in self.tree_model.timehists:
                if hasattr(i, 'directory'):
                    if i.directory == info:
                        self.append_ts(i.name, i.directory, ts_type)

        if import_type == 'singlefilefromdirectory':
            self.append_ts(info, directory, ts_type)
        if ts_type == 'full':
            self.fullstress.setup_ts_edit()
        if ts_type == 'friction':
            self.friccalc.setup_ts_edit()

    def append_ts(self, name, directory, ts_type='full'):

        # finding the timehistory and adding it to the timehist in the
        # fullstress calculations. Adding the info into lists
        found = 0
        for i in self.tree_model.timehists:
            if i.name == name:
                if ts_type == 'full':
                    if not directory:
                        self.fullstress.add_ts(i)
                        found = 1
                        break
                    elif hasattr(i, 'directory'):
                        if directory == i.directory:
                            self.fullstress.add_ts(i)
                            found = 1
                            break
                elif ts_type == 'friction':
                    self.friccalc.add_ts(i)
                    found = 1
        if found == 0:
            print('Could not transfer data. Try again.')
            return

# noinspection PyPep8Naming
class TreeItem(object):
    """ class for making the items that constitutes the tree model. The items carries the data"""

    def __init__(self, data, parent=None, finalRow=None, color=None):
        self.parentItem = parent
        self.itemData = data
        self.childItems = []
        self.finalRow = finalRow
        self.color = color

    def setColor(self, color):
        self.color = color

    def Color(self):
        return self.color

    def insertChildren(self, position, count, columns, finalRow=None):
        if position < 0 or position > len(self.childItems):
            return False

        for row in range(count):
            # noinspection PyUnusedLocal
            data = [None for v in range(columns)]
            if finalRow:
                item = TreeItem(data, self, finalRow=finalRow + row - range(count)[0])
            else:
                item = TreeItem(data, self)
            self.childItems.insert(position, item)

        return True

    def appendChild(self, item):
        self.childItems.append(item)

    def child(self, row):
        return self.childItems[row]

    def childCount(self):
        return len(self.childItems)

    def columnCount(self):
        return len(self.itemData)

    def data(self, column):
        try:
            return self.itemData[column]
        except IndexError:
            return None

    def parent(self):
        return self.parentItem

    def removeChildren(self, position, count):
        if position < 0 or position + count > len(self.childItems):
            return False

        for row in range(count):
            self.childItems.pop(position)

        return True

    def row(self):
        if self.parentItem:
            return self.parentItem.childItems.index(self)
        return 0

    def setData(self, column, value):
        """
        Method for setting data into given column. Should be used when setting data in model.
        :param value: The value of the data to be written
        :param column: Which column the data is to be written to
        """
        if column < 0 or column >= len(self.itemData):
            return False

        self.itemData[column] = value
        # writing this to background data as well. I have the feeling we have one layer to much here
        return True

class ModelTreeItem(TreeItem):
    """ Special items for model data"""
    def __init__(self, data, parent=None, finalRow=None, color=None,type=None,nopt=None):
        super(ModelTreeItem, self).__init__(data,parent=parent,finalRow=finalRow,color=color)
        if type=='combo':
            self.type=type
            self.options=['sheath','wires']
        if type=='presslayer':
            self.type=type
            self.options=list(map(str, list(range(1,nopt+1))))

    def updateitemoptions(self,options):
        self.options=options

def itemcheck(item, inputstring):
    """Function for checking an item. returning something
    Returning float of the input if one input. Returning a list if more inputs
    :param item:The data to be checked
    :param inputstring: The type of data to be checked. For output to user.
    """
    # noinspection PyPep8,PyPep8
    if isinstance(item, float):
        return item
    elif (not hasattr(item, "strip") and
              hasattr(item, "__getitem__") or
              hasattr(item, "__iter__")):
        # listlike
        if len(item) > 1:
            for i in range(len(item)):
                if item[i] is None:
                    return item[i]
                else:
                    try:
                        return float(item)
                    except (TypeError, ValueError):
                        # error message
                        # if a string of numbers
                        l = []
                        for t in re.split(',| ', item):
                            try:
                                l.append(float(t))
                            except ValueError:
                                pass
                            except TypeError:
                                print('Cannot parse the ', inputstring, ' given')
                        return l

        else:
            if not item:
                # empty string
                return None
            elif item[0] is None:
                return item[0]
            else:
                try:
                    return float(item[0])
                except TypeError:
                    # error message
                    print('Cannot parse the ', inputstring, ' given')
    else:
        # Stringlike
        if item is None:
            return item
        try:
            return float(item)
        except TypeError:
            # error message
            print('Cannot parse the ', inputstring, ' given')

def open_tab(tab_widget, intext):
    if tab_widget.hid == 'yes':
        tab_widget.hid = 'no'
        tab_widget.show()
    tabs = tab_widget.count()
    for index in range(tabs):
        text = tab_widget.tabText(index)
        if text == intext:
            tab_widget.setCurrentIndex(index)
            return 'True'
    return 'False'

# noinspection PyPep8Naming
class TreeModel(GenericTreeModel):
    """The model holding the connection between the items."""

    def __init__(self, parent=None):
        super(TreeModel, self).__init__(parent)
        self.rootItem = TreeItem(("Item", "Summary/Value"))
        self.slenderdata = slenders.Slender(['0'])
        self.setupModelData(self.rootItem)

    def flags(self, index):
        """
        Used to select the way each cell works
        :param index: the index of the item queried
        :return: view get info on items "Type"
        """
        if not index.isValid():
            return 0
        if index.column() > 0:
            item = self.getItem(index)
            itemtext = item.itemData[0]
            tsItem = self.recParentValidate(index, 'Global results')
            if itemtext in ('Calculations', 'Data for layer', 'Model data', 'Scenario definition'):
                return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
            elif tsItem:
                return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
            else:
                calcItem = self.recParentValidate(index, 'Calculations')
                if calcItem:
                    return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
                else:
                    return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        else:
            tsItem = self.recParentValidate(index, 'Global results')
            if tsItem:
                return QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsDragEnabled
            else:
                return QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled

    # drag and drop stuff
    @staticmethod
    def mimeTypes():
        return ['text/plain']

    def mimeData(self, indexes):
        item = self.getItem(indexes[0])
        if item.itemData[0] == 'Time history package':
            mimedata = QtCore.QMimeData()
            #mimedata.setData('text/plain', 'singlefile,%s' % item.itemData[1])
            mimedata.setText('singlefile,%s' % item.itemData[1])
        elif item.itemData[0] == 'Timehistories from directory':
            mimedata = QtCore.QMimeData()
            mimedata.setText('directory,%s' % item.itemData[1])
            #mimedata.setData('text/plain', 'directory,%s' % item.itemData[1])
        elif item.itemData[0] == 'Time history file':
            mimedata = QtCore.QMimeData()
            #mimedata.setData('text/plain', 'singlefilefromdirectory,%s,%s'
            #                 % (item.itemData[1], item.parent().itemData[1]))
            mimedata.setText('singlefilefromdirectory,%s,%s' % (item.itemData[1], item.parent().itemData[1]))
        else:
            return None

        return mimedata

    def updateModel(self):
        """Model for updating the model with new slenderdata"""
        parentindex = self.index(0, 0, QtCore.QModelIndex())
        #Updating slenderdata that is not on a layer level
        if hasattr(self.slenderdata, 'intpresslayer'):
#            rows = self.rowCount(parentindex)
#            parentItem = self.getItem(parentindex)
#            for row in range(rows-1,-1,-1):
#                item = parentItem.child(row)
#                if item.itemData[0] == 'Internal pressure layer':
#                    self.removeRows(row, 1, parentindex)
            self.setSlenderData(self.slenderdata.intpresslayer, parentindex, 'Internal pressure layer')
        if hasattr(self.slenderdata, 'extpresslayer'):
#            rows = self.rowCount(parentindex)
#            parentItem = self.getItem(parentindex)
#            for row in range(rows-1,-1,-1):
#                item = parentItem.child(row)
#                if item.itemData[0] == 'External pressure layer':
#                    self.removeRows(row, 1, parentindex)
            self.setSlenderData(self.slenderdata.extpresslayer, parentindex, 'External pressure layer')
        self.updateLayermodel(parentindex)
        # update the scenario data
        self.updateScenarioModel()
        if hasattr(self, 'timehists'):
            self.updateTimehistoryModel()
        self.layoutChanged.emit()

    # noinspection PyUnresolvedReferences
    def updateLayermodel(self, parentindex):

        rows = self.rowCount(parentindex)
        layers = self.slenderdata.num_layers()
        parentItem = self.getItem(parentindex)
        layerrows = []
        # finding rows with layerdata
        for row in range(rows):
            item = parentItem.child(row)

            if item.itemData[0] == 'Data for layer':
                layerrows.append(row)

        if len(layerrows) > layers:
            # remove extra layers
            for i in range(len(layerrows) - 1, layers - 1, -1):
                self.removeRows(layerrows[i], 1, parentindex)
        elif len(layerrows) < layers:
            # insert extra for layers
            row = layerrows[-1] + 1
            self.insertRows(row, layers - len(layerrows), parentindex)
            row-=1
            for i in range(layers - len(layerrows)):
                row += 1
                item = parentItem.child(row)
                item.setData(0, 'Data for layer')
                item.setData(1, len(layerrows) + i + 1)

        count=0
        for i in range(layers):
            layerparent = self.index(layerrows[0] + i, 0, parentindex)

            if len(self.slenderdata.layer_type) >= i + 1:
                self.setSlenderData(self.slenderdata.layer_type[i], layerparent, 'Type')
            else:
                self.setSlenderData('wires', layerparent, 'Type')

            if len(self.slenderdata.layer_radii) >= i + 1:
                self.setSlenderData(self.slenderdata.layer_radii[i], layerparent, 'Radius')
            else:
                self.setSlenderData([None], layerparent, 'Radius')

            if len(self.slenderdata.layer_radii) >= i + 1:
                self.setSlenderData(self.slenderdata.thickness[i], layerparent, 'Thickness')
            else:
                self.setSlenderData([None], layerparent, 'Thickness')

            if count>0:
                if len(self.slenderdata.gap_ini) >= i + 1:
                    self.setSlenderData(self.slenderdata.gap_ini[i], layerparent, 'Initial gap')
                else:
                    self.setSlenderData([None], layerparent, 'Initial gap')

            if len(self.slenderdata.youngs_modulus) >= i + 1:
                self.setSlenderData(self.slenderdata.youngs_modulus[i], layerparent, 'Youngs modulus')
            else:
                self.setSlenderData([None], layerparent, 'Youngs modulus')

            if len(self.slenderdata.poisson) >= i + 1:
                self.setSlenderData(self.slenderdata.poisson[i], layerparent, "Poisson's ratio")
            else:
                self.setSlenderData([None], layerparent, "Poisson's ratio")

            if count>0:
                if len(self.slenderdata.fricfac) >= i + 1:
                    self.setSlenderData(self.slenderdata.fricfac[i], layerparent, 'Friction factor')
                else:
                    self.setSlenderData([None], layerparent, 'Friction factor')

            if self.slenderdata.layer_type[i]=='wires':
                if len(self.slenderdata.lay_angle) >= i + 1:
                    self.setSlenderData(self.slenderdata.lay_angle[i], layerparent, 'Lay angle')
                else:
                    self.setSlenderData([None], layerparent, 'Lay angle')
            elif self.slenderdata.layer_type[i]=='sheath':
                self.removeSlenderData(layerparent, 'Lay angle')

            if self.slenderdata.layer_type[i]=='wires':
                if len(self.slenderdata.comp_number) >= i + 1:
                    self.setSlenderData(self.slenderdata.comp_number[i], layerparent, 'Number of tendons')
                else:
                    self.setSlenderData([None], layerparent, 'Number of tendons')
            elif self.slenderdata.layer_type[i]=='sheath':
                self.removeSlenderData(layerparent, 'Number of tendons')

            if self.slenderdata.layer_type[i]=='wires':
                if len(self.slenderdata.lay_angle) >= i + 1:
                    self.setSlenderData(self.slenderdata.width[i], layerparent, 'Width')
                else:
                    self.setSlenderData([None], layerparent, 'Width')
            elif self.slenderdata.layer_type[i]=='sheath':
                self.removeSlenderData(layerparent, 'Width')
            count+=1

    # noinspection PyUnresolvedReferences
    def updateScenarioModel(self):
        parentindex = self.index(1, 0, QtCore.QModelIndex())
        if hasattr(self.slenderdata, 'typical_curvature'):
            self.setSlenderData(self.slenderdata.typical_curvature, parentindex, 'Typical curvature')
        if hasattr(self.slenderdata, 'typical_tension'):
            self.setSlenderData(self.slenderdata.typical_tension, parentindex, 'Typical tension')
        if hasattr(self.slenderdata, 'internal_pressure'):
            self.setSlenderData(self.slenderdata.internal_pressure, parentindex, 'Internal pressure')

    def updateTimehistoryModel(self):
        parentindex = QtCore.QModelIndex()
        rows = self.rowCount(parentindex)
        for i in range(rows):
            index = self.index(i, 0, parentindex)
            itemtext = self.data(index, QtCore.Qt.DisplayRole)
            if itemtext == 'Global results':
                subindex = index
                subParentItem = self.getItem(subindex)
                break
        try:
            # noinspection PyUnboundLocalVariable
            subindex
        except UnboundLocalError:
            parentItem = self.getItem(parentindex)
            parentItem.appendChild(TreeItem(['Global results', ''
                                             ], parentItem))
            nchildren = parentItem.childCount()
            subParentItem = parentItem.child(nchildren - 1)
            subindex = self.index(nchildren + 1, 0, parentindex)
        # noinspection PyUnboundLocalVariable
        nchildren = subParentItem.childCount()
        # checking that all time histories are there
        for timehistory in self.timehists:
            if hasattr(timehistory, 'directory'):
                nchildren = subParentItem.childCount()
                for i in range(nchildren):
                    tmp = subParentItem.child(i)
                    if timehistory.directory == tmp.data(1):
                        subSubParentItem = subParentItem.child(i)
                try:
                    subSubParentItem
                except UnboundLocalError:
                    subParentItem.appendChild(
                        TreeItem(['Timehistories from directory', timehistory.directory], subParentItem))
                    nchildren = subParentItem.childCount()
                    subSubParentItem = subParentItem.child(nchildren - 1)
                found=0
                for i in range(subSubParentItem.childCount()):
                    if timehistory.name == subSubParentItem.child(i).data(1):
                        found=1
                if found==0:
                    subSubParentItem.appendChild(TreeItem(['Time history file', timehistory.name], subSubParentItem))

            else:

                found=0
                for i in range(subParentItem.childCount()):
                    if timehistory.name == subParentItem.child(i).data(1):
                        found=1
                if found == 0:
                    subParentItem.appendChild(TreeItem(['Time history package', timehistory.name]
                                                       , subParentItem))
        # todo add removal of time series that are no longer present

    def setSlenderData(self, data, parentindex, tag):
        """
        :param data: the data to be inserted in the model
        :param parentindex: The index of the parent under which the data is to be put
        :param tag: The text that belongs to the data
        :return: nothing
        """
        rows = self.rowCount(parentindex)
        for row in range(rows):
            index = self.index(row, 0, parentindex)
            itemtext = self.data(index, QtCore.Qt.DisplayRole)
            if itemtext == tag:
                dataindex = self.index(row, 1, parentindex)
                if not data:
                    self.setData(dataindex, str(data))
                elif isinstance(data, float):
                    self.setData(dataindex, str(data))
                elif isinstance(data, int):
                    self.setData(dataindex, str(data))
                elif isinstance(data, str):
                    self.setData(dataindex, data)
                else:
                    for i in range(len(data)):
                        if i == 0:
                            datastr = str(data[i])
                        else:
                            # noinspection PyUnboundLocalVariable
                            datastr = datastr + ',' + str(data[i])
                    # noinspection PyUnboundLocalVariable
                    self.setData(dataindex, datastr)
                if tag == "Internal pressure layer" or tag =="External pressure layer":
                    item=self.getItem(index)
                    item.updateitemoptions(list(map(str, list(range(1,len(self.slenderdata.layer_radii)+1)))))


                return

        parentItem = self.getItem(parentindex)
        if data is None or data == 'None':
            dataindex = self.index(rows + 1, 1, parentindex)
            self.setData(dataindex, data)
            return
        elif isinstance(data, float):
            datastr = str(data)
        elif isinstance(data, str):
            datastr = str(data)
        elif isinstance(data, int):
            datastr = str(data)
        else:
            for i in range(len(data)):
                if i == 0:
                    datastr = str(data[i])
                else:
                    # noinspection PyUnboundLocalVariable
                    datastr = datastr + ',' + str(data[i])
        # noinspection PyUnboundLocalVariable

        if tag =='Type':
            parentItem.appendChild(ModelTreeItem([tag, datastr], parentItem,type='combo'))
        elif tag == "Internal pressure layer":
            parentItem.appendChild(ModelTreeItem([tag, datastr], parentItem,type='presslayer',
                                                 nopt=len(self.slenderdata.layer_radii)))
        elif tag == "External pressure layer":
            parentItem.appendChild(ModelTreeItem([tag, datastr], parentItem,type='presslayer',
                                                 nopt=len(self.slenderdata.layer_radii)))
        else:
            parentItem.appendChild(ModelTreeItem([tag, datastr], parentItem))

        dataindex = self.index(rows + 1, 1, parentindex)
        self.setData(dataindex, data)

    def removeSlenderData(self, parentindex, tag):
        rows = self.rowCount(parentindex)
        for row in range(rows):
            index = self.index(row, 0, parentindex)
            itemtext = self.data(index, QtCore.Qt.DisplayRole)
            if itemtext == tag:
                self.getItem(parentindex).removeChildren(row,1)

    # noinspection PyUnresolvedReferences
    def addLayer(self):
        # TODO add all stuff the layer can have.
        self.slenderdata.layer_type.append('wires')
        self.slenderdata.layer_radii.append(0.0)
        self.slenderdata.thickness.append([0.0])
        self.slenderdata.gap_ini.append([0.0])
        self.slenderdata.youngs_modulus.append(0.0)
        self.slenderdata.poisson.append(0.0)
        self.slenderdata.fricfac.append(0.0)
        self.slenderdata.lay_angle.append(0.0)
        self.slenderdata.comp_number.append(0.0)
        self.slenderdata.width.append(0.0)

    def addTimeHist(self, name, parent=None):
        if not parent:
            subParentItem = self.addGlobalResults()
            subParentItem.appendChild(TreeItem(['Time history package', name]
                                               , subParentItem))
        else:
            parent.appendChild(TreeItem(['Time history file', name]
                                        , parent))

    def addGlobalResults(self):
        parentindex = QtCore.QModelIndex()
        rows = self.rowCount(parentindex)
        # finding out whether we already have time histories
        for row in range(rows):
            index = self.index(row, 0, parentindex)
            itemtext = self.data(index, QtCore.Qt.DisplayRole)
            if itemtext == 'Global results':
                subindex = index
                subParentItem = self.getItem(subindex)
                break
        try:
            # noinspection PyUnboundLocalVariable
            subindex
        except UnboundLocalError:
            parentItem = self.getItem(parentindex)
            parentItem.appendChild(TreeItem(['Global results', ''
                                             ], parentItem))
            nchildren = parentItem.childCount()
            subParentItem = parentItem.child(nchildren - 1)
        # noinspection PyUnboundLocalVariable
        return subParentItem

    def findTimehist(self, name):
        """
        :param name: Nema of the timehist
        :return: the index in which has the same name, None is returned if not found
        """
        for i in range(len(self.timehists)):
            if self.timehists[i].name == name:
                return i
        return None

    @staticmethod
    def addGlobalDirectory(parentItem, directory):
        parentItem.appendChild(TreeItem(['Timehistories from directory', directory
                                         ], parentItem))
        nchildren = parentItem.childCount()
        return parentItem.child(nchildren - 1)

    def addCalc(self, key='friction'):
        if key == 'friction':
            text = "Friction calculation"
            data = "To check importance of friction"
        elif key == 'full':
            text = 'Full stress calculation'
            data = ''
        else:
            print('Error adding calculation to tree model')
        parentindex = QtCore.QModelIndex()
        rows = self.rowCount(parentindex)
        found = 0
        for row in range(rows):
            index = self.index(row, 0, parentindex)
            itemtext = self.data(index, QtCore.Qt.DisplayRole)
            if itemtext == 'Calculations':
                parentindex = index
                subrows = self.rowCount(parentindex)
                for subrow in range(subrows):
                    index = self.index(subrow, 0, parentindex)
                    itemtext = self.data(index, QtCore.Qt.DisplayRole)
                    # noinspection PyUnboundLocalVariable
                    if itemtext == text:
                        found = 1
                        break
        if found == 0:
            parentItem = self.getItem(parentindex)
            # noinspection PyUnboundLocalVariable
            parentItem.appendChild(TreeItem([text, data]
                                            , parentItem))
        self.layoutChanged.emit()

    def recParentValidate(self, index, parenttext):
        """ method that find out is any forefather has the given text in its first column
        :param index: The index of the item in question.
        :param parenttext: the text to be searched for in forefathers"""

        parentIndex = self.parent(index)
        if parentIndex == QtCore.QModelIndex():
            return False
        item = self.getItem(parentIndex)
        itemtext = item.itemData[0]
        if itemtext == parenttext:
            return True
        else:
            answer = self.recParentValidate(parentIndex, parenttext)
            return answer

    # noinspection PyUnresolvedReferences
    def setupModelData(self, parent):
        """ used to set up the model. should be rewritten to use data or setdata to avoid duplication.
        :param parent: The root item in all cases
        :return: nothing
        """
        parents = [parent]
        # Cross sectional data
        parents[0].appendChild(TreeItem(['Model data', 'Containing the model'], parents[0]))
        parents.append(parents[-1].child(parents[-1].childCount() - 1))

        # Data for a given layer
        for i in range(len(self.slenderdata.layer_radii)):
            if i > 0:
                parents.pop()
            parents[1].appendChild(TreeItem(['Data for layer', i + 1], parents[1]))
            parents.append(parents[-1].child(parents[-1].childCount() - 1))
            if hasattr(self.slenderdata, 'layer_type'):
                parents[2].appendChild(ModelTreeItem(['Type', str(self.slenderdata.layer_type[i])], parents[2],type='combo'))

            if hasattr(self.slenderdata, 'layer_radii'):
                parents[2].appendChild(TreeItem(['Radius', str(self.slenderdata.layer_radii[i])], parents[2]))

            if hasattr(self.slenderdata, 'thickness'):
                parents[2].appendChild(TreeItem(['Thickness', str(self.slenderdata.thickness[i])], parents[2]))
            if i>0:
                if hasattr(self.slenderdata, 'gap_ini'):
                    parents[2].appendChild(TreeItem(['Initial gap', str(self.slenderdata.thickness[i])], parents[2]))

            if hasattr(self.slenderdata, 'youngs_modulus'):
                parents[2].appendChild(
                    TreeItem(['Youngs modulus', str(self.slenderdata.youngs_modulus[i])], parents[2]))

            if hasattr(self.slenderdata, "poisson"):
                parents[2].appendChild(
                    TreeItem(["Poisson's ratio", str(self.slenderdata.poisson[i])], parents[2]))

            if hasattr(self.slenderdata, 'fricfac'):
                parents[2].appendChild(TreeItem(['Friction factor', str(self.slenderdata.fricfac[i])], parents[2]))

            #specific for wire layers
            if self.slenderdata.layer_type[i]=='wires':

                if hasattr(self.slenderdata, 'lay_angle'):
                    parents[2].appendChild(TreeItem(['Lay angle', str(self.slenderdata.lay_angle[i])], parents[2]))

                if hasattr(self.slenderdata, 'comp_number'):
                    parents[2].appendChild(
                        TreeItem(['Number of tendons', str(self.slenderdata.comp_number[i])], parents[2]))

                if hasattr(self.slenderdata, 'width'):
                    parents[2].appendChild(TreeItem(['Width', str(self.slenderdata.thickness[i])], parents[2]))


        # Layers for internal and external pressure
        parents.pop()
        parents[1].appendChild(ModelTreeItem(['Internal pressure layer', str(self.slenderdata.intpresslayer)],
                                             parents[1],type='presslayer',nopt=len(self.slenderdata.layer_radii)))
        parents[1].appendChild(ModelTreeItem(['External pressure layer', str(self.slenderdata.extpresslayer)],
                                             parents[1],type='presslayer',nopt=len(self.slenderdata.layer_radii)))
        parents.pop()
        # Data for analysis scenario
        parents[0].appendChild(TreeItem(['Scenario definition', 'Used for checking assumptions'], parents[0]))
        parents.append(parents[0].child(parents[0].childCount() - 1))
        if hasattr(self.slenderdata, 'typical_tension'):
            parents[1].appendChild(TreeItem(['Typical tension', str(self.slenderdata.typical_tension)], parents[1]))
        if hasattr(self.slenderdata, 'typical_curvature'):
            parents[1].appendChild(TreeItem(['Typical curvature', str(self.slenderdata.typical_curvature)], parents[1]))
        if hasattr(self.slenderdata, 'internal_pressure'):
            parents[1].appendChild(TreeItem(['Internal pressure', str(self.slenderdata.internal_pressure)], parents[1]))
        parents[0].appendChild(TreeItem(['Calculations', ''], parents[0]))

class FullResultTreeModel(GenericTreeModel):
    def __init__(self, names, parent=None):
        super(FullResultTreeModel, self).__init__(parent)
        self.rootItem = TreeItem(("Active?", "Item"))
        for i in names:
            self.rootItem.appendChild(TreeItem(['No', i], self.rootItem))
        self.activePlots = []
        self.names = names
        self.colorIndexTaken = []
        self.plotData = []

    def add_result(self, number):
        item = self.rootItem.child(number)
        fname = 'tmp/fullresult_' + str(number) + '.h5'
        f = h5py.File(fname, 'r')
        layer_groups = f[self.names[number] + '/AxialStress']
        layers = len(layer_groups)

        for i in range(layers):
            item.appendChild(TreeItem(['No', 'Layer number %d' % (i + 1)], item))
            subitem = item.child(i)
            subitem.appendChild(TreeItem(['No', 'Axial stress'], subitem))

            subitem.appendChild(TreeItem(['No', 'Local bending stress'], subitem))
            subsubitem = subitem.child(-1)
            ntheta = f[self.names[number] + '/LocalBendingStress'][str(i)].shape[1]
            for j in range(ntheta):
                # assumption: uniform thetas
                subsubitem.appendChild(TreeItem(['No', 'Theta = %f' % float(float(j) * 2 * np.pi /
                                                                        float(ntheta))], subsubitem))
                if f[self.names[number] + '/LocalBendingStress'][str(i)].ndim==3:
                    subsubsubitem = subsubitem.child(j)
                    nphi = f[self.names[number] + '/LocalBendingStress'][str(i)].shape[2]
                    for k in range(nphi):
                        if nphi == 4:
                            # assumption: corners of a square chosen
                            subsubsubitem.appendChild(TreeItem(['No', 'Corner number = %d' % (k + 1)], subsubsubitem))

                        else:
                            # assumption: uniform phi chosen
                            subsubsubitem.appendChild(TreeItem(['No', 'Phi = %f' % float(float(k) * 2 * np.pi /
                                                                                         float(nphi))], subsubsubitem))

            if f[self.names[number] + '/LocalBendingStress'][str(i)].ndim==3:
                subitem.appendChild(TreeItem(['No', 'Friction stress'], subitem))
                subsubitem = subitem.child(-1)

            for j in range(ntheta):
                subsubitem.appendChild(TreeItem(['No', 'Theta = %f' % float(float(j) * 2 * np.pi /
                                                                            float(ntheta))], subsubitem))

            subitem.appendChild(TreeItem(['No', 'Stress resultant'], subitem))
                # Stress resultant
            subsubitem = subitem.child(-1)
            ntheta = f[self.names[number] + '/StressResultant'][str(i)].shape[1]
            for j in range(ntheta):
                # assumption: uniform thetas
                subsubitem.appendChild(TreeItem(['No', 'Theta2 = %f' % float(float(j) * 2 * np.pi /
                                                                            float(ntheta))], subsubitem))
                if f[self.names[number] + '/LocalBendingStress'][str(i)].ndim==3:
                    subsubsubitem = subsubitem.child(j)
                    nphi = f[self.names[number] + '/StressResultant'][str(i)].shape[2]
                    for k in range(nphi):
                        if nphi == 4:
                            # assumption: corners of a square chosen
                            subsubsubitem.appendChild(TreeItem(['No', 'Corner number = %d' % (k + 1)], subsubsubitem))

                        else:
                            # assumption: uniform phi chosen
                            subsubsubitem.appendChild(TreeItem(['No', 'Phi = %f' % float(float(k) * 2 * np.pi /
                                                                                     float(nphi))], subsubsubitem))
            # fill inn here
            if f[self.names[number] + '/LocalBendingStress'][str(i)].ndim==3:
                subitem.appendChild(TreeItem(['No', 'Critical curvature'], subitem))
                subitem.appendChild(TreeItem(['No', 'Stick stress'], subitem))
        f.close()
        self.layoutChanged.emit()

    # noinspection PyMethodMayBeStatic
    def flags(self, index):
        """
        Used to select the way each cell works
        :param index: the index of the item queried
        :return: view get info on items "Type"
        """
        if not index.isValid():
            return 0
        if index.column() == 0:
            return QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled
        else:
            return 0

    def toggle(self, index):
        """method for toggling, changing color of the recursive selection and reporting back which lines that are,
        added removed and the color of the added lines
        :param index: the index of the item being toggled
        """

        # list of colors for distuishible plotting
        colorlist = self.colorlist()

        item = self.getItem(index) #Funker ikke!
        if item.itemData[0] == 'No':
            finalstate = 'Yes'
        elif item.itemData[0] == 'Yes':
            finalstate = 'No'
        elif item.itemData[0] == 'Partly':
            finalstate = 'No'
        else:
            print('Internal error, unknown toggle state in result tree.')
            return
        if not self.colorIndexTaken:
            for i in range(len(colorlist)):
                self.colorIndexTaken.append(0)

        self.toggler(index, finalstate, colorlist)

    def toggler(self, index, finalstate, colorlist):
        """toggles index including children, and build info for plotting
        :param colorlist: list of colors applied in plot and tree_model
        :param finalstate: The selected items final state (Yes or No)
        :param index: the index of the item selected
        """

        # we need to go up to find the parents on the way first.
        # To collate the information we need to retrive data.
        # also we use this walk to change column 0
        item = self.getItem(index)
        item.setData(0, finalstate)
        siblingnumber = [item.row()]
        keep_item = item
        item = item.parent()
        if hasattr(self, 'plotData'):
            del self.plotData
        self.plotData = []

        while item.parent():
            keep_item = item
            siblingnumber.append(item.row())
            nos = False
            yeses = False
            for i in range(item.childCount()):
                if item.child(i).itemData[0] == 'No':
                    nos = True
                elif item.child(i).itemData[0] == 'Yes':
                    yeses = True
                elif item.child(i).itemData[0] == 'Partly':
                    yeses = True
                    nos = True

            if yeses and nos:
                item.setData(0, 'Partly')
            elif yeses:
                item.setData(0, 'Yes')
            elif nos:
                item.setData(0, 'No')
            item = item.parent()

        item = keep_item

        treedata = [item.itemData[1]]
        for i in range(len(siblingnumber) - 1):
            item = item.child(siblingnumber[-(i + 2)])
            treedata.append(item.itemData[1])

        level = len(treedata)
        print('children', item.childCount())
        if item.childCount() > 0:
            self.recursiveplot(item, colorlist, treedata, level, finalstate)
        else:
            self.singleplot(treedata, colorlist, finalstate, item)

        self.layoutChanged.emit()

    def singleplot(self, treedata, colorlist, finalstate, item):
        print('len(treeData', len(treedata))
        if len(treedata) == 3:
            if finalstate == 'Yes':
                self.plotData.append(timehist.PlotData(treedata, colorlist, self.colorIndexTaken, 'Add'))
                self.plotData[-1].update(self.activePlots, self.colorIndexTaken)
            elif finalstate == 'No':
                self.plotData.append(timehist.PlotData(treedata, colorlist, self.colorIndexTaken, 'Remove'))
                self.plotData[-1].update(self.activePlots, self.colorIndexTaken)
        if len(treedata) == 4:
            if finalstate == 'Yes':
                self.plotData.append(timehist.FourLevelPlotData(treedata, colorlist, self.colorIndexTaken, 'Add'))
                self.plotData[-1].update(self.activePlots, self.colorIndexTaken)
            elif finalstate == 'No':
                self.plotData.append(timehist.FourLevelPlotData(treedata, colorlist, self.colorIndexTaken, 'Remove'))
                self.plotData[-1].update(self.activePlots, self.colorIndexTaken)
        if len(treedata) == 5:
            if finalstate == 'Yes':
                self.plotData.append(timehist.FiveLevelPlotData(treedata, colorlist, self.colorIndexTaken, 'Add'))
                self.plotData[-1].update(self.activePlots, self.colorIndexTaken)

            elif finalstate == 'No':
                self.plotData.append(timehist.FiveLevelPlotData(treedata, colorlist, self.colorIndexTaken, 'Remove'))
                self.plotData[-1].update(self.activePlots, self.colorIndexTaken)
        if finalstate == 'Yes':
            item.setColor(self.plotData[-1].color)
        elif finalstate == 'No':
            item.setColor(None)

    def recursiveplot(self, item, colorlist, treedata, level, finalstate):

        level += 1
        children = item.childCount()
        for i in range(children):
            if level > 0:
                data = treedata[0:level]
            else:
                data = treedata
            subitem = item.child(i)
            data.append(subitem.itemData[1])
            subitem.setData(0, finalstate)
            if subitem.childCount() > 0:
                self.recursiveplot(subitem, colorlist, data, level, finalstate)
            else:
                self.singleplot(data, colorlist, finalstate, subitem)

    @staticmethod
    def colorlist():
        return [(103, 0, 13), (63, 0, 125), (127, 39, 4), (0, 68, 27), (8, 48, 107), (165, 15, 21), (84, 39, 143),
                (166, 54, 3), (0, 109, 44), (8, 81, 156), (203, 24, 29), (106, 81, 163), (217, 72, 1),
                (35, 139, 69), (33, 113, 181), (239, 59, 44), (128, 125, 186), (241, 105, 19), (65, 171, 93),
                (66, 146, 198), (251, 106, 74), (158, 154, 200), (253, 141, 60), (116, 196, 118), (107, 174, 214),
                (252, 146, 114), (188, 189, 220), (253, 174, 107), (161, 217, 155), (158, 202, 225)]

    def data(self, index, role):
        """ Do not know. called by the view to get hold of the data I believe
        :param index: the index
        :param role: the role
        :return:the data in display friendly mode
        """
        if not index.isValid():
            return None
        if role == QtCore.Qt.DisplayRole:
            item = index.internalPointer()
            return item.data(index.column())

        if role == QtCore.Qt.ForegroundRole:
            item = index.internalPointer()
            if item.Color():
                color = item.Color()
                # return QtWidgets.QColor.fromRgb(int(color[0]),int(color[1]),int(color[2]))
                return QtGui.QColor.fromRgb(int(color[0]), int(color[1]), int(color[2]))
                # return QtWidgets.QColor.fromRgb(color)

        if role == QtCore.Qt.TextColorRole:
            return QtCore.Qt.blue
        return None

def main():
    """ runs the shit.
    :return:nothing
    """
    app = QtWidgets.QApplication(sys.argv)
    mainwindow = Window()
    mainwindow.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
