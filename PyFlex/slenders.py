"""Copyright (C) 2016 Joakim A. Taby

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""
import numpy as np


class Slender:
    """ A class defining a slender cross section"""

    def __init__(self, layer_radii=None, layer_type=None,
                 thickness=None, gap_ini=None, youngs_modulus=None, poisson=None, fric_fac=None,
                 lay_angle=None, comp_number=0, width=None,
                 intpresslayer=None, extpresslayer=None, typical_tension=None, typical_curvature=None):
        """

        :param layer_radii: Radius of a layer
        :param layer_type: Type of a layer wires or sheath
        :param thickness: Thickness of a layer
        :param gap_ini: Initial gap on the inside of a layer
        :param youngs_modulus: Young's modulus for the material of a layer
        :param poisson: Poissons ratio for the material of layer
        :param fric_fac: Friction factor on the inside of a layer
        :param lay_angle: Lay angle of a wire layer, dummy for sheath layer
        :param comp_number: number of tendons in a wire layer, dummy for sheath layer
        :param width: Width of the tendons in a layer, dummy for sheath layer
        :param typical_tension: Typical tension, used for preliminary analysis
        :param typical_curvature: Typical curvature, used for preliminary analysis
        :param intpresslayer: The layer on which internal pressure is applied
        :param extpresslayer: The layer on which external pressure is applied
        :return: Object of type slender
        """

        if layer_radii:
            tmp = []
            for i in layer_radii:
                tmp.append(float(i))
            self.layer_radii = tmp
        else:
            self.layer_radii = [0.0]

        if layer_type:
            tmp = []
            if len(layer_type) == len(layer_type):
                for i in layer_type:
                    tmp.append(i)
                self.layer_type = tmp
            else:
                print("Cannot interpret the layer type. It is either sheath or wire and has to"
                      "given for each layer.")
        else:
            tmp = []
            for i in range(len(self.layer_radii)):
                tmp.append("wires")
            self.layer_type = tmp

        if thickness:
            tmp = []
            if len(thickness) == len(self.layer_radii):
                for i in thickness:
                    tmp.append(float(i))
                self.thickness = tmp
            elif len(thickness) == 1:
                for i in range(len(self.layer_radii)):
                    tmp.append(float(thickness[0]))
                self.thickness = tmp
            else:
                print("Cannot interpret the thickness. either it has to be defined "
                      "with the length equal to the number of layers, or it has "
                      "to be of length 1.")
        else:
            tmp = []
            for i in range(len(self.layer_radii)):
                tmp.append(0.0)
            self.thickness = tmp

        if gap_ini:
            tmp = []
            if len(gap_ini) == len(self.layer_radii):
                for i in gap_ini:
                    tmp.append(float(i))
                self.gap_ini = tmp
            elif len(gap_ini) == 1:
                for i in range(len(self.layer_radii)):
                    tmp.append(float(gap_ini[0]))
                self.gap_ini = tmp
            else:
                print("Cannot interpret the thickness. either it has to be defined "
                      "with the length equal to the number of layers, or it has "
                      "to be of length 1.")
        else:
            tmp = []
            for i in range(len(self.layer_radii)):
                tmp.append(0.0)
            self.gap_ini = tmp

        if not youngs_modulus:
            self.youngs_modulus = []
            self.youngs_modulusDefault = []
            for i in range(len(self.layer_radii)):
                self.youngs_modulus.append(0.0)
                self.youngs_modulusDefault.append(207 * 10 ** 9)
        elif len(youngs_modulus) == 1:
            self.youngs_modulus = []
            for i in range(len(self.layer_radii)):
                self.youngs_modulus.append(float(youngs_modulus[0]))
        elif len(youngs_modulus) == len(self.layer_radii):
            self.youngs_modulus = youngs_modulus
        else:
            print("Wrong format on Youngs modulus given.")

        if poisson:
            tmp = []
            if len(poisson) == len(self.layer_radii):
                for i in poisson:
                    tmp.append(float(i))
                self.poisson = tmp
            elif len(poisson) == 1:
                for i in range(len(self.layer_radii)):
                    tmp.append(float(poisson[0]))
                self.poisson = tmp
            else:
                print("Cannot interpret the thickness. either it has to be defined "
                      "with the length equal to the number of layers, or it has "
                      "to be of length 1.")
        else:
            tmp = []
            for i in range(len(self.layer_radii)):
                tmp.append(0.0)
            self.poisson = tmp

        if not fric_fac:
            self.fricfac = []
            self.fricfacDefault = []
            for i in range(len(self.layer_radii)):
                self.fricfacDefault.append(0.15)
                self.fricfac.append(0.0)
        elif isinstance(fric_fac, float):
            self.fricfac = []
            for i in range(len(self.layer_radii)):
                self.fricfac.append(fric_fac)
        elif len(fric_fac) == 1:
            self.fricfac = []
            for i in range(len(self.layer_radii)):
                self.fricfac.append(fric_fac)
        elif len(fric_fac) == len(self.layer_radii):
            self.fricfac = fric_fac
        else:
            print("If specified the friction factor should either"
                  " be defined as one number to be used for each interface,"
                  "or for the inner interface of each given layer")

        # specific for wire layers
        if width:
            tmp = []
            if len(width) == len(self.layer_radii):
                for i in width:
                    tmp.append(float(i))
                self.width = tmp
            elif len(width) == 1:
                for i in range(len(self.layer_radii)):
                    tmp.append(float(width[0]))
                self.width = tmp
            else:
                print("Cannot interpret the width. either it has to be defined "
                      "with the length equal to the number of layers, or it has "
                      "to be of length 1.")
        else:
            tmp = []
            for i in range(len(self.layer_radii)):
                tmp.append(0.0)
            self.width = tmp

        if lay_angle:
            tmp = []
            if len(lay_angle) == len(self.layer_radii):
                for i in lay_angle:
                    tmp.append(float(i) * np.pi / 180.0)
                self.lay_angle = tmp
            elif len(lay_angle) == 1:
                for i in range(len(self.layer_radii)):
                    tmp.append(float(lay_angle[0]) * np.pi / 180.0 * float((1 - 2 * (i % 2))))
                self.lay_angle = tmp
            else:
                print("Cannot interpret the layangle. either it has to be defined "
                      "with the length equal to the number of layers, or it has "
                      "to be of length 1.")
        else:
            tmp = []
            for i in range(len(self.layer_radii)):
                tmp.append(0.0)
            self.lay_angle = tmp

        if comp_number:
            if len(comp_number) == len(self.layer_radii):
                self.comp_number = comp_number
            else:
                print("Length of comp_number taken to be %d which is different from the number of "
                      "layers found to be %d." % len(comp_number), len(self.layer_radii))
        else:
            self.comp_number = []
            for i in range(len(self.layer_radii)):
                self.comp_number.append(0.0)

        if intpresslayer:
            self.intpresslayer=intpresslayer
        else:
            self.intpresslayer=1
        if extpresslayer:
            self.extpresslayer=extpresslayer
        else:
            self.extpresslayer=len(self.layer_radii)

        #Scenario parameters

        if typical_tension:
            self.typical_tension = float(typical_tension)
        else:
            self.typical_tension = 0.0

        if typical_curvature:
            self.typical_curvature = float(typical_curvature)
        else:
            self.typical_curvature = 0.0

        self.tendon_area = self.calc_tendon_area()

    def calc_tendon_area(self):
        """A method for calculating the tendon area if tendon, or layer area if sheath

        :rtype : list
        :return:The area of the tendons defined
        """
        area = []
        for i in range(len(self.thickness)):
            if self.layer_type[i] == 'wires':
                area.append(self.thickness[i] *
                                  self.width[i])
            elif self.layer_type[i] == 'sheath':
                area.append(2.0*np.pi*self.layer_radii[i]*
                            self.thickness[i])
            else:
                print('Layer type for layer ',i+1,' read as "',self.layer_type[i],'", must be wires or sheath.' )
        return area

    def num_layers(self):
        """Method for finding the number of layers of the model"""
        num = 0
        if self.lay_angle:
            num = max(len(self.lay_angle), num)
        if self.comp_number:
            num = max(len(self.comp_number), num)
        if self.fricfac:
            num = max(len(self.fricfac), num)
        if self.layer_radii:
            num = max(len(self.layer_radii), num)
        if self.thickness:
            num = max(len(self.thickness), num)
        if self.width:
            num = max(len(self.width), num)
        if self.tendon_area:
            num = max(len(self.tendon_area), num)
        if self.youngs_modulus:
            num = max(len(self.youngs_modulus), num)
        return num
