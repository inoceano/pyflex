# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created: Thu Dec 29 13:22:05 2011
#      by: pyside-uic 0.2.13 running on PySide 1.0.9
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(573, 468)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.vboxlayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.vboxlayout.setContentsMargins(0, 0, 0, 0)
        self.vboxlayout.setSpacing(0)
        self.vboxlayout.setObjectName("vboxlayout")
        self.view = QtWidgets.QTreeView(self.centralwidget)
        checkValues= ['to be updated','2b updt']
        self.view.setEditTriggers(QtWidgets.QAbstractItemView.CurrentChanged)
        self.view.setItemDelegate(ComboDelegate(self))
        self.view.setAlternatingRowColors(True)
        self.view.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectItems)
        self.view.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.view.setAnimated(False)
        self.view.setAllColumnsShowFocus(True)
        self.view.setObjectName("view")
        self.view.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.view.setDragDropMode(QtWidgets.QAbstractItemView.DragOnly)
        self.vboxlayout.addWidget(self.view)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 573, 31))
        self.menubar.setObjectName("menubar")
        self.fileMenu = QtWidgets.QMenu(self.menubar)
        self.fileMenu.setObjectName("fileMenu")
        self.importMenu = QtWidgets.QMenu(self.menubar)
        self.importMenu.setObjectName("importMenu")
        self.helpMenu = QtWidgets.QMenu(self.menubar)
        self.helpMenu.setObjectName("helpMenu")
        MainWindow.setMenuBar(self.menubar)
        self.newAction = QtWidgets.QAction(MainWindow)
        self.newAction.setObjectName("newAction")
        self.openAction = QtWidgets.QAction(MainWindow)
        self.openAction.setObjectName("openAction")
        self.exitAction = QtWidgets.QAction(MainWindow)
        self.exitAction.setObjectName("exitAction")
        self.importAction = QtWidgets.QAction(MainWindow)
        self.importAction.setObjectName("importAction")
        self.importFolderAction = QtWidgets.QAction(MainWindow)
        self.importFolderAction.setObjectName("importFolderAction")
        self.aboutAction = QtWidgets.QAction(MainWindow)
        self.aboutAction.setObjectName("aboutAction")
        self.fileMenu.addAction(self.newAction)
        self.fileMenu.addAction(self.openAction)
        self.fileMenu.addAction(self.exitAction)
        self.importMenu.addAction(self.importAction)
        self.importMenu.addAction(self.importFolderAction)
        self.helpMenu.addAction(self.aboutAction)
        self.menubar.addAction(self.fileMenu.menuAction())
        self.menubar.addAction(self.importMenu.menuAction())
        self.menubar.addAction(self.helpMenu.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    # noinspection PyCallByClass
    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(
            QtWidgets.QApplication.translate("MainWindow", "PyFlex", None))
        MainWindow.setWindowIcon(QtGui.QIcon('../icons/logo_small.png'))
        self.fileMenu.setTitle(
            QtWidgets.QApplication.translate("MainWindow", "&File", None))
        self.importMenu.setTitle(
            QtWidgets.QApplication.translate("MainWindow", "&Import", None))
        self.helpMenu.setTitle(
            QtWidgets.QApplication.translate("MainWindow", "&Help", None))
        self.newAction.setText(QtWidgets.QApplication.translate("MainWindow", "New", None))
        self.newAction.setShortcut(
            QtWidgets.QApplication.translate("MainWindow", "Ctrl+N", None))
        self.openAction.setText(
            QtWidgets.QApplication.translate("MainWindow", "Open", None))
        self.openAction.setShortcut(
            QtWidgets.QApplication.translate("MainWindow", "Ctrl+O", None))
        self.exitAction.setText(
            QtWidgets.QApplication.translate("MainWindow", "E&xit", None))
        self.exitAction.setShortcut(
            QtWidgets.QApplication.translate("MainWindow", "Ctrl+Q", None))
        self.importAction.setText(
            QtWidgets.QApplication.translate("MainWindow", "Import time series", None))
        self.importAction.setShortcut(
            QtWidgets.QApplication.translate("MainWindow", "Ctrl+I", None))
        self.importFolderAction.setText(QtWidgets.QApplication.translate("MainWindow", "Import time series folder", None))
        self.importFolderAction.setShortcut(
            QtWidgets.QApplication.translate("MainWindow", "Ctrl+Shift+I", None))
        self.importFolderAction.setToolTip('Imports all tim files in folder')
        self.importFolderAction.hovered.connect(self.handleMenuHovered)
        self.aboutAction.setText(
            QtWidgets.QApplication.translate("MainWindow", "About", None))

    def handleMenuHovered(self):
        # noinspection PyArgumentList
        QtWidgets.QToolTip.showText(
            QtGui.QCursor.pos(), self.importFolderAction.toolTip(), self)


class GenericTreeModel(QtCore.QAbstractItemModel):
    """Superclass for all tree models in the project

    List of methods that needs to be implemented when inherting this class:
    flags, """

    def __init__(self, parent=None):
        super(GenericTreeModel, self).__init__(parent)

    def columnCount(self, parent):
        if parent.isValid():
            return parent.internalPointer().columnCount()
        else:
            return self.rootItem.columnCount()

    def getItem(self, index):
        """
        Method for getting hold of the item from its index
        :param index: the index of the item
        :return:the pointer to the item itself
        """
        if index.isValid():
            item = index.internalPointer()
            if item:
                return item

        return self.rootItem

    def data(self, index, role):
        """ Do not know. called by the view to get hold of the data I believe
        :param index: the index
        :param role: the role
        :return:the data in display friendly mode
        """
        if not index.isValid():
            return None
        if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
            item = index.internalPointer()
            return item.data(index.column())
        elif role == QtCore.Qt.CheckStateRole:
            return None
        return None

    # noinspection PyUnresolvedReferences
    def setData(self, index, value, role=QtCore.Qt.EditRole):
        """
        :param index: the index of the item queried
        :param value: the new value of the data
        :param role: which role the view queries. only EditRole relevant
        :return: returns the result. Should be the data put into some item.
        """
        if role != QtCore.Qt.EditRole:
            return False

        item = self.getItem(index)
        result = item.setData(index.column(), value)

        if result:
            self.dataChanged.emit(index, index)

        return result

    # noinspection PyMethodMayBeStatic
    def supportedDropActions(self):
        return QtCore.Qt.CopyAction | QtCore.Qt.MoveAction

    def headerData(self, section, orientation, role):
        """
        :param section: do not know
        :param orientation: no idea
        :param role: in which context the view queries info
        :return:headers mode
        """
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.rootItem.data(section)
        return None

    def index(self, row, column, parent):
        """
        :param row: the row of the tree model
        :param column: which column of the tree model
        :param parent: the parent of the item
        :return:the index of the item
        """
        if not self.hasIndex(row, column, parent):
            return QtCore.QModelIndex()
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()

    def parent(self, index):
        """
        :param index: the index oof the item
        :return: the index of the parent of the item
        """
        if not index.isValid():
            return QtCore.QModelIndex()
        childItem = index.internalPointer()
        parentItem = childItem.parent()
        if parentItem == self.rootItem:
            return QtCore.QModelIndex()
        return self.createIndex(parentItem.row(), 0, parentItem)

    def removeRows(self, position, rows, parent=QtCore.QModelIndex()):
        parentItem = self.getItem(parent)

        self.beginRemoveRows(parent, position, position + rows - 1)
        success = parentItem.removeChildren(position, rows)
        self.endRemoveRows()

        return success

    def rowCount(self, parent):
        """ returns the number of children of the parent. sounds like it should return the number of rows in total...
        :param parent: the parent of something.
        :return:
        """
        if parent.column() > 0:
            return 0
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
        return parentItem.childCount()

    def insertRows(self, position, rows, parent=QtCore.QModelIndex()):
        parentItem = self.getItem(parent)
        self.beginInsertRows(parent, position, position + rows - 1)
        success = parentItem.insertChildren(position, rows,
                                            self.rootItem.columnCount())
        self.endInsertRows()

        return success

class ComboDelegate(QtWidgets.QStyledItemDelegate):
    """
    A delegate that places a fully functioning QComboBox in every
    cell of the column to which it's applied
    """
    updateModelandDataSignal = QtCore.Signal()
    def __init__(self, parent):
        super(ComboDelegate,self).__init__(parent)

    def createEditor(self, parent, option, index):
        item=index.model().getItem(index)
        # enters here if the item has type. For now only
        # valid for the layer type.
        if hasattr(item,'type'):
            if item.type=='combo' or item.type=='presslayer':
                combo = QtWidgets.QComboBox(parent)
                combo.addItems(item.options)
                for i in range(len(item.options)):
                    if item.options[i] == item.data(1):
                        combo.setCurrentIndex(i)
                self.connect(combo, QtCore.SIGNAL("currentIndexChanged(int)"), self, QtCore.SLOT("currentIndexChanged()"))
                return combo

        return super(ComboDelegate,self).createEditor(parent, option,index)

    def setEditorData(self, editor, index):
        editor.blockSignals(True)
        item=index.model().getItem(index)
        if hasattr(item,'type'):
            if item.type=='combo' or item.type=='presslayer':
                for i in range(len(item.options)):
                    if item.options[i]==item.data:
                        editor.setCurrentIndex(i)
        else:
            super(ComboDelegate,self).setEditorData(editor, index)

    def setModelData(self, editor, model, index):
        item=index.model().getItem(index)
        if hasattr(item,'type'):
            if item.type=='combo' or item.type=='presslayer':
                model.setData(index, editor.currentText())
                self.updateModelandDataSignal.emit()
                return
        super(ComboDelegate,self).setModelData(editor, model, index)


    @QtCore.Slot()
    def currentIndexChanged(self):
        self.commitData.emit(self.sender())

