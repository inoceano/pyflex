"""Copyright (C) 2016 Joakim A. Taby

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""

import slenders


def analytical_input(inputfile, delimiter, commentchar):
    # All needed input
    obligatory_keyword = ["LayerType"]
    optional_keyword = ["TypicalCurvature", "LayerRadii", "TypicalTension",
                        "LayAngle", "CompNumber", "YoungsModulus","Thickness",
                        "Width","Poisson","InitialGap","FrictionFactor",
                        "InternalPressureLayer","ExternalPressureLayer"]
    # initialize all optionals to None

    layer_radii = None
    layer_type = None
    thickness = None
    gap_ini = None
    youngs_modulus = None
    poisson = None
    fric_fac = None
    lay_angle = None
    comp_number = None
    width = None
    intpresslayer = None
    extpresslayer = None
    typical_curvature = None
    typical_tension = None

    # reading all the input obviously
    inputfile_object = open(inputfile, 'r')
    tmp_input_content = inputfile_object.readlines()
    input_content=[]
    for line in tmp_input_content:
        tmp=line.split(commentchar,1)[0]
        if len(tmp)>0:
            input_content.append(line.split(commentchar,1)[0])
    comment = 0
    for line in input_content:
        if line[0] == commentchar:
            comment += 1
        elif line[0:1] == "\n":
            comment += 1

        elif line.find("LayerRadii") > -1:
            index = line.find(delimiter)
            tmp = line[index + 1: len(line) - 1]
            tmp = tmp.split(",")
            count = 0
            layer_radii = []
            for i in tmp:
                count += 1
                layer_radii.append(float(i))

        elif line.find("LayerType") > -1:
            index = line.find(delimiter)
            tmp = line[index + 1: len(line) - 1]
            tmp = tmp.split(",")
            count = 0
            layer_type = []
            for i in tmp:
                count += 1
                layer_type.append(str(i).strip())

        elif line.find("Thickness") > -1:
            index = line.find(delimiter)
            tmp = line[index + 1: len(line) - 1]
            tmp = tmp.split(",")
            count = 0
            thickness = []
            for i in tmp:
                count += 1
                thickness.append(float(i))

        elif line.find("InitialGap") > -1:
            index = line.find(delimiter)
            tmp = line[index + 1: len(line) - 1]
            tmp = tmp.split(",")
            count = 0
            gap_ini = []
            for i in tmp:
                count += 1
                gap_ini.append(float(i))

        elif line.find("YoungsModulus") > -1:
            index = line.find(delimiter)
            tmp = line[index + 1: len(line) - 1]
            tmp = tmp.split(",")
            count = 0
            youngs_modulus = []
            for i in tmp:
                count += 1
                youngs_modulus.append(float(i))

        elif line.find("Poisson") > -1:
            index = line.find(delimiter)
            tmp = line[index + 1: len(line) - 1]
            tmp = tmp.split(",")
            count = 0
            poisson = []
            for i in tmp:
                count += 1
                poisson.append(float(i))

        elif line.find("FrictionFactor") > -1:
            index = line.find(delimiter)
            tmp = line[index + 1: len(line) - 1]
            tmp = tmp.split(",")
            count = 0
            fric_fac = []
            for i in tmp:
                count += 1
                fric_fac.append(float(i))

        elif line.find("LayAngle") > -1:
            index = line.find(delimiter)
            tmp = line[index + 1:len(line) - 1]
            tmp = tmp.split(",")
            count = 0
            lay_angle = []
            for i in tmp:
                count += 1
                lay_angle.append(float(i))

        elif line.find("CompNumber") > -1:
            index = line.find(delimiter)
            tmp = line[index + 1:len(line) - 1]
            tmp = tmp.split(",")
            count = 0
            comp_number = []
            for i in tmp:
                count += 1
                comp_number.append(float(i))

        elif line.find("Width") > -1:
            index = line.find(delimiter)
            tmp = line[index + 1:len(line) - 1]
            tmp = tmp.split(",")
            count = 0
            width = []
            for i in tmp:
                count += 1
                width.append(float(i))

        elif line.find("InternalPressureLayer") > -1:
            index = line.find(delimiter)
            intpresslayer = int(line[index + 1:len(line)])

        elif line.find("ExternalPressureLayer") > -1:
            index = line.find(delimiter)
            extpresslayer = int(line[index + 1:len(line)])

        elif line.find("TypicalTension") > -1:
            index = line.find(delimiter)
            typical_tension = float(line[index + 1:len(line)])

        elif line.find("TypicalCurvature") > -1:
            index = line.find(delimiter)
            typical_curvature = float(line[index + 1:len(line)])

        else:
            tmp = line.split(delimiter)
            print('Keyword "%s" not recognized' % (tmp[0]))
        inputfile_object.close()
    slender = slenders.Slender(layer_radii, layer_type,
                 thickness, gap_ini, youngs_modulus, poisson, fric_fac,
                 lay_angle, comp_number, width,
                 intpresslayer, extpresslayer,
                 typical_tension, typical_curvature)

    return slender


def parser(arguments, delimiter, commentchar):
    if arguments[1] == "-a":
        # analytical approach
        print("Analytical results anticipated")
        slender = analytical_input(arguments[2], delimiter, commentchar)
        return slender
    elif arguments[1] == "n":
        print("Results from local analyses anticipated")
    else:
        print("Not a valid option (yet)")
        # results from numerical analysis provided

