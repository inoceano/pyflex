"""Copyright (C) 2016 Joakim A. Taby

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""
from unittest import TestCase

import coefficients
import slenders
import numpy as np


# noinspection PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences,PyUnresolvedReferences
class TestCoefficients(TestCase):
    """ testing the coefficients module
    """

    def test_analytical_contact_pressures(self):
        # test from Master thesis of Frederika Nugteren.
        alpha = np.array([-86.1*np.pi/180, 87.6*np.pi/180, 30.0*np.pi/180, -30.0*np.pi/180])
        R = np.array([           108.1e-3,         113e-3,       118.2e-3,        124.6e-3])
        A = np.array([           218.7e-6,        45.3e-6,          60e-6,           60e-6])
        n = np.array([                  2,              2,             50,              53])
        p_ext = 0.0
        p_int = 0.0
        t = np.array([4.0e-3,4.0e-3,5.0e-3,5.0e-3])
        sigma=np.array([[  3.45189159e+08,   3.30322233e+08,   3.16835196e+08,   3.12779821e+08],
                        [  3.37408352e+08,   3.22703186e+08,   3.58052949e+08,   3.54092508e+08],
                        [ -7.78080666e+06,  -7.61904752e+06,   4.12177525e+07,   4.13126878e+07],
                        [  3.74934599e+08,   3.58683549e+08,   3.72876786e+08,   3.68474047e+08],
                        [  5.38704293e+08,   5.15502879e+08,   4.94454927e+08,   4.88126084e+08]])

        contactforce1 = coefficients.analytical_contact_pressures(sigma,alpha,n,A,R,t,p_int,p_ext)
        equality_test=np.allclose(contactforce1,
                [[  4.70708766e+07,   1.57487585e+07,   6.38244872e+06,   3.02105496e+06],
                 [  4.70708766e+07,   1.64120719e+07,   7.21908279e+06,   3.42008293e+06],
                 [  3.69746936e-04,   6.63313434e+05,   8.36634074e+05,   3.99027981e+05],
                 [  5.17575871e+07,   1.77110161e+07,   7.51513247e+06,   3.55899030e+06],
                 [  7.34590952e+07,   2.45776079e+07,   9.96048816e+06,   4.71467668e+06]])


        self.assertTrue(equality_test)
        contactforce2 = coefficients.analytical_contact_pressures(sigma[2,:],alpha,n,A,R,t,p_int,p_ext)
        equality_test=np.allclose(contactforce2,
            [[3.69746936e-04,   6.63313434e+05,   8.36634074e+05,   3.99027981e+05]])
        self.assertTrue(equality_test)


    def test_critical_curvature(self):
        errorfraction = 1.0e-6
        s = slenders.Slender([["5.15", "25"], ["10"]], youngs_modulus=[207e9],
                             lay_angle=[35, 45], layer_radii=[1, 2], comp_number=[1, 1])
        # checking for no given lay_angle
        contactforce = np.array([9, 19])
        critcurv = coefficients.critical_curvature(s, contactforce,
                                                   frictionfactors=np.array([0.1]))
        self.assertAlmostEqual(critcurv.max(), 0.0, delta=errorfraction)
        # checking with some numbers
        slenders.Slender([["5.15", "25"], ["10"]], youngs_modulus=[207e9],
                         lay_angle=[45], layer_radii=[1, 2], comp_number=[1, 1])
        critcurv = coefficients.critical_curvature(s, contactforce,
                                                   frictionfactors=np.array([0.1]))
        equality_test = np.allclose(critcurv, np.array([9.55146349e-14 - 1.21781917e-13]))
        self.assertTrue(equality_test)
        # Comparing with first project of Inoceano
        tens = np.array([5000])
        s = slenders.Slender([[5.15e-3, 2.0e-3], [5.15e-3, 2.0e-3]],
                             lay_angle=[20],
                             comp_number=[51, 46], youngs_modulus=[210.0e9],
                             fric_fac=0.15, layer_radii=[46.925e-3, 41.775e-3])
        contactforce = np.array([8399.06251, 15250.90005])
        critcurv = coefficients.critical_curvature(s, contactforce)
        equality_test = np.allclose(critcurv, [  1.08610516e-05,   7.00385091e-06])
        self.assertTrue(equality_test)

    def test_local_bending_sympy(self):
        s = slenders.Slender([[5.15, 25], [5.15e-3]])
        bendcoeff = coefficients.local_bending_sympy(s)
        self.assertEqual(len(bendcoeff[0]), 4)
        # More tests should be written if this is used in its current form

    def test_local_bending(self):
        s = slenders.Slender([[5.15e-3 / 2], [5.15e-3 / 2]], lay_angle=[20],
                             comp_number=[51, 46], youngs_modulus=[210.0e9])
        bendcoeff = coefficients.local_bending(s.layergeometry, s.lay_angle,
                                               np.arange(0, 2 * np.pi, 2 * np.pi / 16).reshape(-1, 1),
                                               np.arange(0, 2 * np.pi, 2 * np.pi / 16),
                                               s.youngs_modulus, bend_model="tan")
        self.assertEqual(np.max(np.max(bendcoeff[0])), 540750000.0)
        bendcoeff = coefficients.local_bending(s.layergeometry, s.lay_angle,
                                               np.arange(0, 2 * np.pi, 2 * np.pi / 16).reshape(-1, 1),
                                               np.arange(0, 2 * np.pi, 2 * np.pi / 16),
                                               s.youngs_modulus)
        self.assertAlmostEqual(np.max(np.max(bendcoeff[0])), 567579730.86247504, delta=0.0001)

    def test_tensstresshist(self):
        t_eff = np.array([0.0,    220.6e3, 220.6e3, 131.1e3,      0])
        p_int =np.array([46.2e6,  46.2e6,     0.0,  50.8e6, 72.1e6])
        p_ext = np.array([  0.0,     0.0,     0.0 ,    0.0,    0.0])

        alpha = np.array([-86.1*np.pi/180, 87.6*np.pi/180, 30.0*np.pi/180, -30.0*np.pi/180])
        R = np.array([           108.1e-3,         113e-3,       118.2e-3,        124.6e-3])
        thick = np.array([            0.0,            0.0,            0.0,             0.0])
        A = np.array([           218.7e-6,        45.3e-6,          60e-6,           60e-6])
        E = np.array([            200.0e9,        200.0e9,        200.0e9,         200.0e9])
        n = np.array([                  2,              2,             50,              53])

        stresshist = coefficients.tension_stress_history(t_eff, p_int, p_ext, alpha, R, thick, A, E, n)
        equality_test = np.allclose(stresshist,
                                    np.array([[  3.45189159e+08,   3.30322233e+08,   3.16835196e+08,   3.12779821e+08],
                                              [  3.37408352e+08,   3.22703186e+08,   3.58052949e+08,   3.54092508e+08],
                                              [ -7.78080666e+06,  -7.61904752e+06,   4.12177525e+07,   4.13126878e+07],
                                              [  3.74934599e+08,   3.58683549e+08,   3.72876786e+08,   3.68474047e+08],
                                              [  5.38704293e+08,   5.15502879e+08,   4.94454927e+08,   4.88126084e+08]]))
        self.assertTrue(equality_test)

    def test_bendingstresshist(self):
        curvhist = (-0.5 + np.random.random([15, 2])) * 0.05
        # testing rectangular tendons
        s = slenders.Slender([[5.15e-3 / 2.0, 10e-3], [5.15e-3 / 2.0, 10e-3]], lay_angle=[20],
                             comp_number=[51, 46], youngs_modulus=[210.0e9])
        stresshist = coefficients.bending_stress_history(s, curvhist, ntheta=8,
                                                         nphi=16, bend_model="tan")
        self.assertEqual(stresshist[0].shape, (15, 8, 4))
        # Data from first project of Inoceano
        s = slenders.Slender([[5.15e-3 / 2.0], [5.15e-3 / 2.0]], lay_angle=[20],
                             comp_number=[51, 46], youngs_modulus=[210.0e9])
        stresshist = coefficients.bending_stress_history(s, curvhist, ntheta=8,
                                                         nphi=8, bend_model="tan")
        self.assertEqual(stresshist[0].shape, (15, 8, 8))

        curvhist = np.array([[1, 0], [0, 1]])
        stresshist = coefficients.bending_stress_history(s, curvhist, ntheta=8, nphi=8,
                                                         bend_model="tan")
        self.assertEqual(stresshist[0][0, 0, 0], stresshist[1][1, 2, 0])
