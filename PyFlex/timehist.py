"""Copyright (C) 2016 Joakim A. Taby

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""
import os
import fnmatch
import numpy as np
import operator
import math
#import matplotlib
from matplotlib.figure import Figure
import functools
import h5py
from PySide2 import QtGui, QtCore, QtWidgets

#matplotlib.rcParams['backend'] = "Qt5Agg"
#matplotlib.rcParams['backend.Qt5'] = "Pyside2"
# noinspection PyPep8
from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)
from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas)
# noinspection PyPep8
import coefficients
import axisymmetric
# noinspection PyPep8
import gc

"""Module for handling time series"""


class TimeHist(object):
    """
    Defining a time series object
    :param object:
    """

    def __init__(self, time_filename, histname, directory=None):
        self.timeFileName = time_filename
        self.name = histname
        # initiation of instance variables other methods.
        # plot
        self.canvas = None
        self.mpl_toolbar = None
        if directory is not None:
            self.directory = directory
        if self.timeFileName[-3:] == 'tim':
            if directory is not None:
                self.timehist = self.set_from_tim(directory)
            else:
                self.timehist = self.set_from_tim('')

    def set_from_tim(self, directory):
        """
        Reads in a tim file or directory and returns it
        :param directory: The directory where the file is located
        :return: The time history in a matrix as follows: [Time Tension Curvy Curvz] X nsteps
        """
        if directory:
            fid = open(directory + '/' + self.timeFileName, "r")
        else:
            fid = open(self.timeFileName, "r")
        headercheck = fid.readline()
        headercheck = headercheck.lower()
        if headercheck[:3] == '# #':
            tdict = dict(timeindex=headercheck.find('time'),
                         tensionindex=headercheck.find('tension'),
                         curvyindex=headercheck.find('curvy'),
                         curvzindex=headercheck.find('curvz'),
                         p_intindex=headercheck.find('p_int'),
                         p_extindex=headercheck.find('p_ext'))
            sorted_dict = sorted(tdict.items(), key=operator.itemgetter(1))
            dictionary = {}
            for i in range(len(sorted_dict)):
                dictionary[sorted_dict[i][0]] = i
        else:
            dictionary = dict(timeindex=0,
                              tensionindex=1,
                              curvyindex=2,
                              curvzindex=3,
                              p_intindex=4,
                              p_extindex=5)
        if directory:
            unsorted = np.loadtxt(directory + '/' + self.timeFileName)
        else:
            unsorted = np.loadtxt(self.timeFileName)

        timehist = np.zeros([len(unsorted[:, 0]), 6])
        timehist[:, 0] = unsorted[:, dictionary['timeindex']]
        timehist[:, 1] = unsorted[:, dictionary['tensionindex']]
        timehist[:, 2] = unsorted[:, dictionary['curvyindex']]
        timehist[:, 3] = unsorted[:, dictionary['curvzindex']]
        timehist[:, 4] = unsorted[:, dictionary['p_intindex']]
        timehist[:, 5] = unsorted[:, dictionary['p_extindex']]
        fid.close()
        return timehist

    def plot(self, parent):
        """
        Method for plotting itself onto the parent widget.
        :param parent:A widget where the canvas will be placed
        """
        fig = matplotlib.figure.Figure()
        self.canvas = FigureCanvas(fig)
        ax1 = fig.add_subplot(211)
        ax1.plot(self.timehist[:, 0], self.timehist[:, 1])
        ax1.set_title('Tension')
        ax2 = fig.add_subplot(212)
        ax2.plot(self.timehist[:, 0], self.timehist[:, 2], self.timehist[:, 0], self.timehist[:, 3])
        ax2.set_title('Curvatures')
        fig.suptitle('Time series %s' % str(self.name))
        self.mpl_toolbar = NavigationToolbar(self.canvas, parent)
        # Need to make this work also if one does not have the right window open.

        temp = QtWidgets.QWidget()
        templayout = QtWidgets.QVBoxLayout()
        temp.setLayout(templayout)
        templayout.addWidget(self.mpl_toolbar)
        templayout.addWidget(self.canvas)
        parent.addTab(temp, 'Plot of %s' % str(self.name))
        parent.setCurrentWidget(temp)


class FrictionCalc(object):
    """Class for holding data for friction calculations"""

    def __init__(self):
        self.timehist = None
        self.labels = []
        self.startSlider = []
        self.startSpinbox = []
        self.endSlider = []
        self.endSpinbox = []
        self.enabled = []
        # Initializing attributes difined elsewhere
        # setup_tab
        self.mainwidget = None
        # setup setup_ts_edit
        self.startTime = None
        self.stopTime = None
        # calculate
        self.startindex = None
        self.stopindex = None
        self.time = None
        self.bendStddev = None
        self.bendMax = None
        self.bendMin = None
        self.bendStress = None
        self.tension = None
        self.p_int = None
        self.p_ext = None
        self.stickStress = None
        self.stickStddev = None
        self.stickMax = None
        self.stickMin = None
        self.axialStress = None
        self.axialStddev = None
        self.axialMax = None
        self.axialMin = None
        # plot
        self.mpl_toolbar = None
        self.canvas = plot = None
        # setup_tab

    def add_ts(self, ts):
        """ Adding/changing a timeseries obviously.
        :param ts: A time series
        """
        self.timehist = ts

    def setup_tab(self, window):
        """
        Setting up a widget where the input to the friction calculation will be shown.
        Needs to be placed into the application on the outside to be shown.
        :param window: The main window of the application. Adds itself to it and uses its eventfilter only.
        :return:
        """
        self.mainwidget = QtWidgets.QWidget()
        self.mainwidget.setAcceptDrops(True)
        self.mainwidget.installEventFilter(window)
        mainlayout = QtWidgets.QVBoxLayout()
        mainlayout.setObjectName('mainlayout')
        self.mainwidget.setLayout(mainlayout)
        frictiontitle = QtWidgets.QLabel('<H1> Friction calculations </H1>')
        mainlayout.addWidget(frictiontitle)
        # Adding a layout for adding the timeseries
        # making gridlayout including all timeseries
        tslayout = QtWidgets.QGridLayout()
        tslayout.setObjectName('tslayout')
        header_name = QtWidgets.QLabel('Name')
        tslayout.addWidget(header_name, 1, 0)
        header_starttime = QtWidgets.QLabel('Start time')
        tslayout.addWidget(header_starttime, 1, 1, columnSpan=2)
        header_stoptime = QtWidgets.QLabel('Stop time')
        tslayout.addWidget(header_stoptime, 1, 3, columnSpan=2)
        use_constant_tension = QtWidgets.QCheckBox('Use constant tension')
        use_constant_tension.setObjectName('use_constant_tension')
        use_constant_tension.setToolTip('From scenario definition')
        if not hasattr(window.tree_model.slenderdata, 'typical_tension'):
            use_constant_tension.setEnabled(False)
        tslayout.addWidget(use_constant_tension, 0, 1)
        use_constant_curvature = QtWidgets.QCheckBox('Use constant curvature')
        use_constant_curvature.setObjectName('use_constant_curvature')
        use_constant_curvature.setToolTip('From scenario definition')
        if not hasattr(window.tree_model.slenderdata, 'typical_curvature'):
            use_constant_curvature.setEnabled(False)
        tslayout.addWidget(use_constant_curvature, 0, 2)
        mainlayout.addLayout(tslayout)
        # Add helping text
        dragdrop = QtWidgets.QLabel('<font color="grey">Drag and drop time series here</font>')
        dragdrop.setObjectName('dragdrop')
        mainlayout.addWidget(dragdrop)
        # Adding calculate buttons and plot button.
        calcbutton = QtWidgets.QPushButton('Calculate')
        calcbutton.setObjectName('calcbutton')
        calcbutton.setEnabled(False)
        mainlayout.addWidget(calcbutton)
        calcbutton.clicked.connect(window.calculate_friction)

    def setup_ts_edit(self):
        """
        Setting up the widget for editing the timeseries. Called when a time series is dropped.
        """
        rows = 2
        # filling up the widgets

        self.stopTime = self.timehist.timehist[-1, 0]
        self.startTime = self.timehist.timehist[0, 0]
        tslayout = self.mainwidget.findChild(QtWidgets.QGridLayout, 'tslayout')
        if not hasattr(tslayout, 'made'):
            label = QtWidgets.QLabel(self.timehist.name)
            label.setObjectName('label')
            tslayout.addWidget(label, rows, 0)
        else:
            label = self.mainwidget.findChild(QtWidgets.QLabel, 'label')
            label.setText(self.timehist.name)

        # Selection of starttime
        if not hasattr(tslayout, 'made'):
            self.startSlider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
            tslayout.addWidget(self.startSlider, rows, 1)
        self.startSlider.setRange(self.startTime,
                                  math.ceil(self.stopTime))

        if not hasattr(tslayout, 'made'):
            self.startSpinbox = QtWidgets.QDoubleSpinBox()
            tslayout.addWidget(self.startSpinbox, rows, 2)
        self.startSpinbox.setRange(self.startTime,
                                   self.stopTime)
        self.startSlider.valueChanged.connect(self.startslider_changed)
        self.startSpinbox.valueChanged.connect(self.startspinbox_changed)

        if not hasattr(tslayout, 'made'):
            self.endSlider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
            tslayout.addWidget(self.endSlider, rows, 3)
        self.endSlider.setRange(self.startTime,
                                math.ceil(self.stopTime))
        if not hasattr(tslayout, 'made'):
            self.endSpinbox = QtWidgets.QDoubleSpinBox()
            tslayout.addWidget(self.endSpinbox, rows, 4)
        self.endSpinbox.setRange(self.startTime,
                                 self.stopTime)
        self.endSlider.valueChanged.connect(self.endslider_changed)
        self.endSpinbox.valueChanged.connect(self.endspinbox_changed)

        self.endSlider.setValue(self.stopTime)
        self.endSpinbox.setValue(self.stopTime)
        calcbutton = self.mainwidget.findChild(QtWidgets.QPushButton, 'calcbutton')
        calcbutton.setEnabled(True)
        tslayout.made = True

    def calculate(self, model):
        """
        Calculates stress time histories based on given timehistory and model.
        :param model:A model of slenders.Slender type
        """
        # finding time index
        self.startindex = np.argmax(self.timehist.timehist[:, 0] > self.startTime)
        self.startindex -= 1
        self.stopindex = np.argmax(self.timehist.timehist[:, 0] >= self.stopTime)
        if self.stopindex < self.startindex:
            self.stopindex = len(self.time)
        # finding which part of the curve to use
        use_constant_curvature = self.mainwidget.findChild(QtWidgets.QCheckBox, 'use_constant_curvature')
        if use_constant_curvature.isChecked():
            curv = np.array([[model.typical_curvature, 0.0]])
        else:
            tmp = [self.timehist.timehist[self.startindex:self.stopindex+1, 2],
                   self.timehist.timehist[self.startindex:self.stopindex+1, 3]]
            curv = np.array(tmp)
            curv = np.transpose(curv)
        # dealing with local bending stresses
        self.time = self.timehist.timehist[self.startindex:self.stopindex+1, 0]
        nphi = 16
        ntheta = 16
        all_bend_stress = coefficients.bending_stress_history(model, curv, nphi=nphi, ntheta=ntheta)
        # finding bendingstress max and standard deviation
        self.bendStddev = []
        self.bendMax = []
        self.bendMin = []
        self.bendStress = []
        thetaindex = []
        phiindex = []
        theta = []
        for i in all_bend_stress:
            i = np.reshape(i, (-1, ntheta, i.shape[-1]))
            all_bend_stddev = np.std(i, axis=0)
            self.bendStddev.append(np.amax(np.amax(all_bend_stddev)))
            self.bendMax.append(np.amax(np.amax(np.amax(i))))
            self.bendMin.append(np.amin(np.amin(np.amin(i))))
            tmp = np.unravel_index(all_bend_stddev.argmax(), all_bend_stddev.shape)
            self.mainwidget.findChild(QtWidgets.QCheckBox, 'use_constant_curvature')
            if use_constant_curvature.isChecked():
                self.bendStress.append(self.bendMax)
            else:
                self.bendStress.append(i[:, tmp[0], tmp[1]])
            thetaindex.append(tmp[0])
            theta.append(tmp[0] * 2 * np.pi / ntheta)
            # phi is either corner number or angle around the crossection depending on the geometry
            phiindex.append(tmp[1])
        print('Contact pressures calculated by analytical means')
        use_constant_tension = self.mainwidget.findChild(QtWidgets.QCheckBox, 'use_constant_tension')
        if use_constant_tension.isChecked():
            self.tension = np.array([model.typical_tension])
        else:
            self.tension = np.array(self.timehist.timehist[self.startindex:self.stopindex+1, 1])
        t_eff = self.tension
        p_int = self.timehist.timehist[self.startindex:self.stopindex+1,4]
        p_ext = self.timehist.timehist[self.startindex:self.stopindex+1,5]
        alpha=model.lay_angle
        R=model.lay_angle
        thick=model.thickness
        A=model.tendon_area
        E=model.youngs_modulus
        n=model.comp_number

        # calculating axial stress
        tendonstress,_,_,model.contPress=axisymmetric.solver(np.asarray(model.layer_type),np.asarray(model.layer_radii),
                                                   np.asarray(model.thickness), np.asarray(model.width),
                                                   np.asarray(model.tendon_area),
                                                   np.asarray(model.lay_angle), np.asarray(model.comp_number),
                                                   np.asarray(model.youngs_modulus),
                                                   np.asarray(model.poisson),
                                                   np.asarray(model.gap_ini),
                                                   model.intpresslayer-1, model.extpresslayer-1,
                                                   np.asarray(t_eff), np.asarray(p_int),np.asarray(p_ext))

        self.axialStress = []
        #self.axialStddev = []
        #self.axialMax = []
        #self.axialMin = []
        for i in range(len(model.layer_radii)):
            self.axialStress.append(tendonstress[:,i])
        self.axialStddev=np.std(self.axialStress,1)
        self.axialMax=np.amax(self.axialStress,1)
        self.axialMin=np.amin(self.axialStress,1)

        model.critcurv = coefficients.critical_curvature(model, model.contPress,
                                                         frictionfactors=np.array(model.fricfac))
        # finding the friction stress
        # assuming stick, but limiting stick stress to stress as critical curvature
        self.stickStress = coefficients.stick_stress(model, curv, model.critcurv)
        for i in range(len(self.stickStress)):
            self.stickStress[i] = self.stickStress[i] * np.pi / 2.0
        # finding maximum and standard deviation
        self.stickStddev = []
        self.stickMax = []
        self.stickMin = []
        for i in self.stickStress:
            self.stickStddev.append(np.std(i, axis=0))
            self.stickMax.append(np.amax(i))
            self.stickMin.append(-np.amax(i))

    def plot(self, parent):
        """
        Plotting the calculated results

        :param parent: Where the plot should plot itself
        """
        fig = matplotlib.figure.Figure()
        self.canvas = FigureCanvas(fig)
        self.canvas.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.canvas.setFocus()
        self.canvas.ax1 = fig.add_subplot(111)
        self.canvas.ax1.set_xlabel('Time')
        self.canvas.ax1.set_ylabel('Stress')

        #  Building up the plot data:
        for i in range(len(self.bendStress)):
            use_constant_curvature = self.mainwidget.findChild(QtWidgets.QCheckBox, 'use_constant_curvature')
            if use_constant_curvature.isChecked():
                self.canvas.ax1.plot([self.startTime, self.stopTime],
                                     [self.bendStress[i], self.bendStress[i]],
                                     label=('Bend stress for the %d th layer' % (i + 1)))
            else:
                self.canvas.ax1.plot(self.time,
                                     self.bendStress[i],
                                     label=('Bend stress for the %d th layer' % (i + 1)))
            if use_constant_curvature.isChecked():
                self.canvas.ax1.plot([self.startTime, self.stopTime],
                                     [self.stickStress[i][0], self.stickStress[i][1]],
                                     label=('Stick stress for the %d th layer' % (i + 1)))
            else:
                self.canvas.ax1.plot(self.time,
                                     self.stickStress[i],
                                     label=('Stick stress for the %d th layer' % (i + 1)))
            use_constant_tension = self.mainwidget.findChild(QtWidgets.QCheckBox, 'use_constant_tension')
            if use_constant_tension.isChecked():
                self.canvas.ax1.plot([self.startTime, self.stopTime - 1],
                                     [self.axialStress[i], self.axialStress[i]],
                                     label=('Axial stress for the %d th layer' % (i + 1)))
            else:
                self.canvas.ax1.plot(self.time,
                                     self.axialStress[i],
                                     label=('Axial stress for the %d th layer' % (i + 1)))
            self.canvas.ax1.legend(loc='best')
        self.canvas.ax1.set_title('Stress components')
        fig.subplots_adjust(left=0.1, right=0.97, top=0.9, bottom=0.2)
        self.mpl_toolbar = FricCalcNavigationToolbar(self.canvas, parent)
        self.canvas.mpl_connect('key_press_event', self.on_key_press, )
        button_widget = QtWidgets.QToolButton()
        # add the new widget to the existing navigation toolbar
        self.mpl_toolbar.addWidget(button_widget)
        temp = QtWidgets.QWidget()
        templayout = QtWidgets.QVBoxLayout()
        temp.setLayout(templayout)
        templayout.addWidget(self.mpl_toolbar)
        templayout.addWidget(self.canvas)
        # finding if we already have one of these. if so, remove it and insert this there
        tabs = parent.count()
        found = 0
        for index in range(tabs):
            text = parent.tabText(index)
            if text == 'Plot of stresses from friction calculations':
                widget = parent.widget(index)
                parent.removeTab(index)
                widget.deleteLater()
                found = 1
                break
        if found == 0:
            parent.addTab(temp, 'Plot of stresses from friction calculations')
            parent.setCurrentIndex(tabs)
        elif found == 1:
            # noinspection PyUnboundLocalVariable
            parent.insertTab(index, temp, 'Plot of stresses from friction calculations')
            parent.setCurrentIndex(index)

    def on_key_press(self, event):
        """
        Wrapper for custom toolbar.
        implement the default mpl key press events described at
        http://matplotlib.org/users/navigation_toolbar.html#navigation-keyboard-shortcuts
        :param event:
        :return:
        """
        matplotlib.backend_bases.key_press_handler(event, self.canvas, self.mpl_toolbar)

    def table(self):
        """
        Makes a table of results and puts it in a widget.
        Max min and stddev is calculated shown.

        :return: A widget with the table in it
        """

        items_per_layer = 3
        ncols = 4
        tablewidget = QtWidgets.QTableWidget(items_per_layer * len(self.bendMin), ncols)
        # filling the tablewidget
        # header first
        headeritem = QtWidgets.QTableWidgetItem('Stress component')
        tablewidget.setHorizontalHeaderItem(0, headeritem)
        headeritem = QtWidgets.QTableWidgetItem('Minimum')
        headeritem.setToolTip('The minimum obtain through the applied time series')
        tablewidget.setHorizontalHeaderItem(1, headeritem)
        headeritem = QtWidgets.QTableWidgetItem('Maximum')
        headeritem.setToolTip('The maximum obtain through the applied time series')
        tablewidget.setHorizontalHeaderItem(2, headeritem)
        headeritem = QtWidgets.QTableWidgetItem('Std')
        headeritem.setToolTip('The he standard deviation of the given stress component')
        tablewidget.setHorizontalHeaderItem(3, headeritem)
        for i in range(len(self.bendMin)):
            # adding bending stress
            newitem = QtWidgets.QTableWidgetItem('Local bending stress for layer %d' % (i + 1))
            newitem.setToolTip('Local bending stress for layer %d' % (i + 1))
            tablewidget.setItem(i * items_per_layer, 0, newitem)
            newitem = QtWidgets.QTableWidgetItem(str(self.bendMin[i]))
            tablewidget.setItem(i * items_per_layer, 1, newitem)
            newitem = QtWidgets.QTableWidgetItem(str(self.bendMax[i]))
            tablewidget.setItem(i * items_per_layer, 2, newitem)
            newitem = QtWidgets.QTableWidgetItem(str(self.bendStddev[i]))
            tablewidget.setItem(i * items_per_layer, 3, newitem)
            # adding axial stress
            newitem = QtWidgets.QTableWidgetItem('Axial stress for layer %d' % (i + 1))
            newitem.setToolTip('Axial stress from tension variations %d.' % (i + 1))
            tablewidget.setItem(i * items_per_layer + 1, 0, newitem)
            newitem = QtWidgets.QTableWidgetItem(str(self.axialMin[i]))
            tablewidget.setItem(i * items_per_layer + 1, 1, newitem)
            newitem = QtWidgets.QTableWidgetItem(str(self.axialMax[i]))
            tablewidget.setItem(i * items_per_layer + 1, 2, newitem)
            newitem = QtWidgets.QTableWidgetItem(str(self.axialStddev[i]))
            tablewidget.setItem(i * items_per_layer + 1, 3, newitem)
            # adding axial stress
            newitem = QtWidgets.QTableWidgetItem('Friction stress estimate for layer %d' % (i + 1))
            newitem.setToolTip('Estimated based on stick slip, but capped by the stress at critical curvature')
            tablewidget.setItem(i * items_per_layer + 2, 0, newitem)
            newitem = QtWidgets.QTableWidgetItem(str(self.stickMin[i]))
            tablewidget.setItem(i * items_per_layer + 2, 1, newitem)
            newitem = QtWidgets.QTableWidgetItem(str(self.stickMax[i]))
            tablewidget.setItem(i * items_per_layer + 2, 2, newitem)
            newitem = QtWidgets.QTableWidgetItem(str(self.stickStddev[i]))
            tablewidget.setItem(i * items_per_layer + 2, 3, newitem)
        tablewidget.setSortingEnabled(True)
        return tablewidget

    def startslider_changed(self, value):
        """
        :param value: Current value of slider
        """

        # noinspection PyUnresolvedReferences
        if self.startSlider.hasFocus():
            self.startTime = value
            # noinspection PyUnresolvedReferences
            self.startSpinbox.setValue(value)
            # noinspection PyUnresolvedReferences
            self.endSlider.setRange(value, math.ceil(self.timehist.timehist[-1, 0]))
            # noinspection PyUnresolvedReferences
            self.endSpinbox.setRange(value, self.timehist.timehist[-1, 0])

    def startspinbox_changed(self, value):
        """
        :param value: Current value of spinbox
        """
        # noinspection PyUnresolvedReferences
        if self.startSpinbox.hasFocus():
            self.startTime = value
            # noinspection PyUnresolvedReferences
            self.startSlider.setValue(value)
            # noinspection PyUnresolvedReferences
            self.endSpinbox.setRange(value, self.timehist.timehist[-1, 0])
            # noinspection PyUnresolvedReferences
            self.endSlider.setRange(value, math.ceil(self.timehist.timehist[-1, 0]))

    def endslider_changed(self, value):
        """
        :param value: Current value of slider
        """
        # noinspection PyUnresolvedReferences
        if self.endSlider.hasFocus():
            self.stopTime = value
            # noinspection PyUnresolvedReferences
            self.endSpinbox.setValue(value)
            # noinspection PyUnresolvedReferences
            self.startSpinbox.setRange(self.timehist.timehist[0, 0], value)
            # noinspection PyUnresolvedReferences
            self.startSlider.setRange(math.floor(self.timehist.timehist[0, 0]), value)

    def endspinbox_changed(self, value):
        """
        :param value: Current value of spinbox
        """
        # noinspection PyUnresolvedReferences
        if self.endSpinbox.hasFocus():
            self.stopTime = value
            # noinspection PyUnresolvedReferences
            self.endSlider.setValue(value)
            # noinspection PyUnresolvedReferences
            self.startSpinbox.setRange(self.timehist.timehist[0, 0], value)
            # noinspection PyUnresolvedReferences
            self.startSlider.setRange(math.floor(self.timehist.timehist[0, 0]), value)


class FullStressCalc(QtCore.QObject):
    """ Class for taking care of the full stress calculations
    """
    removeFullCalcResultSignal = QtCore.Signal()

    def __init__(self):
        super(FullStressCalc, self).__init__()
        self.timehists = []
        self.removeButtons = []
        self.labels = []
        self.startSliders = []
        self.startSpinboxes = []
        self.endSliders = []
        self.endSpinboxes = []
        # defined in setup_tab
        self.mainwidget = None
        self.headerStartLinker = None
        self.headerStopLinker = None
        # container widget for time histories
        self.widget = None
        # defined in calculate
        # list of analyses to be run
        self.analyses = None
        # plot
        self.canvas = None
        self.mpl_toolbar = None
        # list of plots
        self.plotlist = []
        # container for plotted lines
        self.lines = []

    def setup_tab(self, window):
        """
        Method for setting up the tab where user sets preferences
        :param window: The main application window
        """
        self.mainwidget = QtWidgets.QWidget()
        self.mainwidget.setAcceptDrops(True)
        self.mainwidget.installEventFilter(window)
        mainlayout = QtWidgets.QVBoxLayout()
        self.mainwidget.setLayout(mainlayout)
        title = QtWidgets.QLabel('<H1> Full stress calculations</H1>')
        mainlayout.addWidget(title)
        analysiscombo_layout = QtWidgets.QHBoxLayout()
        analysiscombobox_text = QtWidgets.QLabel('Choose local methodology')
        analysiscombo_layout.addWidget(analysiscombobox_text)
        analysis_combobox = QtWidgets.QComboBox()
        analysis_combobox.setObjectName('analysis_combobox')
        analysis_combobox.addItem('Use analytical local analysis')
        analysis_combobox.currentIndexChanged.connect(self.localmethod)
        analysiscombo_layout.addWidget(analysis_combobox)
        mainlayout.addLayout(analysiscombo_layout)
        setupwidget = QtWidgets.QWidget()
        setup_layout = QtWidgets.QHBoxLayout()
        setup_layout.setObjectName('setup_layout')
        setupwidget.setLayout(setup_layout)
        self.localmethod(setup_layout)
        mainlayout.addWidget(setupwidget)
        tslayout = QtWidgets.QGridLayout()
        tslayout.setObjectName('tslayout')
        removeall_button = QtWidgets.QPushButton('Remove all')
        tslayout.addWidget(removeall_button, 0, 0)
        removeall_button.clicked.connect(self.remove_all_ts)
        header_name = QtWidgets.QLabel('Name')
        tslayout.addWidget(header_name, 0, 1)
        header_starttime = QtWidgets.QLabel('Start time')
        tslayout.addWidget(header_starttime, 0, 2)
        self.headerStartLinker = QtWidgets.QCheckBox()
        self.headerStartLinker.setToolTip('Set same starttime for all timeseries')
        tslayout.addWidget(self.headerStartLinker, 0, 3)
        header_starttime = QtWidgets.QLabel('Stop time')
        tslayout.addWidget(header_starttime, 0, 4)
        self.headerStopLinker = QtWidgets.QCheckBox()
        self.headerStopLinker.setToolTip('Link stop time for all timeseries')
        tslayout.addWidget(self.headerStopLinker, 0, 5)
        scrollarea = QtWidgets.QScrollArea()
        scrollarea.setWidgetResizable(True)
        self.widget = QtWidgets.QWidget()
        self.widget.setLayout(tslayout)
        scrollarea.setWidget(self.widget)
        mainlayout.addWidget(scrollarea)
        dragdrop = QtWidgets.QLabel('<font color="grey">Drag and drop time series here</font>')
        dragdrop.setObjectName('dragdrop')
        mainlayout.addWidget(dragdrop)
        calcbutton = QtWidgets.QPushButton('Calculate')
        calcbutton.setObjectName('calcbutton')
        calcbutton.setEnabled(False)
        calcbutton.clicked.connect(window.fullcalc)
        mainlayout.addWidget(calcbutton)

    def add_ts(self, ts):
        """
        MEthod for adding a time series. obviously
        :param ts: time series object
        """
        self.timehists.append(ts)

    def setup_ts_edit(self):
        """
        Method for setting up the edit area for the time series
        """
        rows = len(self.timehists)
        self.remove_all_ts(deletedata='no')
        self.widget.setVisible(False)
        for i in range(rows):
            # filling up the widgets
            # Adding a remove icon
            removebutton = QtWidgets.QPushButton(QtGui.QIcon('../icons/remove.png'), '')
            self.removeButtons.append(removebutton)
            tslayout = self.mainwidget.findChild(QtWidgets.QGridLayout, 'tslayout')
            tslayout.addWidget(self.removeButtons[-1], i + 1, 0)
            label = QtWidgets.QLabel(self.timehists[i].name)
            self.labels.append(label)
            self.removeButtons[i].clicked.connect(functools.partial(self.remove_ts, self.labels[i]))
            self.timehists[i].endTime = self.timehists[i].timehist[-1, 0]
            self.timehists[i].startTime = self.timehists[i].timehist[0, 0]
            tslayout.addWidget(self.labels[i], i + 1, 1)
            # Selection of starttime
            startslider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
            self.startSliders.append(startslider)
            self.startSliders[i].setRange(self.timehists[i].startTime,
                                          math.ceil(self.timehists[i].endTime))
            tslayout.addWidget(self.startSliders[i], i + 1, 2)
            startspinbox = QtWidgets.QDoubleSpinBox()
            self.startSpinboxes.append(startspinbox)
            self.startSpinboxes[i].setRange(self.timehists[i].startTime,
                                            self.timehists[i].endTime)
            # Connecting them to each other
            self.startSliders[i].valueChanged.connect(functools.partial(self.startslider_changed, self.labels[i]))
            self.startSpinboxes[i].valueChanged.connect(functools.partial(self.startspinbox_changed, self.labels[i]))
            tslayout.addWidget(self.startSpinboxes[i], i + 1, 3)
            # selection of endtime
            endslider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
            self.endSliders.append(endslider)
            self.endSliders[i].setRange(self.timehists[i].startTime,
                                        math.ceil(self.timehists[i].endTime))
            tslayout.addWidget(self.endSliders[i], i + 1, 4)
            endspinbox = QtWidgets.QDoubleSpinBox()
            self.endSpinboxes.append(endspinbox)
            self.endSpinboxes[i].setRange(self.timehists[i].startTime,
                                          self.timehists[i].endTime)
            # connecting the widgets
            self.endSliders[i].valueChanged.connect(functools.partial(self.endslider_changed, self.labels[i]))
            self.endSpinboxes[i].valueChanged.connect(functools.partial(self.endspinbox_changed, self.labels[i]))
            tslayout.addWidget(self.endSpinboxes[i], i + 1, 5)
            self.endSliders[i].setValue(self.timehists[i].endTime)
            self.endSpinboxes[i].setValue(self.timehists[i].endTime)
            calcbutton = self.mainwidget.findChild(QtWidgets.QPushButton, 'calcbutton')
            calcbutton.setEnabled(True)
        self.widget.setVisible(True)

    def localmethod(self, setup_layout):
        """
        Method for setting up an area where the user can choose parameters to be used.
        Should be split up if more options than analyticalare added
        :param setup_layout: The parent layout in this case
        """
        analysis_combobox = self.mainwidget.findChild(QtWidgets.QComboBox, 'analysis_combobox')
        text = analysis_combobox.currentText()
        item = setup_layout.itemAt(0)
        if item is not None:
            widget = item.widget()
            setup_layout.removeWidget(widget)
            widget.hide()
        if text == 'Use analytical local analysis':
            # calculate some data and put them in the left menu
            if not self.mainwidget.findChild(QtWidgets.QGridLayout, 'analytical_layout'):
                analytical_layout = QtWidgets.QGridLayout()
                label = QtWidgets.QLabel('<H2>Analytical settings</H2>')
                analytical_layout.addWidget(label, 0, 0, 1, 3)
                analytical_widget = QtWidgets.QWidget()
                analytical_widget.setObjectName('analytical_widget')
                analytical_widget.setLayout(analytical_layout)
                corelabel = QtWidgets.QLabel('Number of cores for calculation')
                analytical_layout.addWidget(corelabel, 1, 0, 1, 1)
                corespinbox = QtWidgets.QSpinBox()
                corespinbox.setObjectName('corespinbox')
                corespinbox.setRange(1, 1000)
                bandlabel = QtWidgets.QLabel('Bend model used for local bending stress')
                analytical_layout.addWidget(bandlabel, 1, 2, 1, 1)
                bendcombobox = QtWidgets.QComboBox()
                bendcombobox.setObjectName('bendcombobox')
                bendcombobox.addItem('Savik')
                bendcombobox.addItem('Tan')
                analytical_layout.addWidget(bendcombobox, 1, 3, 1, 1)
                analytical_layout.addWidget(corespinbox, 1, 1, 1, 1)
                thetalabel = QtWidgets.QLabel('Number of calculation points around the crossection')
                analytical_layout.addWidget(thetalabel, 2, 0, 1, 1)
                theta_combobox = QtWidgets.QComboBox()
                theta_combobox.setObjectName('theta_combobox')
                theta_combobox.addItem('4')
                theta_combobox.addItem('8')
                theta_combobox.addItem('16')
                theta_combobox.addItem('32')
                theta_combobox.addItem('64')
                theta_combobox.setCurrentIndex(2)
                analytical_layout.addWidget(theta_combobox, 2, 1, 1, 1)
                philabel = QtWidgets.QLabel('Number of calculation points around the tendon')
                analytical_layout.addWidget(philabel, 2, 2, 1, 1)
                phi_combobox = QtWidgets.QComboBox()
                phi_combobox.setObjectName('phi_combobox')
                phi_combobox.addItem('4')
                phi_combobox.addItem('8')
                phi_combobox.addItem('16')
                phi_combobox.addItem('32')
                phi_combobox.addItem('64')
                phi_combobox.setCurrentIndex(2)
                phi_combobox.setToolTip('Only applicable for circular tendons')
                analytical_layout.addWidget(phi_combobox, 2, 3, 1, 1)
            else:
                analytical_widget = self.mainwidget.findChild(QtWidgets.QWidget, 'analytical_widget')
            analytical_widget.show()
            setup_layout.addWidget(analytical_widget)
        elif text == 'None':
            displaytext = QtWidgets.QLabel('<font color="grey">You need to choose a local method</font>')
            layout = QtWidgets.QHBoxLayout()
            layout.addWidget(displaytext)
            textwidget = QtWidgets.QWidget()
            textwidget.setLayout(layout)
            setup_layout.addWidget(textwidget)

    def remove_all_ts(self, deletedata='yes'):
        """
        Method for removing all the timeseries from view, and with option for also removing them from calculation scope
        :param deletedata: Toggle for whether the underlying data should be removed
        """
        self.widget.setVisible(False)
        for i in range(len(self.startSliders)):
            self.remove_ts(self.labels[0], deletedata=deletedata)
        self.widget.setVisible(True)

    def remove_ts(self, label, deletedata='yes'):
        """
        Method for removing a single time series from view. With option for removing the time
        series from calculations scope as well
        :param label: The label of the time series
        :param deletedata:Toggle for whether the underlying data should be removed
        """
        # finding the row which have the given label
        i = self.index_from_label(label)
        # deleting data and corresponding widgets
        tslayout = self.mainwidget.findChild(QtWidgets.QGridLayout, 'tslayout')
        tslayout.removeWidget(self.removeButtons[i])
        self.removeButtons[i].deleteLater()
        del self.removeButtons[i]

        tslayout.removeWidget(self.labels[i])
        self.labels[i].deleteLater()
        del self.labels[i]

        tslayout.removeWidget(self.startSliders[i])
        self.startSliders[i].deleteLater()
        del self.startSliders[i]

        tslayout.removeWidget(self.startSpinboxes[i])
        self.startSpinboxes[i].deleteLater()
        del self.startSpinboxes[i]

        tslayout.removeWidget(self.endSliders[i])
        self.endSliders[i].deleteLater()
        del self.endSliders[i]

        tslayout.removeWidget(self.endSpinboxes[i])
        self.endSpinboxes[i].deleteLater()
        del self.endSpinboxes[i]
        if deletedata == 'yes':
            del self.timehists[i]

        # When ts is removed, plot interactivty is no longer working -> remove
        self.removeFullCalcResultSignal.emit()

    def startslider_changed(self, label, value):
        """
        Method for keeping track of slider. With possible coupling between elements
        :param label:Label of the time series of which the slider belong
        :param value:Curent value of slider
        """
        # finding the row which have the given label
        row = self.index_from_label(label)
        if self.startSliders[row].hasFocus():
            if self.headerStartLinker.isChecked():
                increment = value - self.timehists[row].startTime
                for i in range(len(self.startSliders)):
                    tmp = min(self.timehists[i].startTime + increment, self.timehists[i].endTime)
                    tmp = max(tmp, 0)
                    self.timehists[i].startTime = tmp
                    self.startSliders[i].setValue(self.timehists[i].startTime)
                    self.startSpinboxes[i].setValue(self.timehists[i].startTime)
                    self.endSpinboxes[i].setRange(tmp, self.timehists[i].timehist[-1, 0])
                    self.endSliders[i].setRange(tmp, math.ceil(self.timehists[i].timehist[-1, 0]))
            else:
                self.timehists[row].startTime = value
                self.startSpinboxes[row].setValue(value)
                self.endSpinboxes[row].setRange(value, self.timehists[row].timehist[-1, 0])
                self.endSliders[row].setRange(value, math.ceil(self.timehists[row].timehist[-1, 0]))

    def startspinbox_changed(self, label, value):
        """
        Method for keeping track of spinbox. With possible coupling between elements
        :param label:Label of the time series of which the slider belong
        :param value:Curent value of slider
        """
        # finding the row which have the given label
        row = self.index_from_label(label)
        if self.startSpinboxes[row].hasFocus():
            if self.headerStartLinker.isChecked():
                increment = value - self.timehists[row].startTime
                for i in range(len(self.startSpinboxes)):
                    tmp = min(self.timehists[i].startTime + increment, self.timehists[i].endTime)
                    tmp = max(tmp, 0)
                    self.timehists[i].startTime = tmp
                    self.startSliders[i].setValue(self.timehists[i].startTime)
                    self.startSpinboxes[i].setValue(self.timehists[i].startTime)
                    self.endSpinboxes[i].setRange(tmp, self.timehists[i].timehist[-1, 0])
                    self.endSliders[i].setRange(tmp, math.ceil(self.timehists[i].timehist[-1, 0]))
            else:
                self.timehists[row].startTime = value
                self.startSliders[row].setValue(value)
                self.endSpinboxes[row].setRange(value, self.timehists[row].timehist[-1, 0])
                self.endSliders[row].setRange(value, math.ceil(self.timehists[row].timehist[-1, 0]))

    def endslider_changed(self, label, value):
        """
        Method for keeping track of slider. With possible coupling between elements
        :param label:Label of the time series of which the slider belong
        :param value:Curent value of slider
        """
        # finding the row which have the given label
        row = self.index_from_label(label)
        if self.endSliders[row].hasFocus():
            if self.headerStopLinker.isChecked():
                increment = value - self.timehists[row].endTime
                for i in range(len(self.endSliders)):
                    tmp = max(self.timehists[i].endTime + increment, self.timehists[i].startTime)
                    tmp = min(tmp, self.timehists[i].timehist[-1, 0])
                    self.timehists[i].endTime = tmp
                    self.endSliders[i].setValue(self.timehists[i].endTime)
                    self.endSpinboxes[i].setValue(self.timehists[i].endTime)
                    self.startSpinboxes[i].setRange(self.timehists[row].timehist[0, 0], tmp)
                    self.startSliders[i].setRange(math.floor(self.timehists[row].timehist[0, 0]), tmp)
            else:
                self.timehists[row].endTime = value
                self.endSpinboxes[row].setValue(value)
                self.startSpinboxes[row].setRange(self.timehists[row].timehist[0, 0], value)
                self.startSliders[row].setRange(math.floor(self.timehists[row].timehist[0, 0]), value)

    def endspinbox_changed(self, label, value):
        """
        Method for keeping track of slider. With possible coupling between elements
        :param label:Label of the time series of which the spinbox belong
        :param value:Curent value of slider
        """
        # finding the row which have the given label
        row = self.index_from_label(label)
        if self.endSpinboxes[row].hasFocus():
            if self.headerStopLinker.isChecked():
                increment = value - self.timehists[row].endTime
                for i in range(len(self.endSpinboxes)):
                    tmp = max(self.timehists[i].endTime + increment, self.timehists[i].startTime)
                    tmp = min(tmp, self.timehists[i].timehist[-1, 0])
                    self.timehists[i].endTime = tmp
                    self.endSliders[i].setValue(self.timehists[i].endTime)
                    self.endSpinboxes[i].setValue(self.timehists[i].endTime)
                    self.startSpinboxes[i].setRange(0, tmp)
                    self.startSliders[i].setRange(0, tmp)
            else:
                self.timehists[row].endTime = value
                self.endSliders[row].setValue(value)
                self.startSpinboxes[row].setRange(0, value)
                self.startSliders[row].setRange(0, value)

    def calculate(self, model):
        """
        Method responsible for setting up the calculation. Calculation thrown to threads
        :param model: The model of the riser. Type slenders.Slender
        """
        self.analyses = []
        calcbutton = self.mainwidget.findChild(QtWidgets.QPushButton, 'calcbutton')
        calcbutton.setEnabled(False)
        analysis_combobox = self.mainwidget.findChild(QtWidgets.QComboBox, 'analysis_combobox')
        if analysis_combobox.currentText() == 'Use analytical local analysis':
            corespinbox = self.mainwidget.findChild(QtWidgets.QSpinBox, 'corespinbox')
            cores = corespinbox.value()
            analysespercore = math.ceil(len(self.timehists) / cores)
            theta_combobox = self.mainwidget.findChild(QtWidgets.QComboBox, 'theta_combobox')
            ntheta = int(theta_combobox.currentText())
            phi_combobox = self.mainwidget.findChild(QtWidgets.QComboBox, 'phi_combobox')
            nphi = int(phi_combobox.currentText())
            bendcombobox = self.mainwidget.findChild(QtWidgets.QComboBox, 'bendcombobox')
            bendmodel = bendcombobox.currentText()
            properties = AnalysisProp('Analytical', ntheta, nphi, bendmodel, model)
            for i in range(cores):
                startindex = i * analysespercore
                stopindex = min(analysespercore * (i + 1), len(self.timehists))

                timehists = self.timehists[startindex:stopindex]
                numbers = list(range(startindex, stopindex))
                self.analyses.append(FullAnalysisThread(model, timehists, numbers, properties))
                # self.analyses[-1].resultFinishedSignal.connect(self.receiveFullCalcs)
                self.analyses[-1].finishedSignal.connect(self.enablecalcbutton)
                self.analyses[-1].start()

    def index_from_label(self, label):
        """
        Method for finding the index in the time series vector .
        :param label: Label of the timeseries
        :return:
        """
        for i in range(len(self.labels)):
            if self.labels[i] == label:
                return i

    def index_from_name(self, name):
        """
        Method for finding the index in the time series vector .
        :param name: Name of the timeseries
        :return:
        """
        for i in range(len(self.timehists)):
            if self.timehists[i].name == name:
                return i

    # noinspection PyChainedComparisons
    def plot(self, plotdata, layout):
        """
        Plots data on the layout <layout>. Uses the color given in plotdata
        :param plotdata: object of PlotData type.
        :param layout: Where to add the plot
        """

        if not self.canvas:
            fig = Figure()
            self.canvas = FigureCanvas(fig)
            self.canvas.setFocusPolicy(QtCore.Qt.StrongFocus)
            self.canvas.setFocus()
            self.canvas.ax1 = fig.add_subplot(111)
            fig.subplots_adjust(left=0.1, right=0.97, top=0.9, bottom=0.2)
            self.mpl_toolbar = FullCalcNavigationToolbar(self.canvas, None)
            self.canvas.mpl_connect('key_press_event', self.on_key_press, )
            anotherwidget = QtWidgets.QToolButton()
            # add the new widget to the existing navigation toolbar
            self.mpl_toolbar.addWidget(anotherwidget)
            temp = QtWidgets.QWidget()
            plotlayout = QtWidgets.QVBoxLayout()
            plotlayout.setObjectName('plotlayout')
            plotlayout.addWidget(self.mpl_toolbar)
            plotlayout.addWidget(self.canvas)

        for plot in plotdata:
            tsnumber = self.index_from_name(plot.label)
            fname = 'tmp/fullresult_' + str(tsnumber) + '.h5'
            f = h5py.File(fname, 'r')
            startindex, endindex = find_indices(self.timehists[tsnumber].startTime, self.timehists[tsnumber].endTime
                                                , self.timehists[tsnumber].timehist[:, 0])
            if plot.type == 'Axial stress':
                if plot.addRemove == 'Add':
                    x_plot = self.timehists[tsnumber].timehist[startindex:endindex, 0]
                    y_plot = f[self.timehists[tsnumber].name + '/AxialStress'][str(plot.layer - 1)]
                    legend = plot.label + '/AxialStress/Layer' + str(plot.layer)
                    color = (
                        plot.color[0] * (1.0 / 255.0), plot.color[1] * (1.0 / 255.0), plot.color[2] * (1.0 / 255.0))
                    self.plotlist.append(plot)
                else:
                    # find where in the labellist given plot is
                    for i in range(len(self.plotlist)):
                        if self.plotlist[i].type == plot.type and self.plotlist[i].layer == plot.layer and \
                                        self.plotlist[i].label == plot.label:
                            del self.plotlist[i]
                            self.lines[i].pop(0).remove()
                            self.lines.pop(i)
                            break

            if plot.type == 'Critical curvature':
                if plot.addRemove == 'Add':
                    x_plot = self.timehists[tsnumber].timehist[startindex:endindex, 0]
                    y_plot = f[self.timehists[tsnumber].name + '/CriticalCurvature'][str(plot.layer - 1)]
                    legend = plot.label + '/CriticalCurvature/Layer' + str(plot.layer)
                    color = (
                        plot.color[0] * (1.0 / 255.0), plot.color[1] * (1.0 / 255.0), plot.color[2] * (1.0 / 255.0))
                    self.plotlist.append(plot)
                else:
                    # find where in the labellist given plot is
                    for i in range(len(self.plotlist)):
                        if self.plotlist[i].type == plot.type and self.plotlist[i].layer == plot.layer and \
                                        self.plotlist[i].label == plot.label:
                            del self.plotlist[i]
                            self.lines[i].pop(0).remove()
                            self.lines.pop(i)
                            break

            if plot.type == 'Stick stress':
                if plot.addRemove == 'Add':
                    x_plot = self.timehists[tsnumber].timehist[startindex:endindex, 0]
                    y_plot = f[self.timehists[tsnumber].name + '/MaxStickStress'][str(plot.layer - 1)]
                    legend = plot.label + '/MaxStickStress/Layer' + str(plot.layer)
                    color = (
                        plot.color[0] * (1.0 / 255.0), plot.color[1] * (1.0 / 255.0), plot.color[2] * (1.0 / 255.0))
                    self.plotlist.append(plot)
                else:
                    # find where in the labellist given plot is
                    for i in range(len(self.plotlist)):
                        if self.plotlist[i].type == plot.type and self.plotlist[i].layer == plot.layer and \
                                        self.plotlist[i].label == plot.label:
                            del self.plotlist[i]
                            self.lines[i].pop(0).remove()
                            self.lines.pop(i)
                            break

            if plot.type == 'Local bending stress':
                if plot.addRemove == 'Add':
                    ntheta = f[self.timehists[tsnumber].name + '/LocalBendingStress'][str(plot.layer - 1)].shape[1]
                    if f[self.timehists[tsnumber].name + '/StressResultant'][str(plot.layer - 1)].ndim==3:
                        nphi = f[self.timehists[tsnumber].name + '/LocalBendingStress'][str(plot.layer - 1)].shape[2]
                    for thetaindex in range(ntheta):
                        theta = thetaindex * 2 * np.pi / ntheta
                        if theta >= plot.theta - 1.0e-5:
                            break
                    if f[self.timehists[tsnumber].name + '/StressResultant'][str(plot.layer - 1)].ndim==3:
                        if plot.corner:
                            for phiindex in range(nphi):
                                if phiindex + 1 == plot.cornerPhi:
                                    break
                        else:
                            for phiindex in range(nphi):
                                phi = phiindex * 2 * np.pi / nphi
                                if phi >= plot.cornerPhi - 1.0e-5:
                                    break
                        x_plot = self.timehists[tsnumber].timehist[startindex:endindex, 0]
                        y_plot = f[self.timehists[tsnumber].name + '/LocalBendingStress'][
                                     str(plot.layer - 1)][:, thetaindex, phiindex]
                        if plot.corner:
                            legend = plot.label + '/LocalBendingStress/Layer' + str(plot.layer) + 'Theta' + str(
                                theta) + 'Corner' + str(phiindex)
                        else:
                            # noinspection PyUnboundLocalVariable
                            legend = plot.label + '/LocalBendingStress/Layer' + str(plot.layer) + 'Theta' + str(
                                theta) + 'Phi' + str(phi)
                    if f[self.timehists[tsnumber].name + '/StressResultant'][str(plot.layer - 1)].ndim==2:
                        x_plot = self.timehists[tsnumber].timehist[startindex:endindex, 0]
                        y_plot = f[self.timehists[tsnumber].name + '/LocalBendingStress'][
                                     str(plot.layer - 1)][:, thetaindex]
                        legend = plot.label + '/LocalBendingStress/Layer' + str(plot.layer) + 'Theta' + str(
                                theta)

                    color = (
                        plot.color[0] * (1.0 / 255.0), plot.color[1] * (1.0 / 255.0), plot.color[2] * (1.0 / 255.0))
                    self.plotlist.append(plot)
                else:
                    for i in range(len(self.plotlist)):
                        if self.plotlist[i].type == plot.type and self.plotlist[i].layer == plot.layer and \
                                        self.plotlist[i].label == plot.label:
                            del self.plotlist[i]
                            self.lines[i].pop(0).remove()
                            self.lines.pop(i)
                            break
            if plot.type == 'Friction stress':
                if plot.addRemove == 'Add':
                    ntheta = f[self.timehists[tsnumber].name + '/FrictionStress'][str(plot.layer - 1)].shape[1]
                    for thetaindex in range(ntheta):
                        theta = thetaindex * 2 * np.pi / ntheta
                        if theta >= plot.theta - 1.0e-5:
                            break
                    x_plot = self.timehists[tsnumber].timehist[startindex:endindex, 0]
                    y_plot = f[self.timehists[tsnumber].name + '/FrictionStress'][str(plot.layer - 1)][:, thetaindex]
                    legend = plot.label + '/FrictionStress/Layer' + str(plot.layer) + 'Theta' + str(theta)
                    color = (
                        plot.color[0] * (1.0 / 255.0), plot.color[1] * (1.0 / 255.0), plot.color[2] * (1.0 / 255.0))
                    self.plotlist.append(plot)
                else:
                    for i in range(len(self.plotlist)):
                        if self.plotlist[i].type == plot.type and self.plotlist[i].layer == plot.layer and \
                                        self.plotlist[i].label == plot.label:
                            del self.plotlist[i]
                            self.lines[i].pop(0).remove()
                            del self.lines[i]
                            # self.lines.pop(i)
                            break
            if plot.type == 'Stress resultant':
                if plot.addRemove == 'Add':
                    ntheta = f[self.timehists[tsnumber].name + '/StressResultant'][str(plot.layer - 1)].shape[1]
                    if f[self.timehists[tsnumber].name + '/StressResultant'][str(plot.layer - 1)].ndim==3:
                        nphi = f[self.timehists[tsnumber].name + '/StressResultant'][str(plot.layer - 1)].shape[2]
                    for thetaindex in range(ntheta):
                        theta = thetaindex * 2 * np.pi / ntheta
                        if theta >= plot.theta - 1.0e-5:
                            break
                    if f[self.timehists[tsnumber].name + '/StressResultant'][str(plot.layer - 1)].ndim==3:
                        if plot.corner:
                            for phiindex in range(nphi):
                                if phiindex + 1 == plot.cornerPhi:
                                    break
                        else:
                            for phiindex in range(nphi):
                                phi = phiindex * 2 * np.pi / nphi
                                if phi >= plot.cornerPhi - 1.0e-5:
                                    break
                    x_plot = self.timehists[tsnumber].timehist[startindex:endindex, 0]
                    if f[self.timehists[tsnumber].name + '/StressResultant'][str(plot.layer - 1)].ndim==3:
                        y_plot = f[self.timehists[tsnumber].name + '/StressResultant'][
                                 str(plot.layer - 1)][:, thetaindex, phiindex]
                        if plot.corner:
                            legend = plot.label + '/StressResultant/Layer' + str(plot.layer) + 'Theta' + str(
                                theta) + 'Corner' + str(phiindex)
                        else:
                            # noinspection PyUnboundLocalVariable
                            legend = plot.label + '/StressResultant/Layer' + str(plot.layer) + 'Theta' + str(
                                theta) + 'Phi' + str(phi)
                    elif f[self.timehists[tsnumber].name + '/StressResultant'][str(plot.layer - 1)].ndim==2:
                        y_plot = f[self.timehists[tsnumber].name + '/StressResultant'][
                                 str(plot.layer - 1)][:, thetaindex]
                        legend = plot.label + '/StressResultant/Layer' + str(plot.layer) + 'Theta' + str(
                                theta)

                    color = (
                        plot.color[0] * (1.0 / 255.0), plot.color[1] * (1.0 / 255.0), plot.color[2] * (1.0 / 255.0))
                    self.plotlist.append(plot)
                else:
                    for i in range(len(self.plotlist)):
                        if self.plotlist[i].type == plot.type and self.plotlist[i].layer == plot.layer and \
                                        self.plotlist[i].label == plot.label:
                            del self.plotlist[i]
                            self.lines[i].pop(0).remove()
                            self.lines.pop(i)
                            break
            # Plotting done here, removal done at finding point
            if plot.addRemove == 'Add':
                # noinspection PyUnboundLocalVariable,PyUnboundLocalVariable,PyUnboundLocalVariable,PyUnboundLocalVariable
                self.lines.append(self.canvas.ax1.plot(x_plot, y_plot, label=legend, c=color))
            f.close()
        plotted = self.canvas.ax1.get_lines()
        max_ylim = -float(1e99)
        min_ylim = float(1e99)
        max_xlim = -float(1e99)
        min_xlim = float(1e99)
        for plot in plotted:
            xdata = plot.get_xdata()
            ydata = plot.get_ydata()
            max_yplot = max(ydata)
            if max_yplot >= 0 and max_yplot > max_ylim:
                max_ylim = max(max_ylim, max_yplot * 1.1 + 0.1)
            else:
                max_ylim = max(max_ylim, max_yplot * 0.9 + 0.1)
            min_yplot = min(ydata)
            if min_yplot >= 0 and min_yplot < min_ylim:
                min_ylim = min(min_ylim, min_yplot * 0.9 - 0.1)
            else:
                min_ylim = min(min_ylim, min_yplot * 1.1 - 0.1)
            max_xplot = max(xdata)
            if max_xplot >= 0 and max_xplot > max_xlim:
                max_xlim = max(max_xlim, max_xplot * 1.1 + 0.1)
            else:
                max_xlim = max(max_xlim, max_xplot * 0.9 + 0.1)
            min_xplot = min(xdata)
            if min_xplot >= 0 and min_xplot < min_xlim:
                min_xlim = min(min_xlim, min_xplot * 0.9 - 0.1)
            else:
                min_xlim = min(min_xlim, min_xplot * 1.1 - 0.1)
        # set plot limits
        self.canvas.ax1.set_ylim([min_ylim, max_ylim])
        self.canvas.ax1.set_xlim([min_xlim, max_xlim])

        # find were to put this
        found = False
        for i in range(layout.count()):
            widget = layout.widget(i)
            if hasattr(widget, 'accessibleName'):
                name = widget.accessibleName()
                if name == 'plotwidget':
                    # widget.deleteLater()
                    found = True
                    break
        if not found:
            plotwidget = QtWidgets.QWidget()
            plotwidget.setAccessibleName('plotwidget')
            # noinspection PyUnboundLocalVariable
            plotwidget.setLayout(plotlayout)
            layout.addWidget(plotwidget)
            plotwidget.show()
        else:
            self.canvas.draw()
        gc.collect()

    def deleteplot(self):
        """
        Method for removign the plot thouroghly
        """
        self.canvas = None
        del self.mpl_toolbar
        self.plotlist = []
        self.lines = []

    def on_key_press(self, event):
        """
        implement the default mpl key press events described at
        http://matplotlib.org/users/navigation_toolbar.html#navigation-keyboard-shortcuts
        """

        matplotlib.backend_bases.key_press_handler(event, self.canvas, self.mpl_toolbar)

    def enablecalcbutton(self):
        """
        Enables the calcbutton, so new calculations can be carried out
        """
        calcbutton = self.mainwidget.findChild(QtWidgets.QPushButton, 'calcbutton')
        calcbutton.setEnabled(True)

    def export_all_results(self):
        """
        Method for merging all the files from the full calculations to one h5 file
        """
        file_dialog = QtWidgets.QFileDialog()
        file_types = "hdf5 file (*.h5);;Any file (*)"
        # show the dialog
        fname, extension = file_dialog.getSaveFileName(self.mainwidget, 'Save file', '', file_types)
        if fname:
            if extension.find('h5') == -1:
                # extension not specified
                output_file = h5py.File(fname, 'w')
            else:
                # extension specified
                if fname.find('h5', -2) != -1:
                    # filename ends with h5
                    output_file = h5py.File(fname, 'w')
                else:
                    # filename not ending with h5
                    output_file = h5py.File(fname + '.h5', 'w')

            # finding which files to copy
            for input_file_name in os.listdir('tmp'):
                if fnmatch.fnmatch(input_file_name, 'fullresult*.h5'):
                    # copy the files
                    input_file = h5py.File('tmp/' + input_file_name, 'r+')
                    keys = input_file.keys()
                    for key in keys:
                        input_file.copy(key, output_file)
                        # adding results from memory
                        index = self.index_from_name(key)
                        output_file[key].create_dataset('Time', data=self.timehists[index].timehist[:, 0])
                        output_file[key].create_dataset('Tension', data=self.timehists[index].timehist[:, 1])
                        output_file[key].create_dataset('Curvature', data=self.timehists[index].timehist[:, 2:4])

                    input_file.close()

            output_file.close()


class FullAnalysisThread(QtCore.QThread):
    """
    A thread to carry out full stress calculations.
    """
    addFullCalcSignal = QtCore.Signal(int)
    finishedSignal = QtCore.Signal(bool)

    def __init__(self, model, timeseries, indices, properties):
        QtCore.QThread.__init__(self)
        self.model = model
        self.timeseries = timeseries
        self.indices = indices
        self.ntheta = properties.ntheta
        self.nphi = properties.nphi
        self.bendmodel = properties.bendmodel
        # used in run and in filedump
        # results to be written to file
        self.axialstress = None
        self.bendstress = None
        self.critcurv = None
        self.maxStickStress = None
        self.fricStress = None
        # counter used for filedump
        self.currentindex = None

    # noinspection PyPep8
    def run(self):
        """
        Method for running the analysis
        """
        ntheta = self.ntheta
        nphi = self.nphi
        bend_model = self.bendmodel
        # timsesteps between each dump to file
        maxlength = 5000
        for k in range(len(self.indices)):
            ts = self.timeseries[k]
            globalindex = self.indices[k]
            globalstartindex, globalstopindex = find_indices(ts.startTime, ts.endTime, ts.timehist[:, 0])
            tslength = globalstopindex - globalstartindex
            # start midpoint for friction routine
            mid=[]
            for i in range(len(self.model.layer_radii)):
                mid.append([0.0, 0.0])

            parts = math.ceil((globalstopindex - globalstartindex) / maxlength)
            for part in range(parts):  # maybe one more iteration?
                startindex = globalstartindex + part * maxlength
                stopindex = min(startindex + maxlength, globalstopindex)

                # finding axial stress
                self.axialstress = []
                thickness=self.model.thickness
                sigma,_,_,contactpressures=axisymmetric.solver(np.asarray(self.model.layer_type),
                                                           np.asarray(self.model.layer_radii),
                                                           np.asarray(self.model.thickness),
                                                           np.asarray(self.model.width),
                                                           np.asarray(self.model.tendon_area),
                                                           np.asarray(self.model.lay_angle),
                                                           np.asarray(self.model.comp_number),
                                                           np.asarray(self.model.youngs_modulus),
                                                           np.asarray(self.model.poisson),
                                                           np.asarray(self.model.gap_ini),
                                                           self.model.intpresslayer-1, self.model.extpresslayer-1,
                                                           np.asarray(ts.timehist[startindex:stopindex,1]),
                                                           np.asarray(ts.timehist[startindex:stopindex,4]),
                                                           np.asarray(ts.timehist[startindex:stopindex,5]))
                for i in range(len(self.model.layer_radii)):
                    self.axialstress.append(sigma[:,i])
#                    self.axialstress.append(coefficients.tension_stress_history(ts.timehist
#                                                                                [startindex:stopindex, 1],
#                                                                                i))
                # finding local bending stress
                # implementation of analysis options should be done once.
                self.bendstress = coefficients.bending_stress_history(self.model,
                                                                      ts.timehist[startindex:stopindex, 2:4],
                                                                      ntheta=ntheta, nphi=max(nphi),
                                                                      bend_model=bend_model)

                critcurv = coefficients.critical_curvature(self.model, contactpressures,
                                                           frictionfactors=np.array(self.model.fricfac))
                self.critcurv = []
                eff_full_slipcurvature = []
                for i in range(len(self.model.layer_radii)):
                    self.critcurv.append(critcurv[:, i]*np.pi/2.0)
                    eff_full_slipcurvature.append(critcurv[:, i] * np.pi / 2.0)
                self.maxStickStress = coefficients.max_stickstress(self.model, eff_full_slipcurvature)
                # for debugging. to be removed
                # self.stickStress=coefficients.stick_stress(self.model,ts.timehist[startindex:stopindex,2:4],
                #                                           critcurv*np.pi/2.0)
                self.fricStress = []
                timehist = ts.timehist[startindex:stopindex, 2:4]
                for i in range(len(self.model.layer_radii)):
                    if self.model.layer_type[i]=='wires':
                        self.fricStress.append(np.empty([len(ts.timehist[startindex:stopindex, 0]), ntheta]))
                        # removing dots for speed
                        effectivecurvature = eff_full_slipcurvature[i]
                        print('i',i)
                        max_stickstress = self.maxStickStress[i]
                        stress, mid[i] = coefficients.fric_stress(timehist, effectivecurvature, max_stickstress, mid[i])
                        for j in range(ntheta):
                            self.fricStress[i][:, j] = (stress[:, 0] * np.cos(j * 2 * np.pi / ntheta) +
                                                        stress[:, 1] * np.sin(j * 2 * np.pi / ntheta))
                    elif self.model.layer_type[i]=='sheath':
                        self.fricStress.append(np.empty([len(ts.timehist[startindex:stopindex, 0]), ntheta]))
                    else:
                        print('Layer type for layer ',i+1,' not understood')

                self.resultantstress=[]
                for i in range(len(self.axialstress)):
                    if self.model.layer_type[i]=='wires':
                        self.resultantstress.append(np.swapaxes(np.swapaxes(self.bendstress[i],0,2)+
                                                    np.swapaxes(self.fricStress[i],0,1)+self.axialstress[i],0,2))
                    elif self.model.layer_type[i]=='sheath':
                        self.resultantstress.append(np.swapaxes(np.swapaxes(self.bendstress[i],0,1)+
                                                    +self.axialstress[i],0,1))


                self.filedump(part, tslength, k, globalindex)
            # the resultsFinishedSignal should be pointer to file!
            self.addFullCalcSignal.emit(globalindex)
            # self.resultFinishedSignal.emit(self.axialstress,self.bendstress,self.fricStress,self.critcurv,self.max_stickstress,localindex)
        self.finishedSignal.emit(True)

    def filedump(self, part, tslength, localindex, globalindex):
        """
        Method for dumping to file. Makes files, keep track of indices and dumps data
        :param part: counter for which part of the analysis is carried out.
        :param tslength: Total length of time series
        :param globalindex: index in all timeseries analysed
        :param localindex: index in timeseries analyzed in this thread
        """
        fname = 'tmp/fullresult_' + str(globalindex) + '.h5'
        # maybe better to keep the hf instance
        if part == 0:
            self.currentindex = 0
            hf = h5py.File(fname, 'w')
            # creating groups
            #print('globalindex', globalindex)
            #print('len(self.timeseries)', len(self.timeseries))
            #print('localindex', localindex)
            axial = hf.create_group(self.timeseries[localindex].name + '/AxialStress')
            bend = hf.create_group(self.timeseries[localindex].name + '/LocalBendingStress')
            fric = hf.create_group(self.timeseries[localindex].name + '/FrictionStress')
            stressres = hf.create_group(self.timeseries[localindex].name + '/StressResultant')
            critcurv = hf.create_group(self.timeseries[localindex].name + '/CriticalCurvature')
            maxstick = hf.create_group(self.timeseries[localindex].name + '/MaxStickStress')
            # creating datasets
            for i in range(len(self.axialstress)):
                axial.create_dataset(str(i), (tslength,))
                axial[str(i)][self.currentindex:self.currentindex + len(self.axialstress[i])] = self.axialstress[i]
                if self.model.layer_type[i]=='wires':
                    bend.create_dataset(str(i), (tslength, self.ntheta, self.nphi[i]))
                elif self.model.layer_type[i]=='sheath':
                    bend.create_dataset(str(i), (tslength, self.ntheta))
                bend[str(i)][self.currentindex:self.currentindex + len(self.bendstress[i])] = self.bendstress[i]
                if self.model.layer_type[i]=='wires':
                    fric.create_dataset(str(i), (tslength, self.ntheta))
                    fric[str(i)][self.currentindex:self.currentindex + len(self.fricStress[i])] = self.fricStress[i]
                if self.model.layer_type[i]=='wires':
                    stressres.create_dataset(str(i), (tslength, self.ntheta, self.nphi[i]))
                elif self.model.layer_type[i]=='sheath':
                    stressres.create_dataset(str(i), (tslength, self.ntheta))
                stressres[str(i)][self.currentindex:self.currentindex + len(self.resultantstress[i])] = self.resultantstress[i]
                if self.model.layer_type[i]=='wires':
                    critcurv.create_dataset(str(i), (tslength,))
                    critcurv[str(i)][self.currentindex:self.currentindex + len(self.critcurv[i])] = self.critcurv[i]
                    maxstick.create_dataset(str(i), (tslength,))
                    maxstick[str(i)][self.currentindex:self.currentindex + len(self.maxStickStress[i])] = \
                        self.maxStickStress[i]
            hf.close()
        else:
            hf = h5py.File(fname, 'r+')
            for i in range(len(self.axialstress)):
                axial = hf[self.timeseries[localindex].name + '/AxialStress']
                axial[str(i)][self.currentindex:self.currentindex + len(self.axialstress[i])] = self.axialstress[i]
                bend = hf[self.timeseries[localindex].name + '/LocalBendingStress']
                bend[str(i)][self.currentindex:self.currentindex + len(self.axialstress[i]), :, :] = self.bendstress[i]
                fric = hf[self.timeseries[localindex].name + '/FrictionStress']
                fric[str(i)][self.currentindex:self.currentindex + len(self.axialstress[i]), :] = self.fricStress[i]
                bend = hf[self.timeseries[localindex].name + '/StressResultant']
                bend[str(i)][self.currentindex:self.currentindex + len(self.axialstress[i]), :, :] = self.resultantstress[i]
                critcurv = hf[self.timeseries[localindex].name + '/CriticalCurvature']
                critcurv[str(i)][self.currentindex:self.currentindex + len(self.axialstress[i])] = self.critcurv[i]
                maxstick = hf[self.timeseries[localindex].name + '/MaxStickStress']
                maxstick[str(i)][self.currentindex:self.currentindex + len(self.axialstress[i])] = self.maxStickStress[
                    i]
            hf.close()
        self.currentindex += len(self.axialstress[0])


class FricCalcNavigationToolbar(NavigationToolbar):
    """
    Custom toolbaer for friction class plot
    """
    picked = QtCore.Signal(int, name='picked')

    def __init__(self, canvas, parent):
        NavigationToolbar.__init__(self, canvas, parent)
        self.clearButtons = []
        # Search through existing buttons
        # next use for placement of custom button
        nextbutton = None
        for c in self.findChildren(QtWidgets.QToolButton):
            if c.text() == 'Customize':
                nextbutton = c

        savedata = QtWidgets.QAction("Save data", self)
        savedata.setIcon(QtGui.QIcon("../icons/savedata.png"))
        savedata.setCheckable(True)
        savedata.setToolTip("Save raw data")
        self.picker = savedata
        button = QtWidgets.QToolButton(self)
        button.setDefaultAction(self.picker)

        # Add it to the toolbar, and connect up event
        self.insertWidget(nextbutton.defaultAction(), button)
        savedata.toggled.connect(self.savedata_triggered)

        # Grab the picked event from the canvas
        # canvas.mpl_connect('pick_event',self.canvasPicked)

    def savedata_triggered(self, ):
        """
        Method for saving plot data to h5 file
        """

        file_dialog = QtWidgets.QFileDialog()
        file_types = "hdf5 file (*.h5);;Any file (*)"
        # show the dialog
        fname, extension = file_dialog.getSaveFileName(self, 'Save file', '', file_types)
        if fname:
            if extension.find('h5') == -1:
                # extension not specified
                hf = h5py.File(fname)
            else:
                # extension specified
                if fname.find('h5', -2) != -1:
                    # filename ends with h5
                    hf = h5py.File(fname)
                else:
                    # filename not ending with h5
                    hf = h5py.File(fname + '.h5')

            lines = self.canvas.ax1.get_lines()
            for line in lines:
                xydata = line.get_xydata()
                label = line.get_label()
                g1 = hf.create_group('frictioncalc/%s' % label)
                g1.create_dataset('x', data=xydata[:, 0])
                g1.create_dataset('y', data=xydata[:, 1])
            hf.close()


class FullCalcNavigationToolbar(FricCalcNavigationToolbar):
    """
    Class for custom toolbar for Full calculation plot
    """
    def __init__(self, canvas, parent):
        FricCalcNavigationToolbar.__init__(self, canvas, parent)

    def savedata_triggered(self, ):
        """
        Method for saving plot data as h5 file
        """
        file_dialog = QtWidgets.QFileDialog()
        file_types = "hdf5 file (*.h5);;ASCII file (*.ts)"
        # show the dialog
        fname, extension = file_dialog.getSaveFileName(self, 'Save file', '', file_types)
        if fname:
            if extension.find('ts') != -1:
                if fname[-3:-1]=='ts':
                    filename=fname
                else:
                    filename=fname+'.ts'
                type = 'ts'
            elif extension.find('h5') != -1:
                if fname[-2:]=='h5':
                    filename=fname
                else:
                    filename=fname+'.h5'
                type = 'h5'

            lines = self.canvas.ax1.get_lines()
            if type == 'ts':
                printmatrix=[]
                header='Time'
                printmatrix.append(lines[0].get_xdata())
                for line in lines:
                    ydata = line.get_ydata()
                    label = line.get_label()
                    printmatrix.append(ydata)
                    header+="\t"+label
                npprintmatrix=np.array(printmatrix)
                np.savetxt(filename, npprintmatrix.T, delimiter="\t", header=header)

            elif type == 'h5':
                hf = h5py.File(filename, 'w')
                for line in lines:
                    xydata = line.get_xydata()
                    label = line.get_label()
                    g1 = hf.create_group(label)
                    g1.create_dataset('Time', data=xydata[:, 0])
                    g1.create_dataset('Value', data=xydata[:, 1])
                hf.close()


class PlotData(object):
    """
    Class for making plot data. Keeps track of what color it should be and whether it should be removed or added
    Several plot data instances keep track of each other through colortaken and colorlist Could be made class
    variable really
    """
    def __init__(self, treedata, colorlist, colortaken, add_remove):
        self.label = treedata[0]
        self.layer = int(treedata[1].split()[-1])
        self.type = treedata[2]
        self.addRemove = add_remove
        if self.addRemove == 'Add':
            colormin = min(colortaken)
            for i in range(len(colortaken)):
                if colortaken[i] == colormin:
                    self.color = colorlist[i]
                    self.colorindex = i
                    break

    def update(self, active, color_taken):
        """
        :param active: Whether the plot should be active or not
        :param color_taken: list of which colors that are currently in use in the plot
        """
        if self.addRemove == 'Add':
            color_taken[self.colorindex] += 1
            active.append(self)
        elif self.addRemove == 'Remove':
            for i in range(len(active)):
                if [active[i].type, active[i].label, active[i].layer] == [self.type, self.label, self.layer]:
                    color_taken[active[i].colorindex] -= 1
                    del active[i]
                    break


class FourLevelPlotData(PlotData):
    """ As parent, only with one more level of refineent with distinguishing thetas
    """
    def __init__(self, treedata, colorlist, colortaken, add_remove):
        super(FourLevelPlotData, self).__init__(treedata, colorlist, colortaken, add_remove)
        self.theta = float(treedata[3].split()[-1])

    def update(self, active, color_taken):
        if self.addRemove == 'Add':
            color_taken[self.colorindex] += 1
            active.append(self)
        elif self.addRemove == 'Remove':
            for i in range(len(active)):
                if hasattr(active, 'theta'):
                    if ([active[i].type, active[i].label, active[i].layer, active[i].theta] ==
                            [self.type, self.label, self.layer, self.theta]):
                        color_taken[active[i].colorindex] -= 1
                        del active[i]
                        break


class FiveLevelPlotData(FourLevelPlotData):
    """ As parent, only with one more level of refineent with distinguishing phis
    """
    def __init__(self, treedata, colorlist, colortaken, add_remove):
        super(FiveLevelPlotData, self).__init__(treedata, colorlist, colortaken, add_remove)
        self.cornerPhi = float(treedata[4].split()[-1])
        if treedata[4][0:4] == 'Corn':
            self.corner = True
        else:
            self.corner = False

    def update(self, active, color_taken):
        if self.addRemove == 'Add':
            color_taken[self.colorindex] += 1
            active.append(self)
        elif self.addRemove == 'Remove':
            for i in range(len(active)):
                if hasattr(active, 'theta') and hasattr(active, 'cornerPhi'):
                    if ([active[i].type, active[i].label, active[i].layer, active[i].theta, active[i].cornerPhi] ==
                            [self.type, self.label, self.layer, self.theta, self.cornerPhi]):
                        color_taken[active[i].colorindex] -= 1
                        del active[i]
                        break


class AnalysisProp(object):
    """
    Method for grouping together analysis properties
    """
    def __init__(self, analysistype, ntheta, nphi, bendmodel, model):
        if analysistype == 'Analytical':
            self.ntheta = ntheta
            self.bendmodel = bendmodel
            self.nphi = []
            for i in range(len(model.layer_radii)):
                    self.nphi.append(4)


def find_indices(startvalue, endvalue, vector):
    """
    Assumes sorted data.
    :param startvalue: start value in vector to find
    :param endvalue: End value to find in vector
    :param vector:
    :return startindex: index of last value smaller or equal to startvalue
    :return endindex: first index equal or larger than vector.Bound by length of vector
    """
    startindex = np.argmax(vector > startvalue)
    startindex -= 1
    endindex = np.argmax(vector >= endvalue)
    endindex = np.amin([endindex, len(vector)])
    endindex += 1

    return startindex, endindex

class FullTreeProxyModel(QtCore.QSortFilterProxyModel):
    """
    Subclassed proxy model for custom filter behaviour
    """
    def __init__(self,parent = None):
        super(FullTreeProxyModel, self).__init__(parent)

    def filterAcceptsRow(self, sourceRow, sourceParent):
        index0 = self.sourceModel().index(sourceRow, 0, sourceParent)
        index1 = self.sourceModel().index(sourceRow, 1, sourceParent)
        index2 = self.sourceModel().index(sourceRow, 2, sourceParent)

        return (   (self.filterRegExp().indexIn(self.sourceModel().data(index0,QtCore.Qt.DisplayRole)) >= 0
                    or self.filterRegExp().indexIn(self.sourceModel().data(index1,QtCore.Qt.DisplayRole)) >= 0))


