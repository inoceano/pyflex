"""Copyright (C) 2016 Joakim A. Taby

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""
from unittest import TestCase

import slenders

import numpy as np


# noinspection PyUnresolvedReferences
class TestSlender(TestCase):
    def test_initiation(self):
        errorfraction = 1.0e-6
        s = slenders.Slender(["5.14e-3"])
        self.assertIsInstance(s.comp_number, list)
        self.assertIsInstance(s.fricfac, list)
        self.assertIsInstance(s.lay_angle, list)
        self.assertIsInstance(s.layer_radii, list)
        self.assertIsInstance(s.layergeometry, list)
        self.assertIsInstance(s.tendon_area, list)
        self.assertIsInstance(s.typical_curvature, float)
        self.assertIsInstance(s.typical_tension, float)
        self.assertIsInstance(s.youngs_modulus, list)
        self.assertEqual(s.comp_number[0], 0)
        self.assertAlmostEqual(s.tendon_area[0], 8.299962127078088e-05, s.tendon_area[0] * errorfraction)
        s = slenders.Slender([["5.14e-3"], ["0.1", "0.15"]], lay_angle=[35])
        self.assertAlmostEqual(s.lay_angle[0], -s.lay_angle[1], delta=s.lay_angle[0])
