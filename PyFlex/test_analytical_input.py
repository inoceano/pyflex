"""Copyright (C) 2016 Joakim A. Taby

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>."""
from unittest import TestCase

import pyflex

__author__ = 'jtab'


class TestAnalyticalInput(TestCase):
    def test_analytical_input(self):
        inputfile = "test_inputfile.dat"
        delimiter = "\t"
        commentchar = "#"
        errorfraction = 1e-6
        s = pyflex.analytical_input(inputfile, delimiter, commentchar)
        # Checking type of object values
        self.assertIsInstance(s.comp_number, list)
        self.assertIsInstance(s.fricfac, list)
        self.assertIsInstance(s.lay_angle, list)
        self.assertIsInstance(s.layer_radii, list)
        self.assertIsInstance(s.layergeometry, list)
        self.assertIsInstance(s.tendon_area, list)
        self.assertIsInstance(s.typical_curvature, float)
        self.assertIsInstance(s.typical_tension, float)
        self.assertIsInstance(s.youngs_modulus, list)
        # Checking the values themselves
        self.assertEqual(s.comp_number[0], 51)
        self.assertEqual(s.comp_number[1], 41)
        self.assertAlmostEqual(s.fricfac[0], 0.1, delta=0.1 * errorfraction)
        self.assertAlmostEqual(s.fricfac[1], 0.5, delta=0.5 * errorfraction)
        self.assertAlmostEqual(s.lay_angle[0], 0.715584993317675, delta=0.715584993317675 * errorfraction)
        self.assertAlmostEqual(s.lay_angle[1], -0.715584993317675, delta=-0.715584993317675 * errorfraction)
        self.assertAlmostEqual(s.layer_radii[0], 125.0, delta=125.0)
        self.assertAlmostEqual(s.layer_radii[1], 123.0, delta=123.0)
        self.assertAlmostEqual(s.layergeometry[0][0], 5.15, delta=5.15 * errorfraction)
        self.assertAlmostEqual(s.layergeometry[1][0], 15.0, delta=15.0 * errorfraction)
        self.assertAlmostEqual(s.layergeometry[1][1], 5.0, delta=5.0 * errorfraction)
        self.assertAlmostEqual(s.tendon_area[0], 83.3228911548353, delta=83.3228911548353 * errorfraction)
        self.assertAlmostEqual(s.tendon_area[1], 75.0, delta=75.0 * errorfraction)
        self.assertAlmostEqual(s.typical_curvature, 1e-5, delta=1e-5 * errorfraction)
        self.assertAlmostEqual(s.typical_tension, 10.0, delta=10.0)
        self.assertAlmostEqual(s.youngs_modulus[0], 207, delta=207 * errorfraction)
        self.assertAlmostEqual(s.youngs_modulus[1], 210, delta=210 * errorfraction)
