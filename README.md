# README #

This README would normally document whatever steps are necessary to get your application up and running.

### A stress transfer function for flexible slender marine structures  ###

* Currently using analytic equations for finding cross section properties and transforming curvature and tension into stress time histories

Planned extensions:

* Simple GUI
* Import of results from local analysis
* plugin for orcaflex

### How do I get set up? ###
* clone this depository
* Verify that you have python installed, or install it.
* install the required packages(to be found in setup.py)
* run main.py in the PyFlex folder.

### Contribution guidelines ###

* All bits of codes submitted as contribution must be tested by unittests
* All bits of code submitted as contribution must be adequately commented and involve a docstring describing its purpose
* All bits of code will be reviewed by at least one repositry admin before acceptance 

### Who do I talk to? ###

* Any questions or comments can be submitted to Joakim A. Taby:
joakim.taby@inoceano.com
