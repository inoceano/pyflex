from distutils.core import setup

setup(
    name='pyflex',
    version='1.0',
    packages=[''],
    url='https://bitbucket.org/inoceano/pyflex',
    license='GPL',
    author='Joakim Taby',
    author_email='joakim.taby@inoceano.com',
    description='A program for stress analysis of flexible riser cross sections', requires=['numpy']
)
