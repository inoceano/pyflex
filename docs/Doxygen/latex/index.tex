\hypertarget{index_sec_description}{}\section{Program description}\label{index_sec_description}
This is a program for generating stresses in the cross section of flexible risers. Input is time histories of pressure tensions and curvatures together with cross sectional properties.\hypertarget{index_get_started}{}\section{Getting started}\label{index_get_started}
All files can be found on \href{https://bitbucket.org/inoceano/pyflex}{\tt bitbucket}. Clone the repository, make sure you have python and the nescessary packages installed. Thereafter, run \hyperlink{main_8py}{main.\+py}

To generate the documentation locally, download \href{http://www.doxygen.org}{\tt doxygen} and run it using the Doxy\+File under docs/\+Doxygen in the repository.\hypertarget{index_sec_theory}{}\section{Theory}\label{index_sec_theory}
To find details of the theory, one can explore the source code itself, or the source code documentation. A brief description is also given here.

The solution method is a two step procedure, where the axisymmetric response is found first and the response from bending and frictional response is found subsequently based on contact pressures between the layers found when solving the axisymmetric system. The following assumptions are made\+:
\begin{DoxyEnumerate}
\item Constant curvature
\item The contact pressure from the axisymmetric analysis is valid also when bending is applied
\item The helical wires are initially curved and stress-\/free.
\end{DoxyEnumerate}\hypertarget{index_subsec_coords_and_defs}{}\subsection{Coordinate system and definitions}\label{index_subsec_coords_and_defs}
The figure below shows the global coordinate system, the meaning of $ \theta $ and the corner numbering.

 
\begin{DoxyImage}
\includegraphics[width=10cm]{definition}
\caption{Definitions}
\end{DoxyImage}


A positive $\kappa_i$ is defined as a curvature that gives axial tension for a point with a positiv value along the $i$ axis.

The radiuses and thicknesses are indicated in the following figure.

 
\begin{DoxyImage}
\includegraphics[width=10cm]{radiusandthickness}
\caption{Radius and thickness}
\end{DoxyImage}
\hypertarget{index_subsec_axisymmetric}{}\subsection{Axisymmetric response}\label{index_subsec_axisymmetric}
The main results from the axisymmetric analysis is stresses and contact pressures between layers. The axisymmetric algorithm allows for the modelling of plastic sheaths and also takes possible gap formation between the layers into account.

In order to do this a system of equations with constraints on them needs to be solved. Py\+Flex solves 3 equations for each helic layer, 2 for each sheath layer and one equation for each layer interface taken to be in contact. In addition we solve one equation for overall axial equilibrium. The equations for the wire layers are to a large extent based on the equations found in M\+A\+R\+I\+N\+T\+E\+K/\+N\+T\+N\+U/4\+Subsea (2014), while the equations for the sheath layers are based on expressions found in Custodio e Vaz (2002). The equations are developed based on no pressure working in the hoop direction on the wire layers. This is not a correct assumption for the carcass, or in a case with flooded annulus, but is thought to not give rise to significant errors in fatigue calculations.

The system is built in \hyperlink{namespaceaxisymmetric_a4e34392c330b10793d01c550435c5003}{axisymmetric.\+systemsetup}, and its governing equations are shown here\+:
\begin{DoxyEnumerate}
\item Equation governing the kinematics of a wire layer\+: \begin{align} \epsilon_{11} R_i -\epsilon_a cos(\alpha) R_i-sin^2(\alpha) u_3 = 0 \end{align}
\item Equilibrium of radial forces\+:

For wire layer\+: \begin{align} \frac{E_i A_i n_i \epsilon _{11,i} \sec \left(\alpha _i\right) sin^2\left(\alpha _i\right)}{r_i}-\frac{A_i \nu _i n_i p_i \sec \left(\alpha _i\right) sin^2\left(\alpha _i\right)}{2 f_f r_i}-\frac{A_i \nu _i n_i p_{i+1} \sec \left(\alpha _i\right) sin^2\left(\alpha _i\right)}{2 f_f r_i}-2 \pi p_i r_i+2 \pi p_{i+1} r_{i+1}+\pi r_i t_i+\pi r_{i+1} t_{i+1}=0 \end{align} For sheath layer\+: \begin{align} {1 \over E_i R_i (r^2_{int,i} - r^2_{out,i}} (p_{i+1} (1+\nu_i)(R^2_i(-1+2\nu_i)-r_{int,i}^2) r_{out,i}^2 - p_{i}(1+\nu_i)(R^2_i(-1+2\nu_i)-r_{out,i}^2)r_{int,i}^2)+ E_i R_i (r_{int,i}^2 - r_{out,i}^2) (u_{3,i} + R_i \nu_i \epsilon_{11,i})) =0 \end{align}
\item Thickness equilibrium\+:

For wire layer\+: \begin{align} {\Delta t_i \over t_i} - {(1 + \nu^2_i)p_i \over 2 E_i f_f} - {(1 + \nu^2_i)p_{i+1} \over 2 E_i f_f} + \nu_i \epsilon_{11,i} = 0 \end{align}

For sheath layer\+: \begin{align} \Delta t_i - {t_i \over E_i} ({(p_{i+1}-p_i) r_{int,i}^2 r_{out,i}^2\over R_i^2(r_{out,i}^2-r_{int,i}^2)} + {p_i r_{int,i}^2-p_{i+1}r_{out,i}^2 \over r_{out,i}^2 - r_{int,i}^2} - \nu_i (- { (p_{i+1}-p_i) r_{int,i}^2 r_{out,i}^2 \over R_i^2(r_{out,i}^2-r_{int,i}^2)} + {p_i r_{int,i}^2-p_{i+1}r_{out,i}^2 \over r_{out,i}^2 - r_{int,i}^2}) - {\nu_i \over r_{int,i}^2 - r_{out,i}^2} (-2 p_i \nu_i r_{int,i}^2+2 p_{i+1} \nu_i r_{out,i}^2 + E_i (r_{int,i}^2-r_{out,i}^2) \epsilon_{11,i} ) ) = 0 \end{align}
\item Continuity for layers in contact\+: \begin{align} u_{3,i}-u_{3,i-1}-\frac{\Delta t_{i-1}}{2}-\frac{\Delta t_i}{2}+gap_i^{ini}=0 \end{align}
\item Overall axial equilibrium\+: \begin{align} \sum_{i=0}^{n_{wirelayers}} ( E_i A_i n_i \epsilon _{11,i} \cos \left(\alpha _i\right)-\frac{A_i \nu _i n_i p_i \cos \left(\alpha _i \right)}{2 f_f}-\frac{A_i \nu _i n_i p_{i+1} \cos \left(\alpha _i\right)}{2 f_f}) + \sum_{i=0}^{n_{sheathlayers}} (A E_i \epsilon_{11,i}+\frac{2 A \nu _i p_i r_{in,i}^2}{r_{out,i}^2-r_{in,i}^2}-\frac{2 A \nu _i p_{i+1} r_{out,i}^2}{r_{out,i}^2-r_{int,i}^2}) = -\pi p_{ext} r_{ext}^2+\pi p_{int} r_{int}^2+T \end{align}
\end{DoxyEnumerate}

Solving the above equations directly applying the interface equation for all interfaces will give the correct results given that no gaps are present within the structure, however if gaps are present the interface equation should be removed and a known pressure of 0 should be applied instead on the interface. Several solutions are proposed for finding the correct solution. Skeie et al. (2012) proposes solving the constraints reformulating the equations as a quadratic programming (QP) problem. Due to Pythons preference for solving vectorized systems, the assumption that gaps are not often present and the fact that the stiffness matrix is constant for a given set of gaps, another solution was chosen here. First Py\+Flex solves the system for all instances in time assuming no gaps are present. Thereafter it checks for negative contact pressures. If negative contact pressures are found it finds all possible combinations of gaps based on the results, and consecutively solves the resulting equation system for the time steps where gaps are present, starting from the solutions with the most gaps between layers. It continues through the possible solutions until all time steps have a valid solution.\hypertarget{index_subsec_local_bending}{}\subsection{Local bending stress}\label{index_subsec_local_bending}
The bending stress from bending of wires can be calculated using two different options. Either by the equations presented by Sævik (1992) or Witz and Tan(1992). The equations by Sævik included in Pyflex is resulting from the assumption that the wires follows the loxodromic curve, thus only experiencing axial displacements. The equations for the bending stress coefficient about the strong and weak axis per curvature is\+:

\begin{align} bc_2 = (1+sin^2(\alpha))\cos(\alpha)\sin(\theta_{rel})|\kappa| \end{align}

\begin{align} bc_3 = -cos^4(\alpha)\cos(\theta_{rel})|\kappa| \end{align}

While they according to Witz and Tan is\+:

\begin{align} bc_2 = \cos(\theta_{rel})|\kappa| \end{align}

\begin{align} bc_3 = -\sin(\theta_{rel})\cos(\alpha)|\kappa| \end{align}

These coefficients are multiplied with the distance to the corner following the coordinate system indicated in \hyperlink{index_subsec_coords_and_defs}{Coordinate system and definitions} and the Young\textquotesingle{}s modulus to end up with the total bending stress.\hypertarget{index_subsec_fricstress}{}\subsection{Friction stress}\label{index_subsec_fricstress}
As a flexible pipe is subjected to a curvature, shear forces builds up between the layers. When the curvatures are sufficiently small the available friction is large enough to withstand and sliding, while sliding occurs when the the shear force is larger than the available friction. In the stick domain the stresses arising from friction is found by assuming that the beam behaves as a Euler-\/\+Bernoulli beam and that plane sections remain plane, leading to a sinusoidal stress distribution around the beam. For helical members the stress is found as\+: \begin{align} \sigma_{11,stick} = E_iR_i\cos^2(\alpha_i)\kappa\sin(\theta) \end{align} giving a maximum of\+: \begin{align} \sigma_{11,stick,max} = E_iR_i\cos^2(\alpha_i)\kappa \end{align}

The axial force in the wire is then\+:

\begin{align} F_{11} = E_iA_iR_i\cos^2(\alpha_i)\kappa \end{align}

In the full slip domain the maximum shear force is already built up between the layers and further bending does not introduce further stress. With the assumption that the contact pressure from the axisymmetric analysis still holds, the pressure distribution would be linear in this case, with the frictional stresses remaining at zero at the neutral axis.

The shear force does not build up uniformly, but builds up faster towards the neutral axis of the bending, leading to slip being initiated sooner here than away from the neutral axis. Also, the stress distribution is linear on the part of the cross section in slip, while remaining sinusoidal in the part where slip has not occured yet. In Py\+Flex the entire layer slips at the same time. To ensure conservatism, the distribution is kept at being sinusoidal and the curvature at which slip is initiated is assumed to be at the point where the stress from the plane sections remain plane assumption coincides with the maximum possible frictional stress.

The maximum frictional stress can be found from multiplying the available frictional shear force with the tendon length from the neutral axis to the top of the riser\+: \begin{align} A_i\sigma_{fric,max,i} = b_i(p_i\mu_i+p_{i-1}\mu_{i-1})\frac{\pi R_i}{2|\sin(\alpha_i)|} \rightarrow \sigma_{stick,max,i} = b_i(p_i\mu_i+p_{i-1}\mu_{i-1})\frac{\pi R_i}{2|\sin(\alpha_i)|A_i} \end{align}

by equating the maximum available friction force with the force resulting from stick one can find an effective critical curvature as\+: \begin{align} \kappa=b_i(p_i\mu_i+p_{i-1}\mu_{i-1})\frac{\pi}{2|\sin(\alpha_i)|E_iA_i\cos^2(\alpha_i)}=(p_i\mu_i+p_{i-1}\mu_{i-1})\frac{\pi}{2|\sin(\alpha_i)|E_it_i\cos^2(\alpha)} \end{align}

With the assumptions above in mind, the slipping of the wires will lead to a friction stress free configuration which is not straight. To go from a state were slip is initiated through a reversal to have full slip in the other direction, twice the effective curvature needs to be applied. Together with the assumption that the layer goes abruptly from stick to slip this means an elastic-\/perfectly plastic material model is suitable. As the shape of the stress distribution around the cross section is kept constant and the whole layer goes from stick to slip at once, the same material model can be used for all points around the cross section. The yield surface has a constant radius of the effective critical curvature and the and maximum frictional stress in curvature and stress space respectively. The model keeps track and updates the position of the yield surface to keep the material state within or on the border of the yield surface.\hypertarget{index_sec_how_to_use}{}\section{How to use}\label{index_sec_how_to_use}
In this section aspects of using Py\+Flex will be described. As the source code is open, you can of course apply Py\+Flex as a library and apply it to your workflow as suitable. In this document however, the use through the G\+UI will be described.\hypertarget{index_subsec_inputfile}{}\subsection{Inputfile}\label{index_subsec_inputfile}
An example of an inputfile can be seen in \hyperlink{index_subsec_inputfile_example}{Example Inputfiles}. The input file consists of key words and values. The values are seperated from the key words by one or more tabs. For key words where more than one value is to be given, they are to be divided by a comma. To group values, for example when describing tendon geometries, each group is divided by a comma, while each value within a group is seperated by one or more blank spaces, as exemplified below\+: Layer\+Geometry 1e-\/3 218.\+7e-\/3, 1e-\/3 45.\+3e-\/3, 5e-\/3 12e-\/3, 5e-\/3 12e-\/3 describing four layer geometries.

The text following \# is parsed as a comment.\hypertarget{index_subsubsec_keywords}{}\subsubsection{Keywords}\label{index_subsubsec_keywords}
\tabulinesep=1mm
\begin{longtabu} spread 0pt [c]{*2{|X[-1]}|}
\hline
\rowcolor{\tableheadbgcolor}{\bf Key word }&{\bf Description  }\\\cline{1-2}
\endfirsthead
\hline
\endfoot
\hline
\rowcolor{\tableheadbgcolor}{\bf Key word }&{\bf Description  }\\\cline{1-2}
\endhead
Layer\+Radii &The radii of the layers. Counting from the inenr layer. For now only helical layers are included in the model \\\cline{1-2}
Layer\+Geometry &h\+The geometry of the tendons. Rectangular geometry assumed. For nonrectangular layers the resulting stresses will not be correct. However, applying the correct area will make the contact pressure calculation, as the only geometrical quantity that goes into it is the area of the tendon. \\\cline{1-2}
Lay\+Angle &The lay angle of the layer. In the input file it is given in degrees, within the program it is given in radians. \\\cline{1-2}
Comp\+Number &Number of tendons in a layers \\\cline{1-2}
Friction\+Factor &Friction factor on the inside of each layer. If only one is applied it is assumed to be valid for all layers. \\\cline{1-2}
Youngs\+Modulus &The Youngs for all layers. \\\cline{1-2}
Typical\+Curvature &Typical curvature of the cross section. An option for quickly verifying whether friction will contribute significantly to fatigue. \\\cline{1-2}
Typical\+Tension &Typical tension of the cross section. An option for quickly verifying whether friction will contribute significantly to fatigue. \\\cline{1-2}
\end{longtabu}
The keywords are case insensitive.\hypertarget{index_subsubsec_open_inputfile}{}\subsubsection{Open}\label{index_subsubsec_open_inputfile}
Input files are opened through the Open option under the File menu or by using the ctrl+o shortcut. The extension for the input files is {\itshape dat}.\hypertarget{index_editing_in_gui}{}\subsubsection{Editing through the G\+UI}\label{index_editing_in_gui}
The riser model is held in a tree view on the left, where the numbers can be manipulated. If more layers is needed, right click on \textquotesingle{}Model data\textquotesingle{} and select \textquotesingle{}Add layer\textquotesingle{}.\hypertarget{index_subsec_timehistory_file}{}\subsection{Time history file}\label{index_subsec_timehistory_file}
The time history file is in the current version an A\+S\+C\+II file containing the time, tension, curvatures and pressures. By default the sequence is as follows\+:

$Time$ $\kappa_2$ $\kappa_3$ $p_i$ $p_e$

As for the input file \# is a comment character. If placed on the start of a line that line will be seen as a comment line. To change the sequence of input, add \textquotesingle{}\# \#\textquotesingle{} to the first line followed by the sequence of the data. The key words for the input quanteties are included in the \hyperlink{index_subsec_time_history_example}{Example Time History File}. The time history files have the extension tim.\hypertarget{index_import_timehistories}{}\subsubsection{Import}\label{index_import_timehistories}
The time history files can be imported either one by one or by folder. If a folder is chosen, all $\ast$.tim files in that folder will be loaded into Py\+Flex. \hypertarget{index_import_timehistories}{}\subsubsection{Import}\label{index_import_timehistories}
The time series can be vizualized be right clicking the time series and select \textquotesingle{}Plot the time histories\textquotesingle{}.\hypertarget{index_subsec_analysis}{}\subsection{Analysis}\label{index_subsec_analysis}
\hypertarget{index_subsubsec_full_analysis}{}\subsubsection{Full Analysis}\label{index_subsubsec_full_analysis}
The full analysis is chosen by right clicking on the \textquotesingle{}Calculations\textquotesingle{} item in the tree model in the left in the user interface. A tab containing the needed input and a space for dropping time history files will pop up on the lower right side of the G\+UI. In the current version the user is confined to choose analytical as the analysis method. Further the user can choose the following\+:
\begin{DoxyEnumerate}
\item The number of cores for the calculations. This reflects how many threads that are started for doing the computation. Applying more threads than you have cores will hamper performance.
\item Bend model. Choose between the formulae of Sævik or Tan for finding the stress in the tendons from local bending.
\item Number of points around the cross section. On how many points around the cross section to calculate the stresses.
\item Number of points on the tendon to calculate stresses. This is an option only valid for circular tendons. For rectangular tendons, the stresses will be calculated in the corners. {\bfseries Note\+:} note, contact forces found by formulae for tendons with flat contact surfaces. Also the model has only been validated against models which applied tendons with flat contact surfaces.
\end{DoxyEnumerate}

To include time series for analysis, drag and drop files from the tree model on the left side onto the calculation tab. For imported folder, one can drag and drop all time history files by clicking the \textquotesingle{}Time histories from directory\textquotesingle{} item and dropping it.\hypertarget{index_subsec_results}{}\subsection{subsec\+\_\+results}\label{index_subsec_results}
When the analysis is run, the results pops up in a tab in the upper right window. The results are presented in a tree model. By clicking on an item, it is toggled on and off in the plot area to the right. The color of the font changes to match the color of the corresponding curve in the plot.\hypertarget{index_Export}{}\subsubsection{Export}\label{index_Export}
Exporting the results can be done in two ways. Under the tree model there is a button for exporting all results. This will create a hdf5 file containing all results and also the input time histories. The other way of exporting results is thorugh the tool bar on the plot. Through this the plotted results can be exported. It can be exported in A\+S\+C\+II format of hdf5 format. The A\+S\+C\+II file will have the extension {\itshape ts}.\hypertarget{index_sec_validation}{}\section{Validation}\label{index_sec_validation}
\hypertarget{index_subsec_nugteren}{}\subsection{Nugteren}\label{index_subsec_nugteren}
Nugteren (2015) made a similar model to this, and carried out comparison of the results with results obtained by an unknown industry renowned software for the axisymmetric loads, and with the D\+N\+V\+GL software Helica for bending loads. The files used for this comparison is a part of this software. In the thesis the axisymmetric and bending models are called A and B respectively, the same naming is kept here.\hypertarget{index_subsubsec_nugteren_model_a}{}\subsubsection{Model A}\label{index_subsubsec_nugteren_model_a}
In the axisymmetric model the quantities of interest is the axial stress in the layers themselves, and the contact pressures, as these influence the bending behavior.

The model has the following properties\+: \begin{center} \tabulinesep=1mm
\begin{longtabu} spread 0pt [c]{*6{|X[-1]}|}
\hline
\rowcolor{\tableheadbgcolor}{\bf Property }&{\bf Layer 1 }&{\bf Layer 2 }&{\bf Layer 3 }&{\bf Layer 4 }&{\bf unit  }\\\cline{1-6}
\endfirsthead
\hline
\endfoot
\hline
\rowcolor{\tableheadbgcolor}{\bf Property }&{\bf Layer 1 }&{\bf Layer 2 }&{\bf Layer 3 }&{\bf Layer 4 }&{\bf unit  }\\\cline{1-6}
\endhead
Layer radii &108.\+1 &113 &118.\+2&124.\+6 &mm \\\cline{1-6}
Layer thickness &4&4 &5 &5 &mm \\\cline{1-6}
Layer width &54.\+675 &11.\+325 &12&12 &mm \\\cline{1-6}
Lay angle &-\/86.\+1&87.\+6&-\/30&30 &degrees \\\cline{1-6}
Number of tendons &2&2&50&53 &-\/ \\\cline{1-6}
Young\textquotesingle{}s Modulus&200 &200 &200&200 &M\+Pa \\\cline{1-6}
Initial gap &-\/&0.\+0 &0.\+0 &0.\+0 &mm \\\cline{1-6}
\end{longtabu}
\end{center} 

The thickness of the two inner layers they are estimated from the difference in radius for the layers, while the width is adjusted to give the same area as in Nugterens thesis.

The definition of the five load cases were\+: \begin{center} \tabulinesep=1mm
\begin{longtabu} spread 0pt [c]{*3{|X[-1]}|}
\hline
\rowcolor{\tableheadbgcolor}{\bf Name }&{\bf $ p_i $ }&{\bf $t_{eff}$  }\\\cline{1-3}
\endfirsthead
\hline
\endfoot
\hline
\rowcolor{\tableheadbgcolor}{\bf Name }&{\bf $ p_i $ }&{\bf $t_{eff}$  }\\\cline{1-3}
\endhead
L\+C1 &462 bar &0 kN \\\cline{1-3}
L\+C2 &462 bar &220.\+6 kN \\\cline{1-3}
L\+C3 &0 bar &220.\+6 kN \\\cline{1-3}
L\+C4 &508 bar &131.\+1 kN \\\cline{1-3}
L\+C5 &721 bar &0 kN \\\cline{1-3}
\end{longtabu}
\end{center}  Where the inner pressure is applied on the innermost layer. In the following table the axial stress in each layer is compared with both the unknown industrially accepted tool and the program of Nugteren. \begin{center} \tabulinesep=1mm
\begin{longtabu} spread 0pt [c]{*6{|X[-1]}|}
\hline
\rowcolor{\tableheadbgcolor}{\bf }&{\bf L\+C1 }&{\bf L\+C2 }&{\bf L\+C3 }&{\bf L\+C4 }&{\bf L\+C5 }\\\cline{1-6}
\endfirsthead
\hline
\endfoot
\hline
\rowcolor{\tableheadbgcolor}{\bf }&{\bf L\+C1 }&{\bf L\+C2 }&{\bf L\+C3 }&{\bf L\+C4 }&{\bf L\+C5 }\\\cline{1-6}
\endhead
\multicolumn{6}{|p{(\linewidth-\tabcolsep*6-\arrayrulewidth*1)*6/6}|}{\PBS\centering Layer 1 }\\\cline{1-6}
Design \mbox{[}M\+Pa\mbox{]}&347 &338 &-\/9 &376 &542 \\\cline{1-6}
Nugteren \mbox{[}M\+PA\mbox{]}&345 &337 &-\/9 &375 &540 \\\cline{1-6}
Pyflex \mbox{[}M\+PA\mbox{]}&349 &342 &-\/8 &379 &545 \\\cline{1-6}
Deviation design \mbox{[}\%\mbox{]}&0.\+6 &1.\+1 &-\/16.\+0 &0.\+9 &0.\+5 \\\cline{1-6}
Deviation Nugteren \mbox{[}\%\mbox{]}&1.\+2 &1.\+4 &-\/16.\+0 &1.\+2 &0.\+9 \\\cline{1-6}
\multicolumn{6}{|p{(\linewidth-\tabcolsep*6-\arrayrulewidth*1)*6/6}|}{\PBS\centering Layer 2 }\\\cline{1-6}
Design \mbox{[}M\+Pa\mbox{]} &329 &321 &-\/8 &357 &514 \\\cline{1-6}
Nugteren \mbox{[}M\+PA\mbox{]} &330 &323 &-\/8 &358 &516 \\\cline{1-6}
Pyflex \mbox{[}M\+PA\mbox{]} &334 &327 &-\/7 &363 &521 \\\cline{1-6}
Deviation design \mbox{[}\%\mbox{]} &1.\+4 &1.\+8 &-\/11.\+6 &1.\+6 &1.\+3 \\\cline{1-6}
Deviation Nugteren \mbox{[}\%\mbox{]} &1.\+1 &1.\+1 &-\/11.\+6 &1.\+3 &0.\+9 \\\cline{1-6}
\multicolumn{6}{|p{(\linewidth-\tabcolsep*6-\arrayrulewidth*1)*6/6}|}{\PBS\centering Layer 3 }\\\cline{1-6}
Design \mbox{[}M\+Pa\mbox{]} &314 &359 &45 &372 &490 \\\cline{1-6}
Nugteren \mbox{[}M\+PA\mbox{]} &316 &357 &42 &372 &494 \\\cline{1-6}
Pyflex \mbox{[}M\+PA\mbox{]} &309 &351 &42 &365 &483 \\\cline{1-6}
Deviation design \mbox{[}\%\mbox{]} &-\/1.\+4 &-\/2.\+2 &-\/7.\+3 &-\/1.\+9 &-\/1.\+4 \\\cline{1-6}
Deviation Nugteren \mbox{[}\%\mbox{]} &-\/2.\+1 &-\/1.\+6 &-\/0.\+7 &-\/1.\+9 &-\/2.\+2 \\\cline{1-6}
\multicolumn{6}{|p{(\linewidth-\tabcolsep*6-\arrayrulewidth*1)*6/6}|}{\PBS\centering Layer 4 }\\\cline{1-6}
Design \mbox{[}M\+Pa\mbox{]} &229 &271 &42 &277 &357 \\\cline{1-6}
Nugteren \mbox{[}M\+PA\mbox{]} &311 &353 &40 &368 &487 \\\cline{1-6}
Pyflex \mbox{[}M\+PA\mbox{]} &303 &345 &41 &358 &473 \\\cline{1-6}
Deviation design \mbox{[}\%\mbox{]} &32.\+3 &27.\+1 &-\/1.\+3 &29.\+2 &32.\+5 \\\cline{1-6}
Deviation Nugteren \mbox{[}\%\mbox{]} &-\/2.\+5 &-\/2.\+4 &3.\+6 &-\/2.\+7 &-\/2.\+9 \\\cline{1-6}
\end{longtabu}
\end{center} 

The deviation deviates from what would have been reached by comparing the values in the table as more significant digits were applied for the results from Py\+Flex than what is shown in the table. Barring the results for the outermost layer, the layer stresses are similar. For the outermost layer on the other hand, the stresses from Nugterens model and Py\+Flex are comparable, but are seen to overestimate the stresses compared to the unknown design tool. As the solution method in this tool is not known speculation of why the significant differences arises are hard, however it underlines a need for more open test data.

The table below compares the obtained contact pressures.

\begin{center} \tabulinesep=1mm
\begin{longtabu} spread 0pt [c]{*6{|X[-1]}|}
\hline
\rowcolor{\tableheadbgcolor}{\bf }&{\bf L\+C1 }&{\bf L\+C2 }&{\bf L\+C3 }&{\bf L\+C4 }&{\bf L\+C5 }\\\cline{1-6}
\endfirsthead
\hline
\endfoot
\hline
\rowcolor{\tableheadbgcolor}{\bf }&{\bf L\+C1 }&{\bf L\+C2 }&{\bf L\+C3 }&{\bf L\+C4 }&{\bf L\+C5 }\\\cline{1-6}
\endhead
\multicolumn{6}{|p{(\linewidth-\tabcolsep*6-\arrayrulewidth*1)*6/6}|}{\PBS\centering Layer 1 -\/ Layer 2 }\\\cline{1-6}
Design \mbox{[}M\+Pa\mbox{]} &14.\+70 &15.\+33 &0.\+64 &16.\+54 &22.\+95 \\\cline{1-6}
Nugteren \mbox{[}M\+PA\mbox{]} &14.\+99 &15.\+59 &0.\+62 &16.\+84 &23.\+41 \\\cline{1-6}
Pyflex \mbox{[}M\+PA\mbox{]} &15.\+20 &15.\+85 &0.\+65 &17.\+10 &23.\+72 \\\cline{1-6}
Deviation design \mbox{[}\%\mbox{]} &3.\+4 &3.\+4 &1.\+7 &3.\+4 &3.\+4 \\\cline{1-6}
Deviation Nugteren \mbox{[}\%\mbox{]} &1.\+4 &1.\+7 &5.\+0 &1.\+5 &1.\+3 \\\cline{1-6}
\multicolumn{6}{|p{(\linewidth-\tabcolsep*6-\arrayrulewidth*1)*6/6}|}{\PBS\centering Layer 2 -\/ Layer 3 }\\\cline{1-6}
Design \mbox{[}M\+Pa\mbox{]} &5.\+15 &6.\+02 &0.\+83 &6.\+20 &8.\+11 \\\cline{1-6}
Nugteren \mbox{[}M\+PA\mbox{]} &6.\+09 &6.\+89 &0.\+81 &7.\+17 &9.\+50 \\\cline{1-6}
Pyflex \mbox{[}M\+PA\mbox{]} &6.\+11 &6.\+94 &0.\+83 &7.\+21 &9.\+54 \\\cline{1-6}
Deviation design \mbox{[}\%\mbox{]} &18.\+7 &15.\+3 &0.\+0 &16.\+4 &17.\+6 \\\cline{1-6}
Deviation Nugteren \mbox{[}\%\mbox{]} &0.\+4 &0.\+8 &2.\+4 &0.\+6 &0.\+4 \\\cline{1-6}
\multicolumn{6}{|p{(\linewidth-\tabcolsep*6-\arrayrulewidth*1)*6/6}|}{\PBS\centering Layer 3 -\/ Layer 4 }\\\cline{1-6}
Design \mbox{[}M\+Pa\mbox{]} &2.\+10 &2.\+48 &0.\+38 &2.\+54 &3.\+28 \\\cline{1-6}
Nugteren \mbox{[}M\+PA\mbox{]} &2.\+97 &3.\+36 &0.\+40 &3.\+49 &4.\+62 \\\cline{1-6}
Pyflex \mbox{[}M\+PA\mbox{]} &2.\+90 &3.\+30 &0.\+40 &3.\+43 &4.\+53 \\\cline{1-6}
Deviation design \mbox{[}\%\mbox{]} &38.\+2 &33.\+0 &4.\+5 &34.\+9 &38.\+1 \\\cline{1-6}
Deviation Nugteren \mbox{[}\%\mbox{]} &-\/2.\+3 &-\/1.\+8 &-\/0.\+8 &-\/1.\+8 &-\/2.\+0 \\\cline{1-6}
\end{longtabu}
\end{center} 

Based on this table it is obvious that this model suffers from the same overprediction of contact pressures for the outer layer that the model proposed by Nugteren, as all results are compareble with the results from that model. The small differences stems from Nugteren using an iterative solution taking into account that the area of the outer and inner pressure cahgnes as the pipe expands and contracts and that Py\+Flex corrects for force equilibrium. The differences are direct results of over predicting the tensions in the outer layer.

The higher contact pressures will make the friction stresses be on the conservative side.\hypertarget{index_subsubsec_nugteren_model_b}{}\subsubsection{Model B}\label{index_subsubsec_nugteren_model_b}
The validation of the bending model suffers from lack of data defining the geometry of the riser in question. In Nugterens thesis (2015) she explains how she found the numbers she has applied. The properties she came up with are also applied in the Py\+Flex model\+:

\begin{center} \tabulinesep=1mm
\begin{longtabu} spread 0pt [c]{*6{|X[-1]}|}
\hline
\rowcolor{\tableheadbgcolor}{\bf Property }&{\bf Layer 1 }&{\bf Layer 2 }&{\bf Layer 3 }&{\bf Layer 4 }&{\bf unit  }\\\cline{1-6}
\endfirsthead
\hline
\endfoot
\hline
\rowcolor{\tableheadbgcolor}{\bf Property }&{\bf Layer 1 }&{\bf Layer 2 }&{\bf Layer 3 }&{\bf Layer 4 }&{\bf unit  }\\\cline{1-6}
\endhead
Layer radii &77.\+832 &81.\+36&82&93 &mm \\\cline{1-6}
Layer thickness &1 &1 &5 &5 &mm \\\cline{1-6}
Layer width &2.\+5 &30&10 &10 &mm \\\cline{1-6}
Lay angle &-\/87.\+7&87.\+7 &35&-\/36.\+7 &degrees \\\cline{1-6}
Number of tendons &1 &0 &38&42 &-\/ \\\cline{1-6}
Youngs\+Modulus&210 &210&210&210 &M\+Pa \\\cline{1-6}
Friction\+Factor &0.\+0&0.\+2&0.\+2&0.\+2 &-\/ \\\cline{1-6}
\end{longtabu}
\end{center} 

Nugteren only compared the stresses in the third layer, as that is most usually the critical one for fatigue. Also, she applied the bending formulae proposed in the Handbook on Design and Operation of Flexible Pipes (2014), which is the same as Sævik(1992) presented. The Py\+Flex results are also generated using this model for local bending of the wires. In the validation case a tension of 1000 $kN$ is applied without any internal or external pressure, while the curvature goes up to 0.\+3 $m^{-1}$. The curvature was applied as $\kappa_2$.In the following table the results are compared\+:

\begin{center} \tabulinesep=1mm
\begin{longtabu} spread 0pt [c]{*8{|X[-1]}|}
\hline
\rowcolor{\tableheadbgcolor}\multicolumn{2}{|p{(\linewidth-\tabcolsep*8-\arrayrulewidth*3)*2/8}|}{\cellcolor{\tableheadbgcolor}{\bf }}&\multicolumn{3}{p{(\linewidth-\tabcolsep*8-\arrayrulewidth*3)*3/8}|}{\cellcolor{\tableheadbgcolor}\PBS\centering {\bf $\theta = 0 deg$}}&\multicolumn{3}{p{(\linewidth-\tabcolsep*8-\arrayrulewidth*3)*3/8}|}{\cellcolor{\tableheadbgcolor}\PBS\centering {\bf $\theta = 45 deg$ }}\\\cline{1-8}
\endfirsthead
\hline
\endfoot
\hline
\rowcolor{\tableheadbgcolor}\multicolumn{2}{|p{(\linewidth-\tabcolsep*8-\arrayrulewidth*3)*2/8}|}{\cellcolor{\tableheadbgcolor}{\bf }}&\multicolumn{3}{p{(\linewidth-\tabcolsep*8-\arrayrulewidth*3)*3/8}|}{\cellcolor{\tableheadbgcolor}\PBS\centering {\bf $\theta = 0 deg$}}&\multicolumn{3}{p{(\linewidth-\tabcolsep*8-\arrayrulewidth*3)*3/8}|}{\cellcolor{\tableheadbgcolor}\PBS\centering {\bf $\theta = 45 deg$ }}\\\cline{1-8}
\endhead
\multicolumn{2}{|p{(\linewidth-\tabcolsep*8-\arrayrulewidth*7)*2/8}|}{\multirow{2}{\linewidth}{}}&Helica&Nugteren&Py\+Flex&Helica&Nugteren&Py\+Flex \\\cline{3-8}
\multicolumn{2}{|p{(\linewidth-\tabcolsep*8-\arrayrulewidth*3)*2/8}|}{}&\multicolumn{3}{p{(\linewidth-\tabcolsep*8-\arrayrulewidth*3)*3/8}|}{\PBS\centering \mbox{[}M\+Pa\mbox{]}}&\multicolumn{3}{p{(\linewidth-\tabcolsep*8-\arrayrulewidth*3)*3/8}|}{\PBS\centering \mbox{[}M\+Pa\mbox{]} }\\\cline{1-8}
Axial stress &$ \sigma_{11} $ &326&315&315&320&315&315 \\\cline{1-8}
Friction stress&$ \sigma_{fric} $&168&161&161&84&85&114 \\\cline{1-8}
Bending stress&$ \sigma_{b} $&66&71&71&281&293&293 \\\cline{1-8}
\end{longtabu}
\end{center} 

As can be seen the stresses matches very well. Except for the friction stress at 45 degrees. The stresses differ here because we keep an sinusoidal stress distribution, while in Helica and Nugterens model a linear distribution is clearly applied. For computational efficiency it is benificial to keep the same distribution in both the stick and slip regimes, and as the sinusoidal distribution is always on the conservative side, this is choosen.\hypertarget{index_sec_symbols}{}\section{Symbols}\label{index_sec_symbols}
\begin{center} \tabulinesep=1mm
\begin{longtabu} spread 0pt [c]{*2{|X[-1]}|}
\hline
\rowcolor{\tableheadbgcolor}{\bf Symbol }&{\bf Description  }\\\cline{1-2}
\endfirsthead
\hline
\endfoot
\hline
\rowcolor{\tableheadbgcolor}{\bf Symbol }&{\bf Description  }\\\cline{1-2}
\endhead
$ \alpha $&Lay angle \\\cline{1-2}
$ \epsilon_{11}$&Axial strain in a tendon \\\cline{1-2}
$ \epsilon_a $&Axial strain for pipe \\\cline{1-2}
$ \kappa $&Curvature \\\cline{1-2}
$ \kappa_2 $&Curvature around axis 2. See \hyperlink{index_subsec_coords_and_defs}{Coordinate system and definitions} \\\cline{1-2}
$ \kappa_3 $&Curvature around axis 3. See \hyperlink{index_subsec_coords_and_defs}{Coordinate system and definitions} \\\cline{1-2}
$ \nu $&Poisson\textquotesingle{}s ratio \\\cline{1-2}
$ \sigma_{11} $&Axial stress in a tendon \\\cline{1-2}
$ \sigma_{b} $ &Bending stress \\\cline{1-2}
$ \sigma_{fric} $&Friction stress \\\cline{1-2}
$ \sigma_{fric,max,i} $&Maximum stick stress in a tendon in layer i. \\\cline{1-2}
$ \theta $&Angle of cross sectional position. See \hyperlink{index_subsec_coords_and_defs}{Coordinate system and definitions} \\\cline{1-2}
$ \theta_{rel}$&Relative angle between tendon and curvature. See \hyperlink{index_subsec_coords_and_defs}{Coordinate system and definitions} \\\cline{1-2}
$ A_{tendon} $&Crossectional area of a tendon \\\cline{1-2}
$ b $&Breadth of a tendon \\\cline{1-2}
$ E $&Elastic modulus \\\cline{1-2}
$ f_f $&Fillfactor \\\cline{1-2}
$ N $&Number of layers \\\cline{1-2}
$ n_{tendons} $&Number of tendons in a layer \\\cline{1-2}
$ p_{ext} $&External pressure \\\cline{1-2}
$ p_{int} $&Internal pressure \\\cline{1-2}
$ p_i $&Pressure on the inside of a layer \\\cline{1-2}
$ R_i $&Mean radius of a layer \\\cline{1-2}
$ r_{ext} $&External radius \\\cline{1-2}
$ R_{inner} $&Inner radius. Midpoint between two layers. \\\cline{1-2}
$ r_{int} $&Internal radius \\\cline{1-2}
$ R_{outer} $&Outer radius. Midpoint between two layers. \\\cline{1-2}
$ t_{eff} $&Effective tension \\\cline{1-2}
$ u3 $&Radial displacement \\\cline{1-2}
\end{longtabu}
\end{center} \hypertarget{index_sec_references}{}\section{References}\label{index_sec_references}
Custodio, A., B., Vaz, M.\+A. A nonlinear formulation for the axisymmetric response of umbilical cables and flexible pipes. Applied Ocean Research 24, 2002

M\+A\+R\+I\+N\+T\+E\+K/\+N\+T\+N\+U/4\+Subsea Handbook on Design and Operation of Flexible Pipes, 2014.

Nugteren, F.(2015) Flexible Rieser Fatigue Analysis\+:Studying Conservatism in Flexible Riser Fatigue Analysis and Development of an Engineering Model to Study Influencing Parameters of Local Wire Stress. Delft University of Technology, The Netherlands.

Skeie, G., Sødahl, N. and Steinkjer O. Efficient Fatigue Analysis of Helix Elements in Umbilicals and Flexible Risers\+: Theory and Applications. Journal of Applied Mathematics, 2012

Sævik, S., On stresses and fatigue in flexible pipes, 1992.

Witz J., Tan Z. On the flexural structural behaviour of flexible pipes, umbilicals and marine cables. Marine Structures, 1992.\hypertarget{index_sec_appendices}{}\section{Appendices}\label{index_sec_appendices}
\hypertarget{index_subsec_inputfile_example}{}\subsection{Example Inputfiles}\label{index_subsec_inputfile_example}
\href{modelA.dat}{\tt {\bfseries Nugteren model A}}

\href{modelB.dat}{\tt {\bfseries Nugteren model B}}\hypertarget{index_subsec_time_history_example}{}\subsection{Example Time History File}\label{index_subsec_time_history_example}
\href{modelA.tim}{\tt {\bfseries Nugteren model A}}

\href{modelB.tim}{\tt {\bfseries Nugteren model B}} 