var setup_8py =
[
    [ "author", "setup_8py.html#a3a57a4772d418a06835249cbade0d86a", null ],
    [ "author_email", "setup_8py.html#a5b08034343aa2be607722a8b315f3625", null ],
    [ "description", "setup_8py.html#aedf461ec52a946bda975938ba0b93ec0", null ],
    [ "license", "setup_8py.html#a8ed6f50a28bd6a8794f8e1153baa6de9", null ],
    [ "name", "setup_8py.html#ab3a7a0638d76a01367c5bc3cc699447f", null ],
    [ "packages", "setup_8py.html#aff2375a361fd5865c77bd9aa093be747", null ],
    [ "requires", "setup_8py.html#ad141cbe5f690da7a27b4672ecfb2e6a0", null ],
    [ "url", "setup_8py.html#afc13124aa5c0124e84e1d965e3f4b0fb", null ],
    [ "version", "setup_8py.html#a2aa722b36a933088812b50ea79b97a5c", null ]
];