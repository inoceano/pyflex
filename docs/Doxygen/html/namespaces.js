var namespaces =
[
    [ "axisymmetric", "namespaceaxisymmetric.html", null ],
    [ "coefficients", "namespacecoefficients.html", null ],
    [ "errorhandling", "namespaceerrorhandling.html", null ],
    [ "main", "namespacemain.html", null ],
    [ "pyflex", "namespacepyflex.html", null ],
    [ "setup", "namespacesetup.html", null ],
    [ "slenders", "namespaceslenders.html", null ],
    [ "test_analytical_input", "namespacetest__analytical__input.html", null ],
    [ "test_coefficients", "namespacetest__coefficients.html", null ],
    [ "test_Slender", "namespacetest___slender.html", null ],
    [ "timehist", "namespacetimehist.html", null ],
    [ "ui_mainwindow", "namespaceui__mainwindow.html", null ]
];