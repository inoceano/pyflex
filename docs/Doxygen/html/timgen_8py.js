var timgen_8py =
[
    [ "angle", "timgen_8py.html#aba1c819896b401e7a72ba0caffbb4956", null ],
    [ "fileheader", "timgen_8py.html#a4ea670ff59981e9932254d60d46aff3e", null ],
    [ "header", "timgen_8py.html#aff7ca11a7f74707c340203a42ff6b315", null ],
    [ "kuk", "timgen_8py.html#a8c85e00303595f1e8dd3fdb7f7d0d9a1", null ],
    [ "maxperiod", "timgen_8py.html#a7f3b948de51d54bbcbe427a1a510e676", null ],
    [ "mean", "timgen_8py.html#a473a03b5d1ba0d747f877eacc67c157e", null ],
    [ "meanperiod", "timgen_8py.html#a837ba0b571f238f67dbf72321b98b27c", null ],
    [ "minperiod", "timgen_8py.html#ab85b99d291143cedd7acf0067d70fe65", null ],
    [ "nperiods", "timgen_8py.html#adb8a9401e8faec4b90110ed5ac407c7f", null ],
    [ "peakness", "timgen_8py.html#aa81f89a0c253fb821dfc87157bbd8064", null ],
    [ "periods", "timgen_8py.html#a0b31274f6e68cd9004114c8b4b18ffd9", null ],
    [ "samplerate", "timgen_8py.html#a548a301057c37f3eb9dd62a32df911e4", null ],
    [ "stdev", "timgen_8py.html#ab2384dbf345393268388e755a83149f3", null ],
    [ "sumhist", "timgen_8py.html#a868479a9d1e9d3bf7768df1bef2938e2", null ],
    [ "t", "timgen_8py.html#a54b4fbaae05971cbdec83caf58ac5da4", null ],
    [ "timehist", "timgen_8py.html#ac04434ac4b1535b956e010845b74ef67", null ],
    [ "timespan", "timgen_8py.html#af4e301c42864231752a2e12faeed44a3", null ],
    [ "weight", "timgen_8py.html#a5cdb46ade9db77fbbb04d23900136a83", null ]
];