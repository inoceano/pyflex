var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstuvwy",
  1: "acefgmpstuw",
  2: "acempstu",
  3: "acelmprstu",
  4: "_abcdefghilmnoprstuw",
  5: "_abcdefghilmnoprstuvwy",
  6: "lr"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

