var NAVTREE =
[
  [ "PyFlex", "index.html", [
    [ "Program description", "index.html#sec_description", null ],
    [ "Getting started", "index.html#get_started", null ],
    [ "Theory", "index.html#sec_theory", [
      [ "Coordinate system and definitions", "index.html#subsec_coords_and_defs", null ],
      [ "Axisymmetric response", "index.html#subsec_axisymmetric", null ],
      [ "Local bending stress", "index.html#subsec_local_bending", null ],
      [ "Friction stress", "index.html#subsec_fricstress", null ]
    ] ],
    [ "How to use", "index.html#sec_how_to_use", [
      [ "Inputfile", "index.html#subsec_inputfile", [
        [ "Keywords", "index.html#subsubsec_keywords", null ],
        [ "Open", "index.html#subsubsec_open_inputfile", null ],
        [ "Editing through the GUI", "index.html#editing_in_gui", null ]
      ] ],
      [ "Time history file", "index.html#subsec_timehistory_file", [
        [ "Import", "index.html#import_timehistories", null ]
      ] ],
      [ "Analysis", "index.html#subsec_analysis", [
        [ "Full Analysis", "index.html#subsubsec_full_analysis", null ]
      ] ],
      [ "subsec_results", "index.html#subsec_results", [
        [ "Export", "index.html#Export", null ]
      ] ]
    ] ],
    [ "Validation", "index.html#sec_validation", [
      [ "Nugteren", "index.html#subsec_nugteren", [
        [ "Model A", "index.html#subsubsec_nugteren_model_a", null ],
        [ "Model B", "index.html#subsubsec_nugteren_model_b", null ]
      ] ]
    ] ],
    [ "Symbols", "index.html#sec_symbols", null ],
    [ "References", "index.html#sec_references", null ],
    [ "Appendices", "index.html#sec_appendices", [
      [ "Example Inputfiles", "index.html#subsec_inputfile_example", null ],
      [ "Example Time History File", "index.html#subsec_time_history_example", null ]
    ] ],
    [ "License", "md__home_joakim_pyflex_docs_License_license.html", null ],
    [ "README", "md__home_joakim_pyflex_README.html", null ],
    [ "Packages", null, [
      [ "Packages", "namespaces.html", "namespaces" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"classtimehist_1_1_full_stress_calc.html#a0523443afdaf68413a42d16aea6003aa"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';