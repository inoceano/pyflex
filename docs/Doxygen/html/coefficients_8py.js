var coefficients_8py =
[
    [ "analytical_contact_pressures", "coefficients_8py.html#a59aa39c0fdae3ada356cfea361b628b7", null ],
    [ "bending_stress_history", "coefficients_8py.html#a0e919c8c5d6f81c045e7dae8ccc87e01", null ],
    [ "critical_curvature", "coefficients_8py.html#ac5822200d42756aa6bcf104ebbefb919", null ],
    [ "fric_stress", "coefficients_8py.html#a2e6abd68d7fb12ef6b52f09dff2d52ea", null ],
    [ "local_bending", "coefficients_8py.html#a338e228a67aa37f098f4c8284c7bba69", null ],
    [ "max_stickstress", "coefficients_8py.html#a6b48cf80619fc8ee736af0377a446b8e", null ],
    [ "stick_stress", "coefficients_8py.html#a50b92e5a94329b1b837778d9b514ea5f", null ],
    [ "tension_stress_history", "coefficients_8py.html#a9b142510d29e403292fa4f121db30e43", null ]
];