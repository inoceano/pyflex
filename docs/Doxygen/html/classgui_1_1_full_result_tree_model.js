var classgui_1_1_full_result_tree_model =
[
    [ "__init__", "classgui_1_1_full_result_tree_model.html#acaf962c7bb1c7536feaacf700c319feb", null ],
    [ "add_result", "classgui_1_1_full_result_tree_model.html#a9a2336aa6acde737e1b4e24367fe389c", null ],
    [ "colorlist", "classgui_1_1_full_result_tree_model.html#a376f115ac99f3ff9424d1f0eb23a28f8", null ],
    [ "data", "classgui_1_1_full_result_tree_model.html#a7f3278091ef291f780aa29f8f2e45b8c", null ],
    [ "flags", "classgui_1_1_full_result_tree_model.html#a0351fefaa1abff0ceb7c425612ca94c8", null ],
    [ "recursiveplot", "classgui_1_1_full_result_tree_model.html#aceb19b5a52dc657dffd8d1f9cfaebe1d", null ],
    [ "singleplot", "classgui_1_1_full_result_tree_model.html#a5ffb535c35046a80488e06f37d7481ef", null ],
    [ "toggle", "classgui_1_1_full_result_tree_model.html#a6fbe28852bcf39e67901cf5feb58fc35", null ],
    [ "toggler", "classgui_1_1_full_result_tree_model.html#a9186a6ef3b8a12ea1d2458f377c7aa8b", null ],
    [ "activePlots", "classgui_1_1_full_result_tree_model.html#a526548b2605a5781dc6a1fe9d542afc7", null ],
    [ "colorIndexTaken", "classgui_1_1_full_result_tree_model.html#aa5ee86049da18fcbe6425ec36d5fc599", null ],
    [ "names", "classgui_1_1_full_result_tree_model.html#a480a2bb44e62162a0030e9b10909bfa9", null ],
    [ "plotData", "classgui_1_1_full_result_tree_model.html#a671213dd1ef1ac896572353309833d8a", null ],
    [ "rootItem", "classgui_1_1_full_result_tree_model.html#a3c9d16f76b12bec27c4f3f2bd67b705a", null ]
];