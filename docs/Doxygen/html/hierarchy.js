var hierarchy =
[
    [ "NavigationToolbar", null, [
      [ "timehist.FricCalcNavigationToolbar", "classtimehist_1_1_fric_calc_navigation_toolbar.html", [
        [ "timehist.FullCalcNavigationToolbar", "classtimehist_1_1_full_calc_navigation_toolbar.html", null ]
      ] ]
    ] ],
    [ "object", null, [
      [ "main.TreeItem", "classmain_1_1_tree_item.html", [
        [ "main.ModelTreeItem", "classmain_1_1_model_tree_item.html", null ]
      ] ],
      [ "timehist.AnalysisProp", "classtimehist_1_1_analysis_prop.html", null ],
      [ "timehist.FrictionCalc", "classtimehist_1_1_friction_calc.html", null ],
      [ "timehist.PlotData", "classtimehist_1_1_plot_data.html", [
        [ "timehist.FourLevelPlotData", "classtimehist_1_1_four_level_plot_data.html", [
          [ "timehist.FiveLevelPlotData", "classtimehist_1_1_five_level_plot_data.html", null ]
        ] ]
      ] ],
      [ "timehist.TimeHist", "classtimehist_1_1_time_hist.html", null ],
      [ "ui_mainwindow.Ui_MainWindow", "classui__mainwindow_1_1_ui___main_window.html", [
        [ "main.Window", "classmain_1_1_window.html", null ]
      ] ]
    ] ],
    [ "QAbstractItemModel", null, [
      [ "ui_mainwindow.GenericTreeModel", "classui__mainwindow_1_1_generic_tree_model.html", [
        [ "main.FullResultTreeModel", "classmain_1_1_full_result_tree_model.html", null ],
        [ "main.TreeModel", "classmain_1_1_tree_model.html", null ]
      ] ]
    ] ],
    [ "QMainWindow", null, [
      [ "main.Window", "classmain_1_1_window.html", null ]
    ] ],
    [ "QObject", null, [
      [ "errorhandling.EmittingStream", "classerrorhandling_1_1_emitting_stream.html", null ],
      [ "timehist.FullStressCalc", "classtimehist_1_1_full_stress_calc.html", null ]
    ] ],
    [ "QSortFilterProxyModel", null, [
      [ "timehist.FullTreeProxyModel", "classtimehist_1_1_full_tree_proxy_model.html", null ]
    ] ],
    [ "QStyledItemDelegate", null, [
      [ "ui_mainwindow.ComboDelegate", "classui__mainwindow_1_1_combo_delegate.html", null ]
    ] ],
    [ "QThread", null, [
      [ "timehist.FullAnalysisThread", "classtimehist_1_1_full_analysis_thread.html", null ]
    ] ],
    [ "slenders.Slender", "classslenders_1_1_slender.html", null ],
    [ "TestCase", null, [
      [ "test_analytical_input.TestAnalyticalInput", "classtest__analytical__input_1_1_test_analytical_input.html", null ],
      [ "test_coefficients.TestCoefficients", "classtest__coefficients_1_1_test_coefficients.html", null ],
      [ "test_Slender.TestSlender", "classtest___slender_1_1_test_slender.html", null ]
    ] ]
];